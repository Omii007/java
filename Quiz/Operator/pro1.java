
class Core2Web {

	public static void main(String[] args){
	
		System.out.println(4 / 0);
	}
}
/*1] Infinity
 *
 * 2] Divide by zero exception
 *
 * 3] compile time error
 *
 * 4] None
 *
 * Ans : 2] Divide by zero exception
 */

