
class Core2Web {

	public static void main(String[] args){
	
		int a = 10, b = 10, c = 10, d = 10;

		System.out.println(++a);
		System.out.println(b++);
		System.out.println(+-c);
		System.out.println(-++d);
	}
}
/* 1] 11 10 -10 -11
 *
 * 2] 10 10 10 11
 *
 * 3] 11 10 10 10
 *
 * 4] 11 11 -10 -11
 *
 * Ans : 1] 11 10 -10 -11
 */
