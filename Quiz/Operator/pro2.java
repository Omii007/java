
class Core2Web {

	public static void main(String[] args){
	
		double var1 = (4.0 / 0);
		System.out.println(var1);
	}
}
/* 1] Divide by zero exception
 *
 * 2] Infinity
 *
 * 3] compile time error
 *
 * 4] none
 *
 * Ans : 2] Infinity
 */
