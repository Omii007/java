
class Core2Web {

	public static void main(String[] args){
	
		if(10){
		
			System.out.println("Inside if");
		}else{
		
			System.out.println("Inside else");
		}
	}
}
/* 1] error : value not mapped to a boolean
 *
 * 2] error : incompatible types :int cannot be converted to boolean
 *
 * 3] Inside else
 *
 * 4] Inside if
 *
 * Ans : 2] error : incompatible types: int cannot converted to boolean
 */
