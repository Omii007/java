
class Core2Web {

	public static void main(String[] args){
	
		if(true)
		
			System.out.println("Inside if1");
			System.out.println("Inside if2");
			
	}
}
/* 1] Compile time error
 *
 * 2] Inside if1
 *
 * 3] Inside if2
 *
 * 4] Inside if1
 *    Inside if2
 *
 * Ans : 4] Inside if1
 * 	    Inside if2
 */
