
class Core2Web {

	public static void main(String[] args){
	
		if(false){
		
			System.out.println("Inside if");

		}else(true){
		
			System.out.println("Inside else");
		}
	}
}
/* 1] Inside if
 *
 * 2] Inside else
 *
 * 3] Compile time error
 *
 * 4] None of the above
 *
 * Ans : 3] Compile time error
 */
