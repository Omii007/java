
class Core2Web {

	public static void main(String[] args){
	
		if(true){
		
			if(false){
			
				System.out.println("Inside if");

			}else{
			
				System.out.println("Inside else1");
			}
		}else{
		
			System.out.println("Inside else2");
		}
	}
}
/* 1] Inside else1
 *
 * 2] Inside else2
 *
 * 3] Inside if
 *
 * 4] Compile time error
 *
 * Ans : 1] Inside else1
 */
