
class Core2Web {

	public static void main(String[] args){
	
		if(true)
		
			System.out.println("Inside if1");

			System.out.println("Inside if2");
		else
			System.out.println("Inside else");
	}
}
/* 1] Inside if1
 *
 * 2] Compile time error
 *
 * 3] Inside if2
 *
 * 4] Inside if1
 *    Inside if2
 *
 *Ans : 2] Compile time error
 */
