
/* Que 1. What is the old name for java Programming?
 *
 * 1]  java 2
 *
 * 2]  oak
 *
 * 3]  java
 *
 * ANS : 2] oak
 * 
 * Que 2. When was java released?
 *
 * 1]  1975
 *
 * 2]  1985
 *
 * 3]  1995
 *
 * ANS : 3]  1995
 *
 *
 * Que 4. Which one is not part of the Execution Engine?
 *
 * 1]  Interpreter
 *
 * 2]  JNI  (Java Native Interface)
 *
 * 3]  JIT (Just InTime Compiler)
 *
 * 4]  Garbage Collector
 *
 * ANS : 2] JNI (Java Native Interface)
 *
 *
 * Que 5. ..... is the developer of the java Programming Language.
 *
 * 1] Dennis Ritchie
 *
 * 2] James Gosling
 *
 * 3] Bjarne Stroustrup
 *
 * ANS : James Gosling
 *
 *
 * Que 6. Which of the following is a correct header line for main in a java program?
 *
 * 1]  public static void main(args)
 *
 * 2] public void static main(String[] args)
 *
 * 3] public static void main(String[])
 *
 * 4] public static void main(String[] args)
 *
 * Ans : 4] public static void main(String[] args)
 *
 *
 * Que 7. What is the file extension for java Executable Code?
 *
 * 1] .java
 *
 * 2] .class
 *
 * 3] .exe
 *
 * Ans : 2] .class
 *
 *
 * Que 9. Which one is not part of JVM Architecture?
 *
* 1] Classloader Subsystem
*
* 2] Native method library
*
* 3] java compiler
*
* Ans : 3] java compiler
*
*
* Que 10] What is the file extension for java source code?
*
* 1] .java
*
* 2] .class
*
* 3] .exe
*
* Ans : 1] .java
* */

