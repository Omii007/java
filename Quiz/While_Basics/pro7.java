
class Core2Web {

	public static void main(String s[]){
	
		if(true)
			while(false)
				System.out.println("Hello,Core2Web!");
			
		System.out.println("true,false");
	}
}
/* 1] true
 *
 * 2] Compile time error
 *
 * 3] run time error
 *
 * 4] true,false
 *
 * Ans : 2] Compile time error
 */
