
class Core2Web {

	public static void main(String s[]){
	
		int i = 10;

		while(i < 10){
		
			if(true){
			
				System.out.println("Hello,Core2Web!");
			}
			i++;
		}
		System.out.println("true,false");
	}
}
/* 1] Hello,Core2Web
 *    true,false
 * 
 * 2] true,false
 *
 * 3] Infinite loop
 *
 * 4] error
 *
 * Ans : 2] true,false
 */
