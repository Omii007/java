
class Core2Web {

	public static void main(String[] args){
	
		int var = 5;

		System.out.println("Inside main");

		while(var-- > 3);{
		
			System.out.println("Inside While");
		}
	}
}
/* 1] Inside main
 *
 * 2] Inside main
 *    Inside While
 * 
 * 3] Inside main (Codes goes into infinite loop with no output)
 *
 * 4] Compile time error
 *
 * Ans : 2] Inside main
 * 	    Inside While
 */
