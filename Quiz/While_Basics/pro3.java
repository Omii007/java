
class Core2Web {

	public static void main(String[] args){
	
		while(true){
		
			System.out.println("Hello,Core2Web!");
		}
	}
}
/* 1] true
 *
 * 2] run time error
 *
 * 3] Infinite loop
 *
 * 4] Hello,Core2Web
 *
 * Ans : 3] Infinite loop
 */
