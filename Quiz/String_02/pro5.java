
class Core2Web {

	public static void main (String[] args){
	
		String str1 = new String("Core2Web");
		String str2 = new String("Core2Web");

		if(System.identityHashCode(str1).equals(System.identityHashCode(str2)))
				System.out.println("True");
		else
				System.out.println("False");
	}
	// error : cannot be dereferenced
	// integerHashCode returns integer value
}
