
// Access Specifier:-
// 1. Default
// 2. Private
// 3. Public
// 4. Protected

class Core2Web {

	int numCources = 8;
	private String Favourite = "CPP";

	void disp (){
	
		System.out.println(numCources);
		System.out.println(Favourite);
	}
}
class Student {

	public static void main(String[] args){
	
		Core2Web obj = new Core2Web();

		obj.disp();
		
		System.out.println(obj.numCources);
		System.out.println(obj.Favourite);
	}
}
