
class StringDemo {

	public static void main(String[] args){
	
		String str1 = "Shashi";
		String str2 = "Bagal";

		String str3 = str1 + str2;  // internally StringBuilder class chya .appendmethod la call krto.
		String str4 = str1.concat(str3); // String class chi method ahe

		System.out.println(str3);
		System.out.println(str4);
	}
}
