
// Compareto

import java.util.*;

class compareto {

	static int mystrcompare(String str1,String str2){
		
		char arr1[] = str1.toCharArray();
		char arr2[] = str2.toCharArray();

		for(int i=0;i<arr1.length;i++){
			
			if(arr1[i] == arr2[i]){
			
				continue;
			}
			return (arr1[i]-arr2[i]);
		}
		return 0;
	}
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter String1:");
		String str1 = sc.next();

		System.out.println("Enter String2:");
		String str2 = sc.next();
		
		int ret = mystrcompare(str1,str2);
		if(ret == 0)
			System.out.println("Equal String");
		else
			System.out.println("Notequal String difference is "+ret);

	}
}
