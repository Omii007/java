
class Demo {

	int x = 10;
	private int y = 20;
	
	void fun(){
	
		System.out.println("X = "+x);
		System.out.println("y = "+y);
	}
}
class MainDemo {

	public static void main(String[] args){
	
		Demo obj = new Demo();
		obj.fun();

		System.out.println(obj.x);
		System.out.println(obj.y); // y has private access in Demo
		
		System.out.println(x); // cannot find symbol
		System.out.println(y); // cannot find symbol
	}
}
