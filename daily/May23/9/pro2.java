
class Demo {

	public static void main (String[] args){
	
		String str1 = "Shashi";
		String str2 = new String("Bagal");

		StringBuffer str3 = new StringBuffer("Core2Web");

		StringBuffer str4 = str3.append(str1);
		String str5 = str1.concat(str2);

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
		System.out.println(str4);
		System.out.println(str5);
	}
}
