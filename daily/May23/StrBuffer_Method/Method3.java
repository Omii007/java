
/* Method 3 : public synchronized StringBuffer delete (int start,int end);
 * description: 
 * - Delete character from this <code>stringBuffer</code>.
 * - <code>delete(10,12)</code> will delete 10and 11 ,but not 12.
 * - It is harmless for the end to be larger than length().
 * Parameter : Integer (start the first character to delete).
 * 	       Integer (end the index after the last character to delete).
 * Return Type : StringBuffer (this <code>StringBuffer</code>).
 */

class Delete {

	public static void main (String[] args){
	
		StringBuffer sb = new StringBuffer("Core2Web");

		System.out.println(sb.delete(2,7));
	}
}
