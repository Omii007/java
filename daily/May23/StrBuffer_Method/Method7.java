
/* Method 7 : public synchronized int indexOf (String str, int fromIndex).
 * description : 
 * - Finds the first instance of a String in this StringBuffer, starting at a given index.
 * - If the startting index is less than 0, the search starts at the beginning of this String.
 * - If the starting index is greater than the length of this String, or the substring is not found ,-1 is returned.
 * Parameter : String (str String to find).
 * - Integer (fromIndex index to start the search).
 * Return Type : Integer (location (base 0) of the String, or -1 if not found).
 */


class IndexOf {

	public static void main (String[] args){
	
		StringBuffer str1 = new StringBuffer ("Core2Web");

		System.out.println(str1.indexOf("C",4));
	}
}
