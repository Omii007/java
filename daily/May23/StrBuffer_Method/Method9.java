
/* Method 9 : public synchronized StringBuffer replace (int start, int end , String str);
 * description : 
 * - Replace characters between index <code>start</code> (inclusive) and <code>end</code> (exclusive) with <code>str</code>.
 * - If <code>end</code> is larger than the size of this StringBuffer, all characters after <code>start</code>str</code> are replaced.
 * Parameter : Integer (start the beginnig index of characters to delete (inclusive))
 * 	       Integer (end the index of characters to delete (inclusive))
 *Return Type : StringBuffer (this <code>StringBuffer</code>).
 */

class Replace {

	public static void main (String[] args){
	
		StringBuffer str = new StringBuffer("Know The Code Untill The Core");

		System.out.println(str);
		System.out.println(str.replace(14,20,"Till"));
	}
}
