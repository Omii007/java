
/* Method 8 : public synchronized int lastIndexOf (String str, int fromIndex);
 * description : 
 * - Finds the last instance of a String in this StringBuffer, starting at given index.
 * - If starting index is greater than the maximum valid index, then the search begins at the end of this String.
 * - If the string Index is less than zero ,or the subString is not found . -1 is returned
 * Parameter : String (str String to find).
 * Integer (fromindex index to start the search).
 * Return Type : Integer (location (base 0)of the String, or -1 if not found).
 */

class LastIndex {

	public static void main (String[] args){
	
		StringBuffer str = new StringBuffer("Core2Web");

		System.out.println(str.lastIndexOf("e",3));
	}
}
