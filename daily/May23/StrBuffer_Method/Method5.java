
/* Method 5 : public String toString();
 * description :
 * - Convert this <code>StringBuffer</code> to a <code>String</code>.
 * - The String is composed of the characters currently in this StringBuffer.
 * - Note that the result is a copy ,and that future modifications to this buffer do not affect the String.
 * Parameter : No parameter.
 * Return Type : String (the characters in this StringBuffer).
 */

class TOString {

	public static void main (String[] args){
	
		StringBuffer str1 = new StringBuffer ("Know The Code Till The Core");
		String str2 = "Core2Web";
		String str3 = str1.toString(); // StringBuffer to String
		String str4 = str2.concat(str3);

		System.out.println(str4);
	}
}
