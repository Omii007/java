
/* Method 10 : public synchronized String substring (int beginIndex, int endIndex);
 * description :
 * - Creates a sub string of this StringBuffer , starting at a specified index and ending at once character.
 * - before a specified index.
 * Parameter : Integer (beginIndex index to start at (inclusive,base 0));
 * 	       Integer (endIndex index to end at (exclusive));
 * - String (str the new <code>String</code> to insert).
 * Return Type : String (new String which is a subString of this StringBuffer).
 */

class SubString {

	public static void main (String[] args){
	
		StringBuffer str = new StringBuffer("Know The Code Till The Core");

		//String str2 = str.substring(14,27);
		System.out.println(str.substring(14,27));
	}
}                                                    
