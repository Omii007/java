
/* Method 4 : public synchronized StringBuffer reverse().
 * description :
 * - Reverse the characters in this StringBuffer.
 * - The same sequence of characters exists, but in the reverse index ordering.
 * Parameter : No parameter.
 * Return Type : StringBuffer (this <code>StringBuffer</code>).
 */


class Reverse {

	public static void main (String[] args){
	
		String str1  = "Core2Web";
		StringBuffer str2 = new StringBuffer(str1);

		System.out.println(str2.reverse());
	}
}
