
/* Method 6 : public synchronized int length().
 * description : 
 * - Get the length of the <code>String</code> this <code>StringBuffer</code> would create
 *   Not to be confused with the <em>capacity</em> of the >code>StringBuffer</code>.
 * Parameter : No parameter
 * Return Type : Integer (the length of this <code>StringBuffer</code>).
 */

class Length {

	public static void main (String[] args){
	
		StringBuffer str = new StringBuffer("Core2Web");

		System.out.println(str.length());
	}
}
