
/* Method 2 : public synchronized StringBuffer insert (int offset, String str).
 * description :
 * - Insert the <code>String</code> argument into this <code>StringBuffer</code>.
 * - If str is null ,the String "null" is used instead.
 * Parameter : Integer (offset the place to insert in this buffer).
 * 	       String (str the <code>StringBuffer</code>).
 * Return Type : StringBuffer (this <code>DtringBuffer</code>).
 */

class Insert {

	public static void main(String[] args){
	
		StringBuffer sb = new StringBuffer("ShashiCore2Web");

		System.out.println(sb);
		sb.insert(6,"Bagal");
		
		System.out.println(sb);
	}
}
