
/* Method 1 : public synchronized StringBuffer append (String str);
 * description : 
 * - Append the <code>String</code> to this <code>StringBuffer</code>.
 * - if str is null, the String "null" is appended.
 * Parameter : String (str the <code>String</code> to append).
 * Return Type : StringBuffer (this <code>StringBuffer</code>).
 */

class Append {

	public static void main(String[] args){
	
		StringBuffer str1 = new StringBuffer("Hello");
		String str2 = "World";

		System.out.println(str1.append(str2));
	}
}
