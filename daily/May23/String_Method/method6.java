
/* Method 6 : public boolean equals(Object anObject);
 * description :
 * - Predicate which compares anObject to this.
 * - This is true only for String with the same character sequence.
 * Parameter : Object(anObject)
 * Return Type : boolean
 */

class EqualDemo {

	public static void main(String[] args){
	
		String str1 = "Shashi";
		String str2 = new String("Shashi");
		String str3 = "Bagal";

		System.out.println(str1.equals(str2));  // true
		System.out.println(str1.equals(str3));  // false
	}
}
