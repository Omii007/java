
/* Method 15 : public String toLowerCase();
 * description : it lowercase to this String
 * ex : str.toLowerCase().
 * Parameter : No parameter
 * Return Type : String
 */

class ToLowerCase {

	public static void main(String[] args){
	
		String str = "Core2Web";
		String str1 = "OMKAR";

		System.out.println(str.toLowerCase());
		System.out.println(str1.toLowerCase());
	}
}
