
/* Method 17 : public String trim();
 * description : trims all the white spaces before and after of the String.
 * ex : str.trim();
 * Parameter : No parameter.
 * Return Type : String
 */

class Trim {

	public static void main(String[] args){
	
		String str = "Know The Code Till The Core";

		System.out.println(str.trim());
	}
}
