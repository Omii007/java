
/* Method 2 : public int length().
 * description :-
 * - If return the number of character contained in given String.
 * Parameter : NO parameter
 * Return Type : Integer.
 */

class LengthDemo {

	public static void main(String[] args){
	
		String str1 = "Core2Web";

		System.out.println(str1.length());
	}
}
