
/* Method 4 : public int compareTo(String str2);
 * description :
 * - it compares the str1 and str2 (case sensitive), if both the String are equal ,it returns 0 otherwise return the comparision.
 *ex : str1.comapreTo(str2);
Parameter : String(Second String)
Return Type : integer.
if Both String are equal = 0
if Both String are not equal = difference
*/

class CompareToDemo {

	public static void main(String[] args){
	
		String str1 = "Ashish";
		String str2 = "Ashish";
		String str3 = "Shashi";

		System.out.println(str1.compareTo(str2));  // 0
		System.out.println(str1.compareTo(str3)); // -18
	}
}
