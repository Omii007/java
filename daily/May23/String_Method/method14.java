
/* Method 14 : public String subString(int i,int j);
 * description :
 * - creates a substring of the given String starting at a specified index and ending at one character before the specified index.
 * Parameter : Integer(statrting index),Integer(Ending Index).
 * Return Type : String
 */

class MySubString {

	public static void main(String[] args){
	
		String str = "Core2Web";
		System.out.println(str.substring(5,8));
	}
}
