
/* Method 9 : public boolean endsWith(String suffix).
 * description :
 * - Predicate which determines if the String ends with given suffix.
 *   -if the suffix is an empty String, true is returned.
 *   - throws NULLPointerException if suffix is Null.
 * Parameter : prefix String to compare.
 * return Type : boolean
 */



class EndWith {

	public static void main (String [] args){
	
		String str1 = "Know the code till the core";
		String str2 = "Shashi sir is favourite person";

		System.out.println(str1.endsWith("core"));
		System.out.println(str1.endsWith("till"));
		System.out.println(str2.endsWith("person"));
	}
}
