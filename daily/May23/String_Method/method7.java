
/* Method 7 : boolean equalsIgnoreCase(String anotherString);
 * description : 
 * - compares a String to this String Ignoring case.
 * Parameter : String (str2)
 * Return Type : boolean
 */


class EqualIgnoreCase {

	public static void main(String [] args){
	
		String str1 = "Shashi";
		String str2 = "shahsi";
		String str3 = new String("Shashi");

		System.out.println(str1.equalsIgnoreCase(str2));  // false
		System.out.println(str1.equalsIgnoreCase(str3));  // true
	}
}
