
/* Method 18 : public String[] split(String delimiter);
 * description : Splits this string around matches of regular expressions.
 * Parameter : delimiters (pattern to match)
 * Return Type : String[] (array of split Strings).
 */

class Split {

	public static void main(String[] args){
	
		String str = "Know The Code Till The Core";
		String[] strResult = str.split("");

		for(int i=0;i<strResult.length;i++){
		
			System.out.println(strResult[i]);
		}
	}
}
