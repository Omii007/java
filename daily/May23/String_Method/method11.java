
/* Method 11 : int lastIndexOf(int ch,int fromIndex).
 * description :
 * - finds the last instance of the character in the given String.
 * Parameter : character(ch to find),Integer (Index to start the search).
 * Return Type : Integer
 */


class LastIndexOf {

	public static void main(String[] args){
	
		String str1 = "Shashi";
		String str2 = "Core2Web";

		System.out.println(str1.lastIndexOf('h',2));
		System.out.println(str1.lastIndexOf('h',5));
		System.out.println(str2.lastIndexOf('r',2));
		System.out.println(str2.lastIndexOf('W',7));
	}
}
