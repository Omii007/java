
/* Method 5 : int compareToIgnoreCase(String str);
 * description :
 * - it compares str1 and str2 (case insensitives).
 * Parameter : String
 * Return Type : Integer.
 */

class CompareIgnore {

	public static void main(String [] args){
	
		String str1 = "SHASHI";
		String str2 = "shashikant";
		String str3 = "Sachin";

		System.out.println(str1.compareToIgnoreCase(str2)); // -4
		System.out.println(str1.compareToIgnoreCase(str3)); // 7
	}
}
