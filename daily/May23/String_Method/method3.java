
/* Method 3 : public char charAt(int index);
 * description :
 * - it returns the character located at specified index with the giving String.
 * Parameter : integer (index)
 * Return Type : character
 */

class CharAtDemo {

	public static void main(String[] args){
	
		String str1 = "Core2Web";

		System.out.println(str1.charAt(0));
		System.out.println(str1.charAt(3));
		System.out.println(str1.charAt(6));
		//System.out.println(str1.charAt(8));  // StringindexOutofBond
	}
}
