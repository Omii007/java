
/* Method 10 : public int indexOf(int ch,int fromindex);
 * description :
 * - finds the first instance of the character in the given String.
 * Parameter : character (ch to find),integer(index to start the search).
 * Return Type : Integer
 */


class IndexOf {

	public static void main (String[] args){
	
		String str1 = "Shashi";

		System.out.println(str1.indexOf('a',0));
		System.out.println(str1.indexOf('h',0));
		System.out.println(str1.indexOf('h',2));
	}
}
