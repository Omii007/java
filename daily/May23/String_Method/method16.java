
/* Method 16 : public String toUpperCase();
 * description : it uppercases to this String.
 * ex : str.toUpperCase().
 * Parameter : No parameter
 * Return Type : String
 */

class ToUppperCase {

	public static void main(String[] args){
	
		String str = "core2web";

		System.out.println(str.toUpperCase());
	}
}
