
/* Method 12 : public String replace (char oldChar,char newChar);
 * description :
 * - replaces every instance of a character in the given String with a new character.
 * Parameter : character(old character),character(new character).
 * Return Type : String
 */

class Replace {

	public static void main(String[] args){
	
		String str1 = "Hello World";

		System.out.println(str1.replace('e','i'));
	}
}
