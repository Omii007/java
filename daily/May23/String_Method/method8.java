
/* Method 8 : boolean startsWith(String prefix,int toffset);
 * description :
 * - Predicate which determines if the given String contains the given prefix beginning comparision at toffset.
 * - The results is false if the toffset is negative or greater than str.length().
 *   Parameter : prefix String to compare , tofset offset for this String where the comparision starts
 * Return Type : boolean
 */

class StartWith {

	public static void main (String[] args){
	
		String str1 = "Core2Web";

		System.out.println(str1.startsWith("or",1));  // true
		System.out.println(str1.startsWith("C",0));   // true
							      // String parameter mnun deychi ani techi start jithun hotoy to index parameter mnun deycha.
	}
}
