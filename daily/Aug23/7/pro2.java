

// LinkedList :

import java.util.*;
class LinkedListDemo {

	public static void main(String[] args){
	
		LinkedList ll = new LinkedList();

		ll.add(10);
		ll.addFirst(20);
		ll.add("Omkar");
		ll.addLast(30);

		System.out.println(ll);
		ll.add(2.25);

		System.out.println(ll);
	}
}
