
/* Cursor:
 * 1)Iterator
 * 2)ListIterator
 * 3)Enumeration  -> Vector , Stack var Use kela jato
 * 4)Spliterator  -> Stream
 */

// Iterator:

import java.util.*;

class IteratorDemo {

	public static void main(String[] args){
	
		ArrayList al = new ArrayList();

		al.add("Kanha");
		al.add("Rahul");
		al.add("Ashish");
		al.add("Shashi");
		
		for(var x:al)
			System.out.println(x.getClass().getName());

		Iterator itr = al.iterator();

		while(itr.hasNext()){
		
			if("Rahul".equals(itr.next()))

				itr.remove();

			System.out.println(itr.next());
		}
		System.out.println(itr);

		System.out.println(al);
	}
}
