
// ArrayList:

import java.util.*;
class crickplayer{

	int jerno = 0;
	String name = null;

	crickplayer(int jerno,String name){
	
		this.jerno = jerno;
		this.name = name;
	}
	public String toString(){
	
		return jerno +" : "+name;
	}
}
class ArrayListDemo {

	public static void main(String[] args){
	
		ArrayList al = new ArrayList();

		al.add(new crickplayer(18,"Virat"));
		al.add(new crickplayer(7,"Mahi"));
		al.add(new crickplayer(1,"KL"));
		al.add(new crickplayer(45,"Rohit"));

		System.out.println(al);
	}
}
