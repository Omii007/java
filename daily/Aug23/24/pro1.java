
// Blocking Queue : (Interface)
// 1) ArrayBlocking Queue(C)
// 2) LinkedBlocking Queue(C)
// 3) PriorityBlocking Queue(C)


import java.util.concurrent.*;

class BlockingQueueDemo {

	public static void main(String[] args)throws InterruptedException{
	
		BlockingQueue bQueue = new ArrayBlockingQueue(4);

		bQueue.offer(10);
		bQueue.offer(20);
		bQueue.offer(30);
		bQueue.offer(40);

		System.out.println(bQueue);
		
		bQueue.put(40);  // jevha put he method vapru tevha InterruptedException yete.
		
		System.out.println(bQueue);
	}
}
