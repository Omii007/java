

// LinkedBlocking Queue :
import java.util.*;
import java.util.concurrent.*;
class LinkedBlockingDemo {

	public static void main(String[] args)throws InterruptedException{
	
		BlockingQueue queue = new LinkedBlockingQueue();

		queue.offer(10);
		queue.offer(30);
		queue.offer(20);
		queue.offer(40);
		queue.offer(50);

		System.out.println(queue);

		//size()
		System.out.println(queue.size());
		
		//remainingCapacity()
		System.out.println(queue.remainingCapacity());
		
		//put()
		queue.put(60);
		System.out.println(queue);

		queue.take();
		
		//drainTo
		ArrayList al = new ArrayList();
		queue.drainTo(al);

		System.out.println(al);
		
		System.out.println(queue);

		//contains()
		System.out.println(queue.contains(al));

		//Iterator
		Iterator itr = queue.iterator();

		while(itr.hasNext()){
		
			System.out.println(itr.next());
		}

	}
}
