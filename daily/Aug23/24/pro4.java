
// Producer Consumer Problem :

import java.util.concurrent.*;

class Producer implements Runnable {

	BlockingQueue queue = null;

	Producer(BlockingQueue queue){
	
		this.queue = queue;
	}
	public void run(){
	
		for(int i=1;i<=10;i++){
			try{
				queue.put(i);
				System.out.println("Producer "+i);
			}catch(InterruptedException ie){
			
			}
			try{
			
				Thread.sleep(1000);
			}catch(InterruptedException ie){
			
			}
		}
	}
}
class Consumer implements Runnable {

	BlockingQueue queue = null;

	Consumer(BlockingQueue bqueue){
	
		this.queue = queue;
	}
	public void run(){
	
		for(int i=1;i<=10;i++){
			try{
				queue.take();
				System.out.println("consumer " + i);
			}catch(InterruptedException ie){
			
			}
			try{
			
				Thread.sleep(7000);
			}catch(InterruptedException ie){
			
			}
		}
	}
}

class BlockingDemo {

	public static void main(String[] args){
	
		BlockingQueue queue = new ArrayBlockingQueue(10);

		Producer producer = new Producer(queue);
		Consumer consumer = new Consumer(queue);

		Thread pthread = new Thread(producer);
		Thread cthread = new Thread(consumer);

		pthread.start();
		cthread.start();
	}
}
