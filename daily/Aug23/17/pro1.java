
/* Map : (Interface)	
 * - Hashmap(C) -> LinkedHashmap (C)
 * - IdentityHashmap
 * - WeakHashmap
 * - Sortedmap (I) -> Navigablemap (I) -> Treemap (C)
 * -(Object (C)) -> Dictionary (C) -> Hashtable (C) -> Properties (C)
 */

// Hash map

import java.util.*;

class HashmapDemo {

	public static void main(String[] args){
	
		HashSet hs = new HashSet();

		hs.add("Kanha");
		hs.add("Badhe");
		hs.add("Ashish");
		hs.add("Rahul");

		System.out.println(hs);

		//Hashmap
		HashMap hm = new HashMap();

		hm.put("Kanha","BMC");	
		hm.put("Badhe","Carpro");	
		hm.put("Ashish","Barclays");	
		hm.put("Rahul","BMC");	

		System.out.println(hm);
	}
}
