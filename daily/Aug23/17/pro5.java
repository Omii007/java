
// IdentityHashMap :

import java.util.*;

class IdentityHashMapDemo {

	public static void main(String[] args){
	
		IdentityHashMap hm = new IdentityHashMap();

		hm.put("Kanha",new Integer(10));
		hm.put("Rahul",new Integer(20));
		hm.put("Badhe",new Integer(30));

		System.out.println(hm);
	}
}
