
// Methods of HashMap(Class)

import java.util.*;

class HashMapDemo {

	public static void main(String[] args){
	
		HashMap hm = new HashMap();

		hm.put("Java",".java");
		hm.put("python",".py");
		hm.put("Dart",".dart");

		System.out.println(hm);

		//get
		System.out.println(hm.get("python"));

		//keySet
		System.out.println(hm.keySet());

		//values
		System.out.println(hm.values());

		//entrySet
		System.out.println(hm.entrySet());
	}
}
