
import java.util.*;

class CricPlayer {

	String name = null;
	int JerNo = 0;

	CricPlayer(String name,int JerNo){
	
		this.name = name;
		this.JerNo = JerNo;
	}
	public String toString(){
	
		return "{ "+ name +":" + JerNo +" }";
	}
}
class HashSetDemo {

	public static void main(String[] args){
	
		HashSet hs = new HashSet();

		hs.add(new CricPlayer("Virat",18));
		hs.add(new CricPlayer("ABD",17));
		hs.add(new CricPlayer("Mahi",7));
		hs.add(new CricPlayer("Rohit",45));
		hs.add(new CricPlayer("ABD",17));

		System.out.println(hs);
	}
}
