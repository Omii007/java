

import java.util.*;

class Myclass implements Comparable {

	String name = null;

	Myclass(String name){
	
		this.name = name;
	}
	public String toString(){
	
		return name;
	}
	public int compareTo(Object obj){
	
		return (name.compareTo(((Myclass)obj).name));
	} 
}
class TreeSetDemo {

	public static void main(String[] args){
	
		TreeSet ts = new TreeSet();

		ts.add(new Myclass("Kanha"));
		ts.add(new Myclass("Badhe"));
		ts.add(new Myclass("Ashish"));
		ts.add(new Myclass("Rahul"));

		System.out.println(ts);
	}
}
