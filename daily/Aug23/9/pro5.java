
import java.util.*;

class CricPlayer {

	String name = null;
	int JerNo = 0;

	CricPlayer(String name,int JerNo){
	
		this.name = name;
		this.JerNo = JerNo;
	}
	public String toString(){
	
		return "{ "+ name +":" + JerNo +" }";
	}
}
class LinkedHashSetDemo {

	public static void main(String[] args){
	
		LinkedHashSet hs = new LinkedHashSet();

		hs.add(new CricPlayer("Virat",18));
		hs.add(new CricPlayer("ABD",17));
		hs.add(new CricPlayer("Mahi",7));
		hs.add(new CricPlayer("Rohit",45));
		hs.add(new CricPlayer("ABD",17));

		System.out.println(hs);
	}
}
