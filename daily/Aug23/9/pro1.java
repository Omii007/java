
/* Set:(Interface)
 *   
 *   1)HashSet (class)				1)SortedSet (Interface)
 *   2)LinkedHashSet  (class)			2)Navigable Set (Interface)
 *   						3)TreeSet (class)
 */

// HashSet:

import java.util.*;

class HashSetDemo {

	public static void main(String[] args){
	
		HashSet hs = new HashSet();

		hs.add("Kanha");
		hs.add("Rahul");
		hs.add("Badhe");
		hs.add("Ashish");
		hs.add("Rahul");
		hs.add("Ashish");

		System.out.println(hs.getClass().getName());
		System.out.println(hs);
	}
}
