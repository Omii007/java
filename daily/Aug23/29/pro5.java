
import java.io.*;

class fileMethod {
	
	public static void main(String[] args)throws IOException{
	
		File obj1 = new File("Incubator.txt");

		obj1.createNewFile();

		//getName()
		System.out.println(obj1.getName());
		
		//getParent()
		System.out.println(obj1.getParent());
		
		//getPath()
		System.out.println(obj1.getPath());
		
		//getAbsolutePath()
		System.out.println(obj1.getAbsolutePath());
		
		//canRead()
		System.out.println(obj1.canRead());
		
		//canWrite()
		System.out.println(obj1.canWrite());
		
		//isDirectory()
		System.out.println(obj1.isDirectory());
		
		//isFile()
		System.out.println(obj1.isFile());
		
		//list()
		System.out.println(obj1.list());
		
		//lastModified()
		System.out.println(obj1.lastModified());

		//compareTo
		//delete
	}
}
