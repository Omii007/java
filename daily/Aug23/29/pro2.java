

import java.io.*;

class fileDemo {

	public static void main(String[] args)throws IOException{
	
		File obj1 = new File("FileHandling");

		obj1.mkdir(); // to create folder

		File obj2 = new File("FileHandling","Code1");

		obj2.mkdir();

		File obj3 = new File(obj1,"Code1.txt");

		obj3.createNewFile();

		File obj4 = new File(obj1,"Code2.txt");
		obj4.createNewFile();
	}
}
