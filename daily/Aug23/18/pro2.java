
// Comparable Interface

import java.util.*;

class platform implements Comparable {

	String str = null;
	int foundyear = 0;

	platform(String str,int foundyear){
	
		this.str = str;
		this.foundyear = foundyear;
	}
	public String toString(){
	
		return "{" + str + ":" + foundyear + "}";
	}
	public int compareTo(Object obj){
	
		return foundyear - ((platform)obj).foundyear;
	}
}
class TreeMapDemo {

	public static void main(String[] args){
	
		TreeMap tm = new TreeMap();

		tm.put(new platform("Youtube",2015),"Google");
		tm.put(new platform("Instagram",2010),"Meta");
		tm.put(new platform("Facebook",2004),"Meta");
		tm.put(new platform("ChatGPT",2022),"OpemAI");
		
		System.out.println(tm);
	}
}
