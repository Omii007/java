
// Iterating OverMap :


import java.util.*;

class IterateMapDemo {

	public static void main(String[] args){
	
		SortedMap tm = new TreeMap();

		tm.put("Ind","India");
		tm.put("Pak","Pakistan");
		tm.put("SL","SriLanka");
		tm.put("Ban","Bangladesh");
		tm.put("Aus","Australia");

		System.out.println(tm);

		Set data = tm.entrySet();

		Iterator<Map.Entry>  itr = data.iterator();

		while(itr.hasNext()){
		
			Map.Entry abc = itr.next();
			System.out.println(abc.getKey() + ":" + abc.getValue());
		}
	}
}
