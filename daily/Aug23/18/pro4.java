

// SortedMap :

// Methods

import java.util.*;

class TreeMapDemo {

	public static void main(String[] args){
	
		SortedMap tm = new TreeMap();

		tm.put("Ind","India");
		tm.put("Pak","Pakistan");
		tm.put("SL","SriLanka");
		tm.put("Aus","Australia");
		tm.put("Ban","Bangladesh");

		System.out.println(tm);

		//subMap()
		System.out.println(tm.subMap("Aus","Pak"));

		//headMap()
		System.out.println(tm.headMap("Pak"));
		
		//tailMap()
		System.out.println(tm.tailMap("Pak"));
		
		//firstkey()
		System.out.println(tm.firstKey());
		
		//lastkey()
		System.out.println(tm.lastKey());
		
		//values()
		System.out.println(tm.values());
		
		//entrySet()
		System.out.println(tm.entrySet());
	}
}
