

// Dictionary :

import java.util.*;

class HashtableDemo {

	public static void main(String[] args){
	
		Dictionary ds = new Hashtable();

		ds.put(10,"Sachin");
		ds.put(18,"Virat");
		ds.put(7,"MSD");
		ds.put(1,"KL Rahul");
		ds.put(45,"Rohit");

		System.out.println(ds);

		//size()
		System.out.println(ds.size());
		
		//isEmpty()
		System.out.println(ds.isEmpty());
		
		//keys()
		Enumeration itr1 = ds.keys();
		while(itr1.hasMoreElements()){
		
		System.out.println(itr1.nextElement());
		}

		//elements()

		Enumeration itr2 = ds.elements();

		while(itr2.hasMoreElements()){
		
			System.out.println(itr2.nextElement());
		}

		//get()
		System.out.println(ds.get(10));

		// remove()
		ds.remove(1);
		System.out.println(ds);
		
	}
}
