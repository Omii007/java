
// Comparator Interface

import java.util.*;

class platform {

	String str = null;
	int foundyear = 0;

	platform(String str,int foundyear){
	
		this.str = str;
		this.foundyear = foundyear;
	}
	public String toString(){
	
		return "{" + str + ":" + foundyear + "}";
	}
}
class SortByName implements Comparator {

	public int compare(Object obj1,Object obj2){
	
		return ((platform)obj1).str.compareTo(((platform)obj2).str);
	}
}
class TreeMapDemo {

	public static void main(String[] args){
	
		TreeMap tm = new TreeMap (new SortByName());
// Map madhe ekhada class cha object banvto tevhach aapn parameter deun thevyacha jo class comparator la implements krtoy. 

		tm.put(new platform("Youtube",2005),"Google");
		tm.put(new platform("Instagram",2010),"Meta");
		tm.put(new platform("Facebook",2004),"Meta");
		tm.put(new platform("ChatGPT",2022),"OpenAI");
		
		
		System.out.println(tm);
	}
}
