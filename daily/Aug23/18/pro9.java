

import java.util.*;
import java.io.*;
class PropDemo {

	public static void main(String[] args)throws IOException{
	
		Properties pop = new Properties();

		FileInputStream obj = new FileInputStream("Team.properties");

		pop.load(obj);

		String name = pop.getProperty("Rohit_Sharma");

		System.out.println(name);

		pop.setProperty("Rohit_Sharma","It is a Captain of Team India");

		FileOutputStream fobj = new FileOutputStream("Team.properties");

		pop.store(fobj,"Updated By Omkar");
	} 
}
