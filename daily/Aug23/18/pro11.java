
// NavigableMap :

import java.util.*;

class NavigableMapDemo {

	public static void main(String[] args){
	
		NavigableMap nm = new TreeMap();

		nm.put(10,"Sachin");
		nm.put(7,"MSD");
		nm.put(18,"Virat");
		nm.put(45,"Rohit");
		nm.put(17,"ABD");

		System.out.println(nm);

		//lowerEntry(K)
		System.out.println(nm.lowerEntry(18));
		
		//lowerKey(K)
		System.out.println(nm.lowerKey(18));
		
		//floorEntry(K)
		System.out.println(nm.floorEntry(18));
		
		//lowerKey(K)
		System.out.println(nm.lowerKey(18));
		
		//ceilingEntry(K)
		System.out.println(nm.ceilingEntry(18));
		
		//ceilingKey(K)
		System.out.println(nm.ceilingKey(20));
		
		//higherEntry(K)
		System.out.println(nm.higherEntry(18));
		
		//higherKey(K)
		System.out.println(nm.higherKey(20));
		
		//firstEntry(K)
		System.out.println(nm.firstEntry());
		
		//lastEntry(K)
		System.out.println(nm.lastEntry());

		//PollFirstEntry
		System.out.println(nm.pollFirstEntry());
		
		//PollLastEntry
		System.out.println(nm.pollLastEntry());

		// descendingMap()
		
	}
}
