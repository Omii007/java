
// Properties :

import java.util.*;
import java.io.*;

class PropertiesDemo {

	public static void main(String[] args)throws IOException{
	
		Properties obj = new Properties();

		FileInputStream fobj = new FileInputStream("Friends.properties");

		obj.load(fobj);

		String name = obj.getProperty("Ashish");

		System.out.println(name);
		
		obj.setProperty("Shashi","Biencaps");
		obj.setProperty("Ashish","CarPro");

		FileOutputStream outobj = new FileOutputStream("Friends.properties");

		obj.store(outobj,"Updated By Shashi");
		obj.store(outobj,"Updated By Omkar");
	}
}
