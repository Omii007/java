

// NavigableSet :
// Methods

import java.util.*;

class NavigableSetDemo {

	public static void main(String[] args){
	
		NavigableSet nv = new TreeSet();

		nv.add(10);
		nv.add(18);
		nv.add(7);
		nv.add(1);
		nv.add(45);
		nv.add(43);

		System.out.println(nv);

		//lower(E)
		System.out.println(nv.lower(7));
		
		//floor(E)
		System.out.println(nv.floor(22));
		
		//ceiling(E)
		System.out.println(nv.ceiling(22));
		
		//higher(E)
		System.out.println(nv.higher(45));
		
		//PollFirst(E)
		System.out.println(nv.pollFirst());
		
		//PollLast(E)
		System.out.println(nv.pollLast());
		
		//Iterator()
		
		System.out.println("--------");
		Iterator itr = nv.iterator();

		while(itr.hasNext()){
		
			System.out.println(itr.next());
		}
		// descendingSet()
		/*NavigableSet itr1 = nv.descendingSet();

		while(itr1.hasNext()){
		
			System.out.println(itr1.next());
		}*/
		// descendingIterator()
		Iterator itr2 = nv.descendingIterator();

		while(itr2.hasNext()){
		
			System.out.println(itr2.next());
		}
		// subSet()
		
	}
}
