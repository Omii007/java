/* List-(Interface):
 * 1) Duplicate data allow
 * 2) Insertion order return
 *
 *4 Main class
     1) ArrayList
     2)LinkedList
     3)Vector
     	Stack
*/

// ArrayList:
import java.util.*;

class ArrayListDemo extends ArrayList{

	public static void main(String[] args){

		ArrayListDemo obj = new ArrayListDemo();

		obj.add(10);
		obj.add(20.5f);
		obj.add("Omkar");
		obj.add(10);
		obj.add(20.5f);
		
		ArrayList obj2 = new ArrayList();

		obj2.add("Salman");
		obj2.add("Shahrukh");
		obj2.add("Amir");
		System.out.println(obj);
		//add(Element)
		obj.add(3,"Core2Web");

		System.out.println(obj);

		//int size()
		System.out.println(obj.size());

		//boolean contains(Object)
		System.out.println(obj.contains("Omkar"));
		System.out.println(obj.contains(30));

		//int indexOf(Object,int,int)
		System.out.println(obj.indexOf("Omkar"));

		//int lastIndexOf(Object)
		System.out.println(obj.lastIndexOf(20.5f));

		//E get(int)
		System.out.println(obj.get(3));
		System.out.println(obj);

		//E set(int)
		System.out.println(obj.set(3,"Incubator"));
		System.out.println(obj);

		//void add(int,Element)
		obj.add(3,"Core2Web");
		System.out.println(obj);

		//boolean addAll
		obj.addAll(obj2);
		System.out.println(obj);
		
		//boolean addAll(int,Object)
		obj.addAll(3,obj2);
		System.out.println(obj);

		//java.lang.Object[] toArray()
		Object arr[] = obj.toArray();
		System.out.println(arr);

		for(Object data : arr)
			System.out.print(data+" ");

		System.out.println();

		// E remove(int)
		System.out.println(obj.remove(3));
		System.out.println(obj);

		//boolean remove(Object)
		System.out.println(obj.remove(obj2));

		//void removeRange(int,int)
		obj.removeRange(3,5);
		System.out.println(obj);

		//void clear()
		obj.clear();
		System.out.println(obj);
	}
}
