

// Iterator:

import java.util.*;

class CursorDemo {

	public static void main(String[] args){
	
		ArrayList al = new ArrayList();

		al.add("Kanha");
		al.add("Rahul");
		al.add("Badhe");
		al.add("Ashish");
		al.add("Shashi");

		for(var x : al)
			System.out.println(x.getClass().getName());
		
		System.out.println(al);
		//Class              method
		Iterator Cursor = al.iterator();

		while(Cursor.hasNext()){
			if("Kanha".equals(Cursor.next()))

				Cursor.remove();
		}
		System.out.println(al);

		//ListIterator

		ListIterator litr = al.listIterator();

		while(litr.hasNext()){
		
			System.out.println(litr.next());
		}

		while(litr.hasPrevious()){
		
			System.out.println(litr.previous());
		}
		//Enumeration

		Enumeration cursor = al.elements();

		while(cursor.hasMoreElements()){
		
			System.out.println(cursor.nextElement());
		}

	}
}
