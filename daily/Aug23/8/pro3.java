
// Cursor:
// 1)Iteration:

import java.util.*;

class CursoeDemo {

	public static void main(String[] args){
	
		ArrayList al = new ArrayList();
		
		al.add(10);		
		al.add(20);		
		al.add(30);		
		al.add(40);
		
		for(var x:al){
		
			System.out.println(x.getClass().getName());
		}
		//ListItertor
		ListIterator litr = al.listIterator();
		
		while(litr.hasNext()){
		
			System.out.println(litr.next());
		}
		while(litr.hasPrevious()){
		
			System.out.println(litr.previous());
		}		
	}
}
