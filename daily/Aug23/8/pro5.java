

// Iterator:

import java.util.*;

class CursorDemo {

	public static void main(String[] args){
	
		Stack al = new Stack();

		al.add("Kanha");
		al.add("Rahul");
		al.add("Badhe");
		al.add("Ashish");
		al.add("Shashi");
		//Enumeration

		Enumeration cursor = al.elements();

		while(cursor.hasMoreElements()){
		
			System.out.println(cursor.nextElement());
		}

	}
}
