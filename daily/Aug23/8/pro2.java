

// Stack:

import java.util.*;
class stackDemo {

	public static void main(String[] args){
	
		Stack s = new Stack();
		System.out.println(s.empty());

		s.add(10);
		s.push(20);
		s.push(30);
		s.push(40);
		s.push(50);

		System.out.println(s.peek());
		System.out.println(s);

		System.out.println(s.pop());
		System.out.println(s.empty());
		System.out.println(s);
	}
}
