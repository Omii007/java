

import java.util.*;

class Movies {

	String moviename = null;
	double totcoll = 0.0;
	float imdRating = 0.0f;

	Movies(String moviename,double totcoll,float imdRating){
	
		this.moviename = moviename;
		this.totcoll = totcoll;
		this.imdRating = imdRating;
	}
	public String toString(){
	
		return "{ " +moviename+":"+totcoll+":"+imdRating+" }";
	}
}
class SortByName implements Comparator {

	public int compare(Object obj1,Object obj2){
	
		return ((Movies)obj1).moviename.compareTo(((Movies)obj2).moviename);
	}
}
class SortByColl implements Comparator {

	public int compare(Object obj1,Object obj2){
	
		return (int)(((Movies)obj1).totcoll) -(int)(((Movies)obj2).totcoll);
	}
}
class SortByRating implements Comparator {

	public int compare(Object obj1,Object obj2){
	
		return (int)(((Movies)obj1).imdRating) - (int)(((Movies)obj2).imdRating);
	}
}
class UserListDemo {

	public static void main(String[] args){
	
		ArrayList al = new ArrayList();

		al.add(new Movies("RHTDM",200.00,8.8f));
		al.add(new Movies("VED",75.00,7.5f));
		al.add(new Movies("SAIRAT",100.00,8.9f));
		al.add(new Movies("BAJRANGI",500.00,9.9f));

		System.out.println(al);

		Collections.sort(al,new SortByName());
		
		System.out.println(al);

		Collections.sort(al,new SortByColl());
		
		System.out.println(al);

		Collections.sort(al,new SortByRating());
		
		System.out.println(al);
	}
}
