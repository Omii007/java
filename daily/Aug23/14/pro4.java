
import java.util.*;

class Employee {

	String empname = null;
	float sal = 0.0f;

	Employee (String empname,float sal){
	
		this.empname = empname;
		this.sal = sal;
	}
	public String toString (){
	
		return "{ "+empname + ":"+sal +" }";
	}
}
class SortByName implements Comparator <Employee>{

	public int compare(Employee obj1,Employee obj2){
		
		return obj1.empname.compareTo(obj2.empname);
	}
}
class SortBySal implements Comparator <Employee> {

	public int compare(Employee obj1,Employee obj2){
	
		return (int)(obj1.sal - obj2.sal);
	}
}
class ListSortDemo {

	public static void main(String[] args){
	
		ArrayList <Employee> al = new ArrayList<Employee>();

		al.add(new Employee("Kanha",200000.0f));
		al.add(new Employee("Ashish",250000.0f));
		al.add(new Employee("Badhe",175000.0f));
		al.add(new Employee("Rahul",150000.0f));

		System.out.println(al);

		Collections.sort(al,new SortByName());

		System.out.println(al);
		
		Collections.sort(al,new SortBySal());

		System.out.println(al);
	}
}
