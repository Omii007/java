

// RealTime Example

import java.util.*;

class Cars implements Comparable {

	String carname = null;
	double cost = 0.0;

	Cars(String carname,double cost){
	
		this.carname = carname;
		this.cost = cost;
	}
	public String toString(){
	
		return "{ "+carname +":" + cost +" }";
	}
	public int compareTo(Object obj){
	
		return (carname.compareTo(((Cars)obj).carname));
	}
}
class TreeSetDemo {

	public static void main(String[] args){
	
		TreeSet ts = new TreeSet();

		ts.add(new Cars("BMW",10000000.00));
		ts.add(new Cars("Audio",7000000.00));
		ts.add(new Cars("MERCDIES",5000000.00));
		ts.add(new Cars("FORTUNER",4500000.00));

		System.out.println(ts);
	}
}
