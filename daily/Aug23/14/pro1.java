

// TreeSet

import java.util.*;

class Movies implements Comparable{

	String moviename = null;
	float totcoll = 0.0f;

	Movies (String moviename,float totcoll){
	
		this.moviename = moviename;
		this.totcoll = totcoll;
	}
	public String toString(){
	
		return "{ " + moviename +":" + totcoll+" }";
	}
	public int compareTo(Object obj){
	
		return (moviename.compareTo(((Movies)obj).moviename));
	}
}
class treeSetDemo {

	public static void main(String[] args){
	
		TreeSet ts = new TreeSet();

		ts.add(new Movies("Gadar",150.00f));
		ts.add(new Movies("OMG2",120.00f));
		ts.add(new Movies("Jailer",250.00f));
		ts.add(new Movies("Jailer",250.00f));

		System.out.println(ts);
	}
}
