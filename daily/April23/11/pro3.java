
import java.io.*;
import java.util.*;

class Demo1 {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		String str = br.readLine();

		StringTokenizer st = new StringTokenizer(str," ");

		System.out.println("Token Count = "+st.countTokens());

		while(st.hasMoreTokens()){
		
			System.out.println(st.nextToken());
		}
	}
}
