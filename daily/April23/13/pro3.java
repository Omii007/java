
class Demo {

	public static void main(String[] args){
	
		Demo obj = new Demo();
		obj.fun(10);
		obj.fun(20.5f);
		obj.fun('A');
		obj.fun(true); // incompitable data type :boolean can not converted to float 
	}
	void fun(float x){
	
		System.out.println("In fun");
		System.out.println(x);
	}
}
