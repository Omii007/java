
class ArrayDemo {

	static void fun(int arr[]){
	
		for(int x : arr)
			System.out.print(x+" ");

		System.out.println();

		for(int i=0;i<arr.length;i++){
		
			arr[i] = arr[i] + 50;
		}
	}
	public static void main(String[] args){
	
		int arr[] = {10,20,30,40};

		fun(arr);
		
		for(int x : arr)
			System.out.print(x+" ");

		System.out.println();
	}
}
