
class ArrayDemo {

	static void fun(int arr[]){
	
		for(int x : arr)
			System.out.print(x+" ");

		System.out.println();
	}
	public static void main(String[] args){
	
		int arr[] = {10,20,30,40};

		for(int x : arr)
			System.out.print(x+" ");

		System.out.println();

		fun(arr);
	}
}
