
// Accessing Elements Arom An Array :

class ArrayDemo {

	public static void main(String[] args){
	
		int arr[] = {10,20,30,40,50};

		System.out.print(arr[0]+" ");
		System.out.print(arr[1]+" ");
		System.out.print(arr[2]+" ");
		System.out.print(arr[3]+" ");
		System.out.print(arr[4]+" ");

		System.out.println();
		System.out.println("For Loop:");

		for(int i=0;i<arr.length;i++)
			System.out.print(arr[i]+" ");

		System.out.println();
	}
}
