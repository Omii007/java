
class RCB {

	public static void main(String[] args){
	
		int arr1[] = {10,20,30,40,50};
		char arr2[] = {'A','B','C'};
		float arr3[] = {10.5f,20.5f};
		boolean arr4[] = {true,false,true};

		System.out.println("Integer array:");

		System.out.print(arr1[0]+" ");
		System.out.print(arr1[1]+" ");
		System.out.print(arr1[2]+" ");
		System.out.print(arr1[3]+" ");
		System.out.print(arr1[4]+" ");

		System.out.println();

		System.out.println("Character array:");

		System.out.print(arr2[0]+" ");
		System.out.print(arr2[1]+" ");
		System.out.print(arr2[2]+" ");

		System.out.println();
		System.out.println("Float array:");

		System.out.print(arr3[0]+" ");
		System.out.print(arr3[1]+" ");

		System.out.println();
		System.out.println("Boolean array:");

		System.out.print(arr4[0]+" ");
		System.out.print(arr4[1]+" ");
		System.out.print(arr4[2]+" ");
		
		System.out.println();
	}
}
