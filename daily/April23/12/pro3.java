
import java.util.*;

class Add {

	static void add (int a,int b){
	
		int ans = a + b;

		System.out.println("Add = "+ans);
	}
	static void sub (int a,int b){
	
		int ans = a - b;

		System.out.println("Sub = "+ans);
	}
	static void mult(int a,int b){
	
		int ans = a * b;

		System.out.println("Mult = "+ans);
	}
	static void div (int a,int b){
	
		int ans = a / b;

		System.out.println("Div = "+ans);
	}
	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter int value");

		int a = sc.nextInt();
		int b = sc.nextInt();

		add(a,b);
		sub(a,b);
		mult(a,b);
		div(a,b);
	}
}
