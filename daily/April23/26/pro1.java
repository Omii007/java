
class Demo {

	void fun(int [] arr){
	
		arr[1] = 70;
		arr[2] = 80;
	}
	public static void main(String[] args){
	
		int arr[] = {10,20,30,40};
		Demo obj = new Demo();

		obj.fun(arr);

		for(int x : arr)
			System.out.print(x+" ");

		System.out.println();
		for(int x : arr)
			System.out.println(System.identityHashCode(x));

		int x = 70;
		int y = 80;

		System.out.println(System.identityHashCode(x));
		System.out.println(System.identityHashCode(y));

	}
}
