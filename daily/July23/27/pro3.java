
import java.util.concurrent.*;

class MyThread implements Runnable{

	int num;

	MyThread(int num){
	
		this.num = num;
	}
	public void run(){
	
		System.out.println(Thread.currentThread()+" Start Thread : "+num);
		DailyTask();
		System.out.println(Thread.currentThread()+" End Thread :"+num);
	}
	void DailyTask(){
	
		try{
		
			Thread.sleep(8000);
		}catch(InterruptedException obj){
		
			System.out.println(obj.toString());
		}
	}
}
class ThreadPoolDemo {

	public static void main(String[] args){
	
		ExecutorService ser = Executors.newFixedThreadPool(5);

		for(int i=1;i<=6;i++){
		
			MyThread obj = new MyThread(i);
			ser.execute(obj);
		}
		ser.shutdown();
	}
}
