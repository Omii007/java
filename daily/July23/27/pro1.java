
// ThreadGroup

class MyThread extends Thread {

	MyThread(ThreadGroup tg,String str){
	
		super(tg,str);
	}
	public void run(){
	
		System.out.println(Thread.currentThread());

		try {
		
			Thread.sleep(3000);
		}catch(InterruptedException obj){
		
			System.out.println(obj.toString());
		}
	}
}
class ThreadGroupDemo {

	public static void main(String[] args)throws InterruptedException{
	
		ThreadGroup pThreadGP = new ThreadGroup("India");

		MyThread obj1 = new MyThread(pThreadGP,"Maha");
		MyThread obj2 = new MyThread(pThreadGP,"Goa");

		obj1.start();
		obj2.start();
		
		ThreadGroup cThreadGP = new ThreadGroup(pThreadGP,"Pakistan");

		MyThread obj3 = new MyThread(cThreadGP,"Karachi");
		MyThread obj4 = new MyThread(cThreadGP,"Lahore");

		obj3.start();
		obj4.start();

		ThreadGroup cThreadGP1 = new ThreadGroup(pThreadGP,"Bangladesh");

		MyThread obj5 = new MyThread(cThreadGP1,"Dhaka");
		MyThread obj6 = new MyThread(cThreadGP1,"Mirpur");

		obj5.start();
		obj6.start();
		
		Thread.sleep(5000);
		System.out.println("Thread Active Count "+pThreadGP.activeCount());
		System.out.println("ThreadGroup Active Count "+pThreadGP.activeGroupCount());
	}
}
