
// Deadlock

class MyThread extends Thread {

	static Thread nMain = null;
	public void run(){
		System.out.println(getName());
		try{
		
			nMain.join();
		}catch(InterruptedException obj){
		
		
		}
		for(int i=0;i<10;i++){
		
			System.out.println("In Thread-0");
		}
	}
}
class ThreadDemo {

	public static void main(String[] args)throws InterruptedException{
	
		MyThread.nMain = Thread.currentThread();

		MyThread obj = new MyThread();
		obj.start();

		obj.join();

		for(int i=0;i<10;i++){
		
			System.out.println("In Main");
		}
	}
}
