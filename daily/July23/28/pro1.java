
import java.util.concurrent.*;
class MyThread implements Runnable{

	int num;
	MyThread(int num){
	
		this.num = num;
	}
	public void run(){
	
		System.out.println(Thread.currentThread()+" Start Thread "+num);
		dailyTask();
		System.out.println(Thread.currentThread()+" End Thread "+num);
	}
	void dailyTask(){
	
		try{
		
			Thread.sleep(40000);
		}catch(InterruptedException obj){
		
		}
	}
}
class ThreadpoolDemo {

	public static void main(String[] args){
	
		ExecutorService obj = Executors.newFixedThreadPool(5);

		for(int i=1;i<=5;i++){
		
			MyThread obj1 = new MyThread(i);
			obj.execute(obj1);
		}
		obj.shutdown();

	}
}
