
// Priority of Threads

class MyThread extends Thread {

	public void run(){
	
		Thread t = Thread.currentThread();
		System.out.println(t.getPriority());
		System.out.println("Id :"+t.getId());
		System.out.println("Name :"+t.getName());
	}
}
class ThreadDemo {

	public static void main(String[] args){
	
		Thread t = Thread.currentThread();
		System.out.println(t.getPriority());
		
		System.out.println("Id :"+t.getId());
		System.out.println("Name :"+t.getName());
		MyThread obj1 = new MyThread();
		obj1.start();

		t.setPriority(7);

		MyThread obj2 = new MyThread();
		obj2.start();
	}
}
