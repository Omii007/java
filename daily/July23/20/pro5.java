
class MyThread extends Thread {

	public void run(){
	
		Thread t = Thread.currentThread();
		System.out.println("Name : "+t.getName());
		System.out.println("Priority : "+t.getPriority());
	}
}
class ThreadDemo {

	public static void main(String[] args){
	
		Thread t = Thread.currentThread();
		System.out.println("Name : "+t.getName());
		System.out.println("Priority : "+t.getPriority());

		MyThread obj1 = new MyThread();
		obj1.start();
		obj1.start();

	}
}
