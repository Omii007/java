
class MyThread extends Thread {

	MyThread(ThreadGroup tg,String str){
	
		super(tg,str);
	}
	public void run(){
	
		System.out.println(Thread.currentThread());
	}

}
class ThreadGroupDemo {

	public static void main(String[] args)throws InterruptedException{
	
		//Parent ThreadGroup
		ThreadGroup pThreadGP = new ThreadGroup("C2W");

		MyThread obj1 = new MyThread(pThreadGP,"c");
		MyThread obj2 = new MyThread(pThreadGP,"java");
		MyThread obj3 = new MyThread(pThreadGP,"c++");

		obj1.start();
		obj2.start();
		obj3.start();

		//Child ThreadGroup
		ThreadGroup cThreadGP = new ThreadGroup(pThreadGP,"Incubator");

		MyThread obj4 = new MyThread(cThreadGP,"flutter");
		MyThread obj5 = new MyThread(cThreadGP,"springboot");
		MyThread obj6 = new MyThread(cThreadGP,"Android");
		
		Thread.sleep(1000);
		System.out.println("====== Child ThreadGroup ======");
		obj4.start();
		obj5.start();
		obj6.start();
	}
}
