	
class MyThread extends Thread {

	MyThread(ThreadGroup tg,String str){
	
		super(tg,str);
	}
	public void run(){
	
		System.out.println(Thread.currentThread());
	}
}
class ThreadGroupDemo {

	public static void main(String[] args){
		
		// Parent ThreadGroup
		ThreadGroup pThreadGP = new ThreadGroup("Test");

		MyThread obj = new MyThread(pThreadGP,"Rohit");
		obj.start();
		
		System.out.println("Active Thread Count : "+obj.activeCount());
		System.out.println("Active ThreadGroup Count : "+pThreadGP.activeGroupCount());
		
		// Child1 ThreadGroup
		ThreadGroup cThreadGP = new ThreadGroup(pThreadGP,"T-20");

		MyThread obj1 = new MyThread(cThreadGP,"Mahi");
		obj1.start();
		
		System.out.println("Active Thread Count : "+obj.activeCount());
		System.out.println("Active ThreadGroup Count : "+pThreadGP.activeGroupCount());
		
		// Child2 ThreadGroup
		ThreadGroup c1ThreadGP = new ThreadGroup(pThreadGP,"One-Day");

		MyThread obj2 = new MyThread(c1ThreadGP,"Shikhar");
		obj2.start();
		
		System.out.println("Active Thread Count : "+obj.activeCount());
		System.out.println("Active ThreadGroup Count : "+pThreadGP.activeGroupCount());
	}
}
