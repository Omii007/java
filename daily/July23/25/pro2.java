
// Thread Group

class MyThread extends Thread {

	MyThread(String str){
	
		super(str);
		System.out.println(getName());
	}
	public void run(){
	
		System.out.println(Thread.currentThread());
	} 
}
class ThreadGroupDemo {

	public static void main(String[] args){
	
		MyThread obj = new MyThread("Xyz");
		obj.start();
	}
}
