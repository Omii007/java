
class Outer {

	class Inner {
	
		void fun1(){
		
			System.out.println("In fun-Inner");
			System.out.println(this);
		}
	}
	void fun2(){
	
		System.out.println(this);
	}
}
class Client {

	public static void main(String[] args){
	
		Outer obj1 = new Outer();
		obj1.fun2();

		Outer.Inner obj2 = obj1.new Inner();
		obj2.fun1();
	}
}
