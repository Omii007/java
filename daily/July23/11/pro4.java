
// Annonymous Inner Class

class Demo {

	int x = 10;

	void marry(){
	
		System.out.println("Kriti Sanon");
	}
}
class Client {

	public static void main(String[] args){
	
		Demo obj = new Demo() {
			void marry(){
				System.out.println("Disha Patni");
			}	
		};
		Demo obj1 = new Demo(){
		
		
		};
	}
}
