// Real Time Example

import java.util.Scanner;
class OverSpeedException extends RuntimeException {

	OverSpeedException(String str){
	
		super(str);
	}
}
class Vehicle {

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Vehicle Speed");

		System.out.println("NOTE : Speed > 100");

		int Speed = sc.nextInt();

		if(Speed > 100){
		
			throw new OverSpeedException("Aare vedya speed kami nahitr jashil var");
		}
		System.out.println(Speed);
	}
}
