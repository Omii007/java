
// User Define Exception
//1) Throw Keyword/Clause

import java.util.Scanner;
class Demo {

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);		

		int x = sc.nextInt();

		try {
		
			if(x == 0){
			
				throw new ArithmeticException ("Divide By Zero");
			}
			System.out.println(10/x);
		}catch(ArithmeticException obj){
		
			System.out.print("Exception in thread "+ Thread.currentThread().getName()+" ");
			obj.printStackTrace();
			obj.getMessage();
			obj.toString();
		}
	}
}
