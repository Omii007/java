
// 2) Throw User Defined Exception
import java.util.Scanner;

class DataOverFlowException extends RuntimeException {

	DataOverFlowException (String msg){
	
		super(msg);
	}
}
class DataUnderFlowException extends RuntimeException {

	DataUnderFlowException (String msg){
	
		super(msg);
	}
}

class ArrayDemo {

	public static void main(String[] args){
	
		int arr[] = new int[5];
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Integer Value");

		System.out.println("NOTE : 0 < elements < 100");

		for(int i=0;i<arr.length;i++){
		
			int data = sc.nextInt();

			if(data < 0){
			
				throw new DataUnderFlowException("Mitra Data 0 peksha lahan ahe");
			}
			if(data > 100){
			
				throw new DataOverFlowException("Mitra data 100 peksha jast ahe");
			}
			arr[i] = data;
		}
		for(int i=0;i<arr.length;i++){
		
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}
