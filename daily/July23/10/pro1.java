
// Inner class
// 1) Normal Inner class

class Outer {

	class Inner {
	
		void fun2(){
		
			System.out.println("fun2-Inner");
		}
		class Inner1 {
		
			void fun3(){
			
				System.out.println("fun3-Inner1");
			}
		}
	}
	void fun1(){
	
		System.out.println("fun1-Outer");
		//System.out.println(this$1);
	}
}
class Client {

	public static void main(String[] args){
	
		Outer obj1 = new Outer();
		obj1.fun1();

		Outer.Inner obj2 = obj1.new Inner();
		obj2.fun2();

		Outer.Inner.Inner1 obj3 = obj2.new Inner1();
		obj3.fun3();
	}
}
