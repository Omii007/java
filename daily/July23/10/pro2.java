
// Variable 

class Outer {

	int x = 10;
	static int y = 20;

	class Inner {
		
		int x = 50;
		int y = 60;
		void fun(){
		
			System.out.println(this.x);
			System.out.println(this.y);
			fun1();
		}
		void gun(){  // error modifier static can use only constant variables deceleration
		
			System.out.println(y);
		}
	}
	static void fun1(){
	
		System.out.println("In Outer class");
	}
}
class Client {

	public static void main(String[] args){
	
		Outer.Inner obj = new Outer().new Inner();
		obj.fun();
		obj.gun();
	}
}
