
// Multithreading
// Creating thread using Thread class

class MyThread extends Thread {

	public void run(){
	
		try {
			Thread.sleep(2000);
		}catch(InterruptedException obj1){
		
			System.out.println("Handled");
		}
		for(int i=0;i<10;i++){
		
			System.out.println("In run");
		}
	}
}
class ThreadDemo {

	public static void main(String[] args){
	
		MyThread obj = new MyThread();
		//obj.start();
		System.out.println(obj.getId());
		System.out.println(obj.getName());
		try {
			Thread.sleep(2000);
		}catch(InterruptedException obj1){
		
			System.out.println("Handled");
		}
		for(int i=0;i<10;i++)
			System.out.println("In main");
		obj.start();
	}
}
