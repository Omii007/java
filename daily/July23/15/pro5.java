
// Finally (Connection closed sahi ha block wirte kela jato.
// Exception aso kiva naso caller kade jatana finally block madhun ch jato karan connection close karyche astat.

class Demo {

	void m1(){
	
	}
	void m2(){
	
	}
	public static void main(String[] args){
		
		System.out.println("Start Main");
		Demo obj = new Demo();

		obj.m1();
		obj = null;
		try{
			obj.m2();
		}catch(NullPointerException obj1){
		
			System.out.println("Here");
		}finally{
		
			System.out.println("Connection Closed");
		}

		System.out.println("End Main");
	}
}
