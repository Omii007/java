
// In check Exception compulsary exception throws karave lagtat.
// NumberFormatException

import java.io.*;

class ExceptionDemo {

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		String str = null;
		try {
		       str = br.readLine();
		}catch(IOException obj){
		
			System.out.println("catch");
		}
		System.out.println(str);
		
		int data = 0;
		try {
			data = Integer.parseInt(br.readLine());
		
		}catch(NumberFormatException obj1){
		
			System.out.println("Please Enter Integer Value");
		}
		
		System.out.println(data);		
	}
}
