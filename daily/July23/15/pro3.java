
// Try Catch & Finally

class Demo {

	public static void main(String[] args){
	
		System.out.println("Start Main");

		try {
		
			System.out.println(10/0);
		}catch(ArithmeticException obj){
		
			System.out.println("Exception Occured");
		}catch(IllegalArgumentException obj){
		
			System.out.println("Exception Occured1");
		}catch(RuntimeException obj){
		
			System.out.println("Exception Occured2");
		}catch(Exception obj){
		
			System.out.println("Exception Occured3");
		}/*catch(throwable obj){
		
			System.out.println("Exception Occured4");
		}catch(Object obj){
		
			System.out.println("Exception Occured4");
		}*/
				
		System.out.println("End Main");
	}
}
