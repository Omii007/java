
/* 2. Take N as i/p print odd integers from 1 to N
 * I/P : 6
 * O/P : 1 3 5*/

import java.util.Scanner;

class odd {

	public static void main(String[] args){
	
		int num;

		System.out.println("Enter number");

		Scanner sc = new Scanner(System.in);

		num = sc.nextInt();

		for(int i=1; i<=num; i++){
		
			if(i % 2 == 1)
				System.out.println(i);
		}
	}
}
