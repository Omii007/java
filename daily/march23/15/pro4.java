/* 4. Take N as i/p print all its factor.
 * I/P : 6
 * O/P : 1 2 3 6*/


import java.util.Scanner;

class fact {

	public static void main(String[] args){
	
		int num;

		Scanner sc = new Scanner(System.in);

		num = sc.nextInt();

		for(int i=1;i<=num ;i++){
		
			if(num % i == 0){
			
				System.out.println(i);
			}
		}
	}
}
