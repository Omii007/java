/* 3.Take N i/p print its factorial.
 * I/P : 5
 * O/P : 120*/


import java.util.Scanner;

class fact {

	public static void main(String[] args){
	
		int num;

		System.out.println("Enter number:");

		Scanner sc = new Scanner(System.in);

		num = sc.nextInt();
		
		int mult = 1;

		for(int i=1;i<=num;i++){
		
			mult = mult * i;

		}
		System.out.println(mult);
	}
}
