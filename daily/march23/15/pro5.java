/* 5. Take N i/p count all its factor and print count
 * I/P : 6
 * O/P : 4*/


import java.util.Scanner;

class cnt {

	public static void main(String[] args){
	
		int num;

		System.out.println("Enter number:");

		Scanner sc = new Scanner(System.in);

		num = sc.nextInt();

		int count = 0;

		for(int i=1;i<=num ; i++){
		
			if(num % i == 0){
				count++;
			}

		}
		System.out.println(count);
	}
}
