/* 1. Take N as i/p print from 1 to N
 * I/P : 5
 * O/P : 1 2 3 4 5*/

import java.util.Scanner;

class loop {

	public static void main(String[] args){
	
		int num;

		System.out.println("Enter number:");

		Scanner sc = new Scanner(System.in);

		num = sc.nextInt();

		for(int i=1;i<=num;i++){
		
			System.out.println(i);
		}
	}
}
