
/* 3. Take an integer N as input.
 * Print multiples of 4 till N.
 * I/P : 22
 * O/P :4 8 12 16 20*/

import java.util.Scanner;

class loop {

	public static void main(String[] args){
	
		int num;

		System.out.println("Enter number::");

		Scanner sc = new Scanner(System.in);
		num = sc.nextInt();

		int i = 4;
		while(i <= num){
		
			System.out.println(i);
			i+=4;
		}
	}
}
