
/* 2. Take an integer N as input.
 * Print odd integers from 1 to N using loop
 * I/P : 10
 * O/P : 1 3 5 7 9*/


import java.util.Scanner;

class odd {

	public static void main(String[] args){
	
		int i=1,num=10;
		while(i <= num){
		
			System.out.println(i);
			i+=2;
		}
	}
}
