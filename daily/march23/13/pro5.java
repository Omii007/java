/* 5. Write a program in reverse
 * I/P : 135
 * O/P : 531*/


import java.util.Scanner;

class imp {

	public static void main(String[] args){
	
		int num,rem,store=0;

		System.out.println("Enter a number::");

		Scanner sc = new Scanner(System.in);
		num = sc.nextInt();

		while(num > 0){
		
			rem = num % 10;
			store = store *10 + rem;
			num = num/10;
		}
		System.out.println(store);
	}
}
