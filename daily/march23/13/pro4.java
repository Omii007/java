/* 4. Take an integer N as input, print perfect squares till N.
 * perfect square :An integer whose square root is a integer.
 * I/P : 30
 * O/P : 1  2  3  4  5*/


import java.util.Scanner;

class data {

	public static void main(String[] args){
	
		int num=0,i=1,N;

		System.out.println("Enter a number::");
		Scanner sc = new Scanner(System.in);

		N = sc.nextInt();
		
		while(i*i < N){
			
			num = i * i;
			System.out.println(num);
			i++;
		}


	}
}
