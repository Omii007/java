/* 2. Given an integer N, print sum of its digits.
 * I/P : 753
 * O/P : 15*/


import java.util.Scanner;

class digit {

	public static void main(String[] args){
	
		int num,rem,sum=0;

		System.out.println("Enter a number::");

		Scanner sc = new Scanner(System.in);
		num = sc.nextInt();

		while(num > 0){
		
			rem = num % 10;
			sum = sum + rem;
			num = num / 10;
		}
		System.out.println(sum);
	}
}
