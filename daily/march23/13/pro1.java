/* 1. Given an integers N print all its digit.
 * I/P : 6531
 * O/P :1  3  5  6*/


import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int num,rem;

		System.out.println("Enter a number::");

		Scanner sc = new Scanner(System.in);
		num = sc.nextInt();

		while(num != 0){
		
			rem = num % 10;

			System.out.println(rem);
			num = num/10;
		}
	}
}
