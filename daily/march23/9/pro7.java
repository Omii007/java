
/* 7. Given an integer value as I/P :
 * -print fizz if it is divisible by 3.
 * -print buzz if it is divisible by 5.
 * -print fizz-buzz if it is divisible by both.
 * -if not then print "Not divisible by both".*/

import java.util.Scanner;

class both {

	public static void main(String[] args){
	
		int num;
		System.out.println("Enter number::");

		Scanner sc = new Scanner(System.in);
		num = sc.nextInt();

		if(num % 3 == 0 && num % 5 == 0)
			System.out.println("fizz-buzz");
		
		else if(num % 3 == 0)
			System.out.println("fizz");

		else
			System.out.println("buzz");
	}
}
