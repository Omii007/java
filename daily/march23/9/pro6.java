
/* 6. Given an integer input ,print whether its odd or even.
 * I/P : 7   I/P : 4
 * O/P : 7 is odd    O/P : 4 is even*/


import java.util.Scanner;

class odd {

	public static void main(String[] args){
	
		int num;

		System.out.println("Enter number::");

		Scanner sc = new Scanner(System.in);
		num = sc.nextInt();

		if(num % 2 == 0)
			System.out.println(num + "is even");
		else
			System.out.println(num + "is odd");
	}
}
