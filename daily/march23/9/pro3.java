
/* 3. Take two integers A & B as input.
 * - print the max two .
 * I/P : x=5,y=7;
 * O/P : 7 is greater
 * I/P : x=5,y=5;
 * O/P : Both are equal*/

import java.util.Scanner;

class max {

	public static void main(String[] args){
	
		int x,y;

		System.out.println("Enter x & y::");

		Scanner sc = new Scanner(System.in);
		x = sc.nextInt();
		y = sc.nextInt();

		if(x > y)
			System.out.println(x + "is greater");
		else if(x < y)
			System.out.println(y + "is greater");
		else
			System.out.println("Both are equal");
	}
}
