 
/* 4. Given the temp. of a person in farenheit.
 * - print whwther the person has high,normal,low temp.
 *   > 98.6->high, 98.0<= & <= 98.6->normal, <98.0->low*/

import java.util.Scanner;

class temp {

	public static void main(String[] args){
	
		float temp;

		System.out.println("Enter temp::");

		Scanner sc = new Scanner(System.in);
		temp = sc.nextFloat();

		if(temp > 98.6)
			System.out.println("High");

		else if(temp >= 98.0 && temp <= 98.6)
			System.out.println("Normal");

		else
			System.out.println("Low");
	}
}
