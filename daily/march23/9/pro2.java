
/* 2. Take two integers A & B as input.
 * - Print the max of two.
 * - Assume : x and y are not equal
 * I/P : x=5,y=7
 * O/P : 7 is greater*/

import java.util.Scanner;
class max {

	public static void main(String[] args){
	
		int x,y;

		System.out.println("Enter x & y:: ");

		Scanner sc = new Scanner(System.in);
		x = sc.nextInt();
		y = sc.nextInt();

		if(x > y)
			System.out.println(x + "is greater");
		else
			System.out.println(y + "is greater");
	}
}
