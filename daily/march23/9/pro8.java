
/* 8. Electricity Bill Problem
 * -Given an integer input A which represent units of electricity consumed of your house.
 * -Calculate and print the bill amout.
 *  units <= 100 : price per unit is 1
 *  units > 100 : price per unit is 2.
 *  I/P :50   200
 *  O/P :50   300.*/

import java.util.Scanner;

class bill {

	public static void main(String[] args){
	
		int unit;

		System.out.println("Enter bill units::");

		Scanner sc = new Scanner(System.in);
		unit = sc.nextInt();

		if(unit <= 100)
			System.out.println(1*unit);
		else
			System.out.println(2*unit-1*100);  // ((unit-100)*2+100))
	}
}
