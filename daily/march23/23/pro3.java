/* 2. A  B  C
 *    D  E  F
 *    G  H  I
 *    */

import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int rows,col;

		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();

		System.out.println("Enter col:");
		col = sc.nextInt();

		int ch = 62 + rows;
		
		for(int i=1;i<=rows;i++){
		
			for(int j=1;j<=col;j++){
			
				System.out.print((char)ch++ + "\t");

			}
			System.out.println("");
		}
	}
}
