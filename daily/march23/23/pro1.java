/* 1  2  3
 * 4  5  6
 * 7  8  9*/

import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int rows,col;

		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();
		
		System.out.println("Enter col:");
		col = sc.nextInt();

		for(int i=1;i<=rows*col;i++){
		
			if(i % col == 0){
			
				System.out.print(i +"\t");
				System.out.println(" ");
			}else{
			
				System.out.print(i +"\t");
			}
		}
	}
}
