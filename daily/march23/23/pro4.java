/* 4. A  1  B  2
 *    A  1  B  2
 *    A  1  B  2*/

import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int rows,col;

		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();

		System.out.println("Enter col:");
		col = sc.nextInt();

		for(int i=1;i<=rows;i++){
			
			int ch = 62 + rows;
			int num = 1;
		
			for(int j=1;j<=col;j++){

				if(j % 2 == 1)		
					System.out.print((char)ch++ + "\t");
				else
					System.out.print(num++ + "\t");
			}
			System.out.println("");
		}
	}
}
