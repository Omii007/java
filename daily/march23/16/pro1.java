/* 6. Take N as i/p print whether its prime or not.
 * Quiz : If number (from 1 to N) is divisible by 1 and itself is called prime number.-> false
 * I/P : 8
 * O/P : Not a prime*/

import java.util.Scanner;

class prime {

	public static void main(String[] args){
	
		int num;

		System.out.println("Enter number:");

		Scanner sc = new Scanner(System.in);

		num = sc.nextInt();

		int count = 0;

		for(int i=1;i<=num;i++){
		
			if(num % i == 0){
			
				count++;
			}
		}
		if(count == 2)
			System.out.println(num +" is prime");
		else
			System.out.println(num +" is not prime");
	}
}
