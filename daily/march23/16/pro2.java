/* 2. Take integer N as i/p print whether N is perfect number or not.
 * I/P : 4
 * O/P : Not perfect number
 * I/P : 6
 * O/P : perfect number*/

import java.util.Scanner;

class per {

	public static void main(String[] args){
	
		int num;

		System.out.println("Enter number:");

		Scanner sc = new Scanner(System.in);

		num = sc.nextInt();

		int sum = 0;

		for(int i=1;i<num;i++){
		
			if(num % i == 0){
			
				sum = sum + i;
			}
		}
		if(num == sum)
			System.out.println(num + " is perfect number");
		else
			System.out.println(num + " is not perfect number");
	}
}
