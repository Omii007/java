/* 3. Take N as i/p print whether N is Armstrong number or not.
 * I/P : 23
 * O/P : Not an armstrong number
 * I/P : 153
 * O/P : Armstrong number*/


import java.util.Scanner;

class arm {

	public static void main(String[] args){
	
		int num,sum=0,count=0,mult,rem;

		System.out.println("Enter number:");

		Scanner sc = new Scanner(System.in);
		num = sc.nextInt();

		int num1 = num;
		int num2 = num;

		while(num > 0){
		
			count++;
			num = num/10;
		}
		while(num1 > 0){
			 mult = 1;
			rem = num1%10;
			for(int i=1;i<=count;i++){
		
				mult = mult * rem;
			}
			sum = sum + mult;
			num1 = num1/10;
		}
		if(sum == num2)
			System.out.println(num2 +" is Armstrong number");
		else
			System.out.println(num2 +" is Not Armstrong number");
	}
}
