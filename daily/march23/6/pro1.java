
// Unary Operator

class unary{

	public static void main(String[] srgs){
	
		int x = 5;
		int y = 7;

		System.out.println(++x);  // 6
		System.out.println(++y);  // 8
		System.out.println(--x);  // 5
		System.out.println(--y);  // 7
		System.out.println(x);  // 5
		System.out.println(y);  // 7
	}
}
