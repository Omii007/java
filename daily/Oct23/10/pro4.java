
// Sub Array

/* subarray:
 * - Given an integer array of size N.
 *   Return the length of the smallest subarray with contains both the maximum of the array and the minimum of the array.
 *
 *   Arr = [1,2,3,1,3,4,6,4,6,3]
 */

import java.io.*;

class Demo {
	
	static int subarray(int arr[],int size){
	
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;

		for(int i=0;i<arr.length;i++){
		
			if(arr[i] > max){
			
				max = arr[i];
			}
			if(arr[i] < min){
			
				min = arr[i];
			}
		}
		int minlength = Integer.MAX_VALUE;
		int len = 0;

		for(int i=0;i<arr.length;i++){
		
			for(int j=i+1;j<arr.length;j++){
			
				if(arr[i] == min && arr[j] == max){
				
					len = j - i + 1;
					if(minlength > len){
					
						minlength = len;
					}
				}
			}
		}
		return minlength;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		int x = subarray(arr,size);

		System.out.println("Minimum array is = "+x);
	}
}
