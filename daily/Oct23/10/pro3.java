

/* Equilibrium index of an array
 * - You are given an array A of integers of size N.
 *   Your task is to find the equlibrium index of the given array.
 *   The equilibrium index of an array is an index such that the sum of elements at lower indexes is equal to the sum of elements at higher indexes.
 *   If there are no elements that are at lower indexes or at higher indexes then the corresponding sum of elements is considered as 0.
 *
 *   NOTE : Array index start from 0
 *   if there is no equlibrium index then returtn -1.
 *   if there are more than one equlibrium index then return the minimum index.
 *
 *   Problem Constraints
 *   1 <= N <= 105
 *   -105 <= A[i] <= 105
 *
 *   Ex1 :
 *   A = [-7,1,5,2,-4,3,0]
 *   O/P : 3
 *
 *   Ex2 :
 *   A = [1,2,3]
 *   O/P : -1
 */

import java.io.*;

class Demo {

	static int equil(int arr[],int size){
	
		int flag = 0;
		for(int i=0;i<arr.length;i++){
		
			int leftsum = 0;
			int rightsum = 0;

			for(int j=0;j<i;j++){
			
				leftsum = leftsum + arr[j];
			}
			for(int j = i+1;j<arr.length;j++){
			
				rightsum = rightsum + arr[j];
			}
			if(leftsum == rightsum){
			
				flag = 1;
				return i;
			}
		}
		//if(flag == 0)
		return -1;
	}

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		int x = equil(arr,size);

		System.out.println("Equlibrium index = "+x);
	}
}
