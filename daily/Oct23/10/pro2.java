
/* Given an character array (lower case) return the count of pair (i,j) such that 
 * a) i < j
 * b) arr[i] = 'a'
 *    arr[j] = 'g'
 * Arr = [a,b,e,g,a,g]
 * O/P : 3
 */

import java.io.*;

class Demo {

	static int pair(char arr[],int size){
	
		int cnt = 0,pairs = 0;
		for(int i=0;i<size;i++){
		
			if(arr[i] == 'a'){
				cnt++;
			}else if(arr[i] == 'g')
				pairs = pairs + cnt;
		}
		return pairs;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		char arr[] = new char[size];

		System.out.println("Enter array elements:");

		for(int i=0;i<size;i++){
		
			arr[i] = (char)br.read();
		
		}
		int x = pair(arr,size);

		System.out.println("Count is = "+x);
	}
}
