
class parent{

	int x = 10;
	
	void m1(){
	
		System.out.println("In parent-m1");
	}
}
class child extends parent{

	int a = 20;

	void m1(){
	
		System.out.println("In child-m1");
	}
}
class Demo{

	Demo(parent p){
	
		System.out.println("In parent constructor");
		p.m1();
	}
	Demo(child c){
	
		System.out.println("In child constructor");
		c.m1();
	}
	public static void main(String[] args){
	
		Demo obj1 = new Demo(new parent());

		Demo obj2 = new Demo(new child());
	}
}
