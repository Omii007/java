
class Demo{

	Demo(){
	
		System.out.println("In Demo-constructor");
	}
}
class Demochild extends Demo{

	Demochild(){
	
		System.out.println("In Demochild-constructor");
	}
}
class parent{

	parent(){
	
		System.out.println("In parent-constructor");
	}
	Demo m1(){
	
		System.out.println("In m1-parent");
		return new Demo();
	}
}
class child extends parent{

	child(){
	
		System.out.println("In child-constructor");
	}
	Demochild m1(){
	
		System.out.println("In child-m1");
		return new Demochild();
	}
}
class Client{

	public static void main(String[] args){
	
		parent obj = new child();
		obj.m1();
	}
}
