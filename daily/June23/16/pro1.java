
class parent {

	int x = 10;
	static int y = 20;

	parent(){
	
		System.out.println("In parent constructor");
	}
	void m1(){
	
		System.out.println("In m1 parent");
	}
	static void m2(){
	
		System.out.println("In m2 parent");
	}
}
class child extends parent{

	int a = 50;
	static int b = 60;

	child(){
	
		System.out.println("In child constructor");
	}
	void m1(){
	
		System.out.println("In child m1");
	}
	static void m3(){
	
		System.out.println("In m3 child");
	}
}
class Client {

	public static void main(String[] args){
	
		parent obj1 = new parent();
		obj1.m1();
		obj1.m2();

		child obj2 = new child();
		obj2.m1();
		obj2.m3();

		parent obj3 = new child();
		obj3.m1();
		obj3.m2();
		obj3.m3();
	}
}
