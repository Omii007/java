
class parent {

	int x = 10;
	static int y = 20;

	static {
	
		System.out.println("Parent static block");
	}
	parent(){
	
		System.out.println("In parent constructor");
	}
	void method_one(){
	
		System.out.println(x);
		System.out.println(y);
	}
	static void method_two(){
	
		System.out.println(y);
	}
}
class child extends parent {

	static {
	
		System.out.println("In child static block");
	}
	child (){
	
		System.out.println("In child constructor");
	}
}
class client {
	
	public static void main(String[] args){
	
		child obj = new child();
		obj.method_one();
		obj.method_two();
	}
}
