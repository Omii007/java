
class singleton {
	
	static singleton obj = new singleton();
	singleton(){
	
		System.out.println("Constructor");
	}
	static singleton getobject (){
	
		return obj;
	}
}
class Client {

	public static void main(String[] args){
	
		singleton obj1 = singleton.getobject();
		System.out.println(obj1);
		
		singleton obj2 = singleton.getobject();
		System.out.println(obj2);
		
	}
}
