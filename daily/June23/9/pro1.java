
abstract class parent {

	void career (){
	
		System.out.println("Doctor");
	}
	abstract void marry();
}
class child extends parent {

	void marry(){
	
		System.out.println("Alia Bhatt");
	}
}
class Client {

	public static void main(String[] args){
	
		parent obj = new child();
		obj.marry();
		obj.career();
	}
}
