
interface Demo {

	static void fun(){
	
		System.out.println("In fun-Demo");
	}
}
class Demochild implements Demo {

	void fun(){
	
		System.out.println("In fun-Demochild");
		Demo.fun();
	}
}
class Client {

	public static void main(String[] args){
	
		Demochild obj1 = new Demochild();
		obj1.fun();
	}
}
