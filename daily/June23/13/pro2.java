
interface Demo {

	void gun();
	
	default void fun(){
	
		System.out.println("In fun-Demo");
	}
}
class childDemo implements Demo{

	public void gun(){
	
		System.out.println("In gun-childDemo");
	}
}
class Client {

	public static void main(String[] args){
	
		Demo obj = new childDemo();
		obj.gun();
		obj.fun();
	}
}
