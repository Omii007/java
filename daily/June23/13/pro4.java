
interface Demo {

	static void fun(){
	
		System.out.println("In fun-Demo");
	}
}
class Demochild implements Demo{

}
class Client {

	public static void main(String[] args){
	
		Demochild obj = new Demochild();
		obj.fun();

		//Demo obj2 = new Demochild();
		//obj2.fun();
	}
}
