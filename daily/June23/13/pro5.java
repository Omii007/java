
class Demo {

	static void fun(){
	
		System.out.println("In fun-Demo");
	}
}
class Demochild extends Demo {


}
class Client {

	public static void main(String[] args){
	
		Demochild obj1 = new Demochild();
		obj1.fun();

		Demo obj2 = new Demochild();
		obj2.fun();
	}
}
