
interface Demo1 {

	default void m1(){
	
		System.out.println("Demo1-m1");
	}
}
interface Demo2 {

	default void m1(){
	
		System.out.println("Demo2-m1");
	}
}
class Demochild implements Demo1,Demo2{

	public void m1(){
	
		System.out.println("In Demochild");
	}
}
class Client {

	public static void main(String[] args){
	
		Demochild obj = new Demochild();
		obj.m1();
	}
}
