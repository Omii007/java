
interface Demo1 {

	static void m1(){
	
		System.out.println("Demo1-m1");
	}
}
interface Demo2 {

	static void m1(){
	
		System.out.println("Demo2-m1");
	}
}
class Demochild implements Demo1,Demo2{

	
}
class Client {

	public static void main(String[] args){
	
		Demochild obj = new Demochild();
		obj.m1(); // interface madhe static method child kade yet nahit
		
		Demo1 obj1 = new Demochild();
		obj1.m1();
	}
}
