
class parent {

	private void fun(){ // method cha access class purtach ahe karan method private ahe
	
		System.out.println("Parent fun");
	}
}
class child extends parent {

	void fun(){
	
		System.out.println("Child fun");
	}
}
class Client {

	public static void main(String[] args){
	
		parent obj = new child();
		obj.fun();
		
		child obj = new child();
		obj.fun();
	}
}
