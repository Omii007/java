
class parent{

	parent(){
	
		System.out.println("parent constructor");
	}
	void fun(){
	
		System.out.println("In fun");
	}
}
class child extends parent{

	child(){
	
		System.out.println("child constructor");
	}
	void gun(){
	
		System.out.println("In gun");
	}
}
class Client{

	public static void main(String[] args){
	
		child obj1 = new child();
		obj1.fun();
		obj1.gun();
		
		parent obj2 = new child();
		obj2.fun();
		obj2.gun();
		
		parent obj3 = new parent();
		obj3.fun();
		obj3.gun();
	}
}
