
class parent{

	parent(){
	
		System.out.println("parent constructor");
	}
	void fun(){
	
		System.out.println("In parent fun");
	}
}
class child extends parent{

	child(){
	
		System.out.println("child constructor");
	}
	void fun(int x){
	
		System.out.println("In child fun");
	}
}
class Client{

	public static void main(String[] args){
	
		parent obj = new child();
		obj.fun();
		child obj1 = new child();
		obj1.fun(10);
	}
}
