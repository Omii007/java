
class parent {

	char fun(){
	
		System.out.println("In parent fun");
		return 'A';
	}
}
class child extends parent {

	char fun(){
	
		System.out.println("In child fun");
		return 'A'; // data type and return type must be same
	}
}
class Client {

	public static void main(String[] args){
	
		parent obj = new child();
		obj.fun();
		
		child obj1 = new child();
		obj1.fun();
	}
}
