
class parent {

	Object fun(){
		
		Object obj = new Object();	
		System.out.println("In parent fun");
		return obj;
	}
}
class child extends parent{

	String fun(){
	
		System.out.println("In child fun");
		return "Shashi";
	}
}
class Client{

	public static void main(String[] args){
	
		parent obj = new child();
		obj.fun();
	}
}
