
class IPL{

	void matchinfo(String team1,String team2){
	
		System.out.println(team1 +" VS "+team2);
	}
	void matchinfo(String team1,String team2,String venue){
	
		System.out.println(team1 +" VS "+team2);
		System.out.println("Venue "+venue);
	}
}
class Client{

	public static void main(String[] args){
	
		IPL obj = new IPL();
		obj.matchinfo("GT","CSK");
		obj.matchinfo("GT","CSK","NMSA");
	}
}
