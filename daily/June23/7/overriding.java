
class match {

	void matchType(){
	
		System.out.println("T-20/One Day/Test");
	}

}
class IPLmatch extends match{

	void matchType(){
	
		System.out.println("T-20");
	}
}
class Testmatch extends match{

	void matchType(){
	
		System.out.println("Test");
	}
}
class Client {

	public static void main(String[] args){
	
		match obj1 = new IPLmatch();
		obj1.matchType();
		
		match obj2 = new Testmatch();
		obj2.matchType();
	}
}
