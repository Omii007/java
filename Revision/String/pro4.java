
class Stringdemo {

	public static void main(String[] args){
	
		String str1 = "Shiv";
		String str2 = "Shiv";

		String str3 = new String("Yash");
		String str4 = new String("Yash");
		String str5 = new String("Kunal");
		String str6 = "Kunal";

		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
		System.out.println(System.identityHashCode(str5));
		System.out.println(System.identityHashCode(str6));
	}
}
