
/*  .equals method he exact content compare krte
 *  '==' operator identityHashCode var deal krto
 */


class StringDemo {

	public static void main(String[] args){
	
		String str1 = "Shashi";
		String str2 = "Bagal";

		String str3 = str1 + str2;  // Internally StringBuilder class chya "append" method la call krto.
		String str4 = str1.concat(str2); // concat method he String class chi ahe

		System.out.println(str2);
		System.out.println(str4);
	}
}
