
class Demo {

	public static void main(String[] args){
	
		String str1 = "Om";
		String str2 = new String("Om");
		String str3 = "Om";
		String str4 = new String("Om");

		System.out.println(str1.hashCode());
		System.out.println(str2.hashCode());
		System.out.println(str3.hashCode());
		System.out.println(str4.hashCode());
	}
}
