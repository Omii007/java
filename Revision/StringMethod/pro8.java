
import java.util.*;

class Demo {

	static int mystrlen(String str){
	
		char arr[] = str.toCharArray();

		int count = 0;
		for(int i=0;i<arr.length;i++){
		
			count++;
		}
		return count;
	}

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter String1");
		String str1 = sc.next();
		
		System.out.println("Enter String2");
		String str2 = sc.next();

		if(mystrlen(str1) == mystrlen(str2)){
		
			System.out.println("Equals");
		}else{
		
			System.out.println("Not Equals");
		}
	}
}
