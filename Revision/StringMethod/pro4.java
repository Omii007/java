
// method 4. public char charAt(int index)


class Demo {

	public static void main(String[] args){
	
		String str = "Core2Web";

		System.out.println(str.charAt(4));
		System.out.println(str.charAt(2));
		System.out.println(str.charAt(6));
		try{
			System.out.println(str.charAt(9));
		}catch(StringIndexOutOfBoundsException e){
		
			System.out.println("Handled");
		}
	}
}
