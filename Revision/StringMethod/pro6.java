

// Method 5 : public int compareToIgnoreCase(String str)


class Demo {

	public static void main(String[] args){
	
		String str1 = "SHASHI";
		String str2 = "shashi";

		String str3 = "OMKAR";
		String str4 = "omkar";

		System.out.println(str1.compareToIgnoreCase(str2));
		System.out.println(str3.compareToIgnoreCase(str4));
	}
}
