
// Stack :

import java.util.*;

class stack{

	int maxSize;
	int StackArr[];
	int top = -1;

	stack(int size){
	
		this.maxSize = size;
		this.StackArr = new int[size];
	}

	// push(data)
	void push(int data){
	
		if(top == maxSize-1){
			System.out.println("stack overflow");
			return;
		}else{
			top++;
			StackArr[top] = data;
		}	
	}
	// empty()
	boolean empty(){
	
		if(top == -1)
			return true;
		else
			return false;
	}

	//pop()
	int pop(){
	
		if(empty()){
		
			System.out.println("Stack is empty");
			return -1;
		}else{
		
			int val = StackArr[top];
			top--;
			return val;
		}
	}
	// peek()
	int peek(){
	
		if(empty()){
		
			System.out.println("Stack is empty");
			return -1;
		}else{
		
			return StackArr[top];
		}
	}
	// size()
	int size(){
	
		return top;
	}
	// printStack()
	void printStack(){
	
		if(empty()){
		
			System.out.println("Nothing to print");
			return;
		}else{
		
			System.out.print("[");
			for(int i=0;i<=top;i++){
			
				System.out.print(StackArr[i]+" ");
			}
			System.out.println("]");
		}
	}
}

class Client {

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a stack size:");
		int size = sc.nextInt();

		stack obj = new stack(size);

		char ch;

		do{
		
			System.out.println("1.push");
			System.out.println("2.pop");
			System.out.println("3.peek");
			System.out.println("4.empty");
			System.out.println("5.size");
			System.out.println("6.printstack");

			System.out.println("Enter your choice");

			int choice = sc.nextInt();			
		
			switch(choice){
			
				case 1:
					{
						System.out.println("Enter data for stack");
						int data = sc.nextInt();
						obj.push(data);
					}
					break;
				case 2:
					{
						int flag = obj.pop();

						if(flag != -1)
							System.out.println(flag+" is popped");
					}
					break;
				case 3:
					{
						int flag = obj.peek();
						if(flag != -1){
							
							System.out.println("top = "+flag);
						}
					}
					break;
				case 4:
					{
						boolean flag = obj.empty();
						if(flag){
							
							System.out.println("Stack is empty");
						}else{
							System.out.println("Stack is not empty");
						}
					}
					break;
				case 5:
					{
						int sz = obj.size();
						System.out.println("Stack size = "+(sz + 1));
					}
					break;
				case 6:
					obj.printStack();
					break;
				default :
					System.out.println("Wrong choice");
					break;
			}
			System.out.println("Do you want to continue?");
			ch = sc.next().charAt(0);
		}while(ch == 'y' || ch == 'Y');
		
	}
}
