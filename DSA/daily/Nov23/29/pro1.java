
// Problem Statement Of LinkedList :

/* 1) Reverse LinkedList :
 * 	- Iterative -> In-place Reverse
 * 	- Recursion -> In-place Reverse
 */

import java.util.*;

class Node {

	int data;
	Node next = null;

	Node(int data){
	
		this.data = data;
	}
}

class ReverseLL {

	Node head = null;

	//addNode()
	void addNode(int data){
	
		Node newNode = new Node(data);
		
		if(head == null){
		
			head = newNode;
		}else{
		
			Node temp = head;

			while(temp.next != null){
			
				temp = temp.next;
			}
			temp.next = newNode;
		}
	}
	
	// countNode()
	
	int countNode(){
	
		int count = 0;
		Node temp = head;

		while(temp != null){
		
			count++;
			temp = temp.next;
		}
		return count;
	}
	// reverseItr()
	
	void reverseItr(){
	
		if(head == null){
		
			return;
		}else{
		
			Node prev = null;
			Node curr = head;
			Node forward = null;

			while(curr != null){
			
				forward = curr.next;
				curr.next = prev;
				prev = curr;
				curr = forward;
			}
			head = prev;
		}
	}

	//reverseRec()
	
	void reverseRec(Node prev,Node curr){
	
		if(curr == null){
		
			head = prev;
			return;
		}else{
			Node forward = curr.next;
			curr.next = prev;
			prev = curr;
			curr = forward;
		}
		
		reverseRec(prev,curr);
	}

	//middleLL()
	
	void middleLL(){
	
		int count = countNode();
		Node temp = head;
		int cnt = 0;

		while(cnt < count/2){
		
			temp = temp.next;
			cnt++;
		}
		System.out.println("Middle element of LL is "+temp.data);
	}

	// middlePtrLL()
	
	void middlePtrLL(){
	
		Node slow = head;
		Node fast = head.next;

		while(fast != null){
		
			fast = fast.next;
			if(fast != null)
				fast = fast.next;
			slow = slow.next;
		}
		System.out.println("Middle element of linkedlist is "+slow.data);
	}
	//printLL()
	void printLL(){
	
		if(head == null){
		
			System.out.println("Empty LinkedList");
			return;
		}
		int count = 0;
		Node temp = head;

		while(temp.next != null){
		
			System.out.print(temp.data+"->");
			temp = temp.next;
		}
		System.out.println(temp.data);
	}
}

class Client {

	public static void main(String[] args){
	
		ReverseLL ll = new ReverseLL();

		Scanner sc = new Scanner(System.in);
		char ch;

		do{
		
			System.out.println("1.addNode");
			System.out.println("2.printLL");
			System.out.println("3.reverseItr");
			System.out.println("4.reverseRec");
			System.out.println("5.middleLL");
			System.out.println("6.middlePtrLL");
			
			System.out.println("Enter your choice");
			int choice = sc.nextInt();

			switch(choice){
			
				case 1:
					{
						System.out.println("Enter a data");
						int data = sc.nextInt();
						ll.addNode(data);
					}
					break;
				case 2:
					ll.printLL();
					break;
				case 3:
					ll.reverseItr();
					break;
				case 4:
					{
						Node prev = null;
						Node curr = ll.head;
						ll.reverseRec(prev,curr);
					}
					break;
				case 5:
					ll.middleLL();
					break;
				case 6:
					ll.middlePtrLL();
					break;                                                                                                       
				default:
					System.out.println("Wrong choice");
					break;
			}
			System.out.println("Do you want to continue");
			ch = sc.next().charAt(0);

		}while(ch == 'y' || ch == 'Y');
	}
}
