
// Doubly LinkedList :

import java.util.*;

class Node {

	int data;
	Node prev = null;
	Node next = null;

	Node(int data){
	
		this.data = data;
	}
}

class LinkedList {

	Node head = null;

	//addFirst(data)
	void addFirst(int data){
	
		Node newNode = new Node(data);

		if(head == null){
			
			head = newNode;
		}else{
		
			newNode.next = head;
			head.prev = newNode;
			head = newNode;
		}
	}

	//addLast(data)
	void addLast(int data){
	
		Node newNode = new Node(data);

		if(head == null){
		
			head = newNode;
		}else{
		
			Node temp = head;

			while(temp.next != null){
				
				temp = temp.next;
			}
			temp.next = newNode;
			temp.next.prev = temp;
		}
	}
	
	//countNode()
	int countNode(){
	
		Node temp = head;
		int count = 0;

		while(temp != null){
		
			count++;
			temp = temp.next;
		}
		return count;
	}

	// addAtPos(pos,data)
	void addAtPos(int pos,int data){
	
		if(pos <= 0 || pos >= countNode()+2){
		
			System.out.println("Wrong Input");
			return;
		}
		if(pos == 1){
		
			addFirst(data);
		}else if(pos == countNode()+1){
		
			addLast(data);
		}else{
		
			Node newNode = new Node(data);

			Node temp = head;

			while(pos-2 != 0){
			
				temp = temp.next;
				pos--;
			}
			newNode.next = temp.next;
			newNode.prev = temp;
			temp.next.prev = newNode;
			temp.next = newNode;
		}
	}

	//delFirst()
	void delFirst(){
	
		if(head == null){
		
			System.out.println("LinkedList is Empty");
		}else{
		
			head = head.next;
			head.prev = null;
		}
	}

	//delLast()
	void delLast(){
	
		if(head == null){
		
			System.out.println("LinkedList is empty");
		}else if(countNode() == 1){
			
			head = null;
		}else{
		
			Node temp = head;

			while(temp.next.next != null){
			
				temp = temp.next;
			}
			temp.next = null;
		}
	}

	//delAtPos(pos)
	void delAtPos(int pos){
	
		if(pos <= 0 || pos >= countNode()+1){
		
			System.out.println("Wrong Input");
			return;
		}
		if(pos == 1){
		
			delFirst();
		}else if(pos == countNode()){

			delLast();
		}else{
		
			Node temp = head;

			while(pos-2 != 0){
			
				temp = temp.next;
				pos--;	
			}
			temp.next = temp.next.next;
			temp.next.prev = temp;
		}
	}
	//printDLL()
	void printDLL(){
	
		if(head == null){
		
			System.out.println("LinkedList is Empty");
		}else{
		
			Node temp = head;

			while(temp.next != null){
			
				System.out.print(temp.data+"->");
				temp = temp.next;
			}
			System.out.println(temp.data);
		}
	}
}

class Client {

	public static void main(String[] args){
	
		LinkedList ll = new LinkedList();

		char ch;

		do{
		
			System.out.println("1.addFirst");
			System.out.println("2.addLast");
			System.out.println("3.addAtPos");
			System.out.println("4.delFirst");
			System.out.println("5.delLast");
			System.out.println("6.delAtPos");
			System.out.println("7.countNode");
			System.out.println("8.printDLL");

			System.out.println("Enter a choice");
			Scanner sc = new Scanner(System.in);
			int choice = sc.nextInt();

			switch(choice){
			
				case 1:
					{
						System.out.println("Enter a data");
						int data = sc.nextInt();
						ll.addFirst(data);
					}
					break;
				case 2:
					{
						System.out.println("Enter a data");
						int data = sc.nextInt();
						ll.addLast(data);
					}
					break;
				case 3:
					{
						System.out.println("Enter a position to add Node");
						int pos = sc.nextInt();
						System.out.println("Enter a data");
						int data = sc.nextInt();
						ll.addAtPos(pos,data);
					}
					break;
				case 4:
					ll.delFirst();
					break;
				case 5:
					ll.delLast();
					break;
				case 6:
					{
						System.out.println("Enter a position to delete Node");
						int pos = sc.nextInt();
						ll.delAtPos(pos);
					}
					break;
				case 7:
					{
						int count = ll.countNode();
						System.out.println("Count of Node is "+count);
					}
					break;
				case 8:
					ll.printDLL();
					break;
				default:
					System.out.println("Wrong Input");
					break;
			
			}
			System.out.println("Do you want to continue");
			ch = sc.next().charAt(0);

		}while(ch == 'y' || ch == 'Y');
	}
}
