

// Insertion Sort
// 1st for loop 1 pasun start krycha.
// compare karycha sallest element and 0th index la thevaycha.


import java.io.*;

class Demo {

	static void sort(int arr[]){
	
		for(int i=1;i<arr.length;i++){
		
			int element = arr[i];

			int j = i-1;
			while(j >= 0 && arr[j] > element){
			
				arr[j+1] = arr[j];
				j--;
			}
			arr[j+1] = element;
		}
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		sort(arr);
		for(int i=0;i<arr.length;i++)
			System.out.print(arr[i]+" ");

		System.out.println();

	}
}
