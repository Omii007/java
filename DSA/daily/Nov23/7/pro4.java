
// Sorting Algorithm :
// 1) Bubble Sort :Using Recursion


import java.io.*;

class Demo {

	static int sort(int arr[],int N){
	
		if(N == 0)
			return N;

		for(int j=0;j<arr.length-1;j++){
	
			if(arr[j] > arr[j+1]){
				
				int temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		
		}	
		return sort(arr,N-1);
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		int N = arr.length-1;
		sort(arr,N);

		for(int i=0;i<arr.length;i++)
			System.out.print(arr[i]+" ");

		System.out.println();
	}
}
