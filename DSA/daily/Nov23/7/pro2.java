
// Sorting Algorithm :
// 1) Bubble Sort :


import java.io.*;

class Demo {

	static void sort(int arr[]){
	
		int count = 0;
		for(int i=0;i<arr.length;i++){
		
			for(int j=0;j<arr.length-i-1;j++){
				
				count++;
				if(arr[j] > arr[j+1]){
				
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
			}
		}
		System.out.println(count);
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		sort(arr);

		for(int i=0;i<arr.length;i++)
			System.out.print(arr[i]+" ");

		System.out.println();
	}
}
