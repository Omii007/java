
/* 2nd step for Merge Sort:
 * 2 Sorted Array Merge Karayche.
 */

import java.io.*;

class Demo {

	static void merge(int arr1[],int arr2[],int arr3[]){
	
		int i=0,j=0,k=0;

		while(i < arr1.length && j < arr2.length){
		
			if(arr1[i] < arr2[j]){
			
				arr3[k] = arr1[i];
				i++;
			}else{
			
				arr3[k] = arr2[j];
				j++;
			}
			k++;
		}

		while(i < arr1.length){
		
			arr3[k] = arr1[i];
			i++;
			k++;
		}

		while(j < arr2.length){
		
			arr3[k] = arr2[j];
			j++;
			k++;
		}
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array1 size:");
		int size1 = Integer.parseInt(br.readLine());
		
		System.out.println("Enter array2 size:");
		int size2 = Integer.parseInt(br.readLine());

		int arr1[] = new int[size1];
		int arr2[] = new int[size2];

		System.out.println("Enter array1 element:");
		for(int i=0;i<arr1.length;i++){
			
			arr1[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter array2 element:");
		for(int i=0;i<arr2.length;i++){
			
			arr2[i] = Integer.parseInt(br.readLine());
		}
		int size3 = size1+size2;
		int arr3[] = new int[size3];

		merge(arr1,arr2,arr3);

		for(int i=0;i<arr3.length;i++)
			System.out.print(arr3[i]+" " );

		System.out.println();
	}
}
