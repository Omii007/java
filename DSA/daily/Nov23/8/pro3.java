
// Merge Sort Algorithm:
/* 1) 1st step:
 */

import java.io.*;

class Demo {

	static void merge(int arr[],int start,int end){
	
		if(start < end){
		
			int mid = start+(end-start)/2;

			System.out.println("Start : "+start+"Mid : "+mid+"End : "+end);

			merge(arr,start,mid);
			merge(arr,mid+1,end);
		}
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());
		
		int arr[] = new int[size];

		System.out.println("Enter array element:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		int start = 0;
		int end = arr.length-1;
		merge(arr,start,end);
	}
}
