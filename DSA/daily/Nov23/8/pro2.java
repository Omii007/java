
// Selection Sort :
/* 1) 0th index la minimum index consider karaycha
 * 2) tyala eka variable madhe thevaycha.
 * 3) tya index chya element la other element shi compare karycha aani tya peksha small element sapadla tr tyala minimum index mnun consider karycha aani jo copy kelela index ahe tyachya jagi minimum index chya element la thevaych.
 */

import java.io.*;

class Demo {

	static void selection(int arr[]){
	
		for(int i=0;i<arr.length-1;i++){
		
			int minIndex = i;

			for(int j=i+1;j<arr.length;j++){
			
				if(arr[j] < arr[minIndex]){
				
					minIndex = j;
				}
			}
			int temp = arr[i];
			arr[i] = arr[minIndex];
			arr[minIndex] = temp;
		}
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array element:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		selection(arr);

		for(int i=0;i<arr.length;i++)
			System.out.print(arr[i] + " ");

		System.out.println();
	}
}
