
//Maximum SubArray Sum:  Using Prefix sum

/*- Given an integer array of size N.
 * Find the contiguous subarray (Containing atleast one number) which has the largest sum and return its sum
 * I/P : arr : [-2,1,-3,4,-1,2,1,-5,4]
 * O/P : 6
 * Explenation : [4,-1,2,1] has the largest sum = 6
 */

//TC = O(N^2)
//SP = O(N)

import java.io.*;

class Demo {

	static int sum(int arr[],int psum[],int size){

		psum[0] = arr[0];
		int max = Integer.MIN_VALUE;

		for(int i=1;i<arr.length;i++){
		
			psum[i] = arr[i] + psum[i-1];
		}

		for(int i=0;i<arr.length;i++){
		
			int sum = 0;
			for(int j=i;j<arr.length;j++){
			
				if(i == 0)
					sum = psum[i];
				else
					sum = psum[j] - psum[i-1];

				if(sum > max)
					max = sum;
			}
		}
		System.out.println("max "+max);
		
		return 0;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		int psum[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		sum(arr,psum,size);
	}
}
