
//Maximum SubArray Sum:

/*- Given an integer array of size N.
 * Find the contiguous subarray (Containing atleast one number) which has the largest sum and return its sum
 * I/P : arr : [-2,1,-3,4,-1,2,1,-5,4]
 * O/P : 6
 * Explenation : [4,-1,2,1] has the largest sum = 6
 */

import java.io.*;

class Demo {

	static int sum(int arr[],int size){

		int max = Integer.MIN_VALUE;
		int start = 0,end = 0;

		for(int i=0;i<arr.length;i++){
		
			int sumn = 0;
			for(int j=i;j<arr.length;j++){
			
				sumn = sumn + arr[j];

				if(sumn > max){
					max = sumn;
					start = i;
					end = j;
				}
			}
		}
		System.out.println("Maximum sum of subarray is = "+max+" range between "+start+" to "+end);
		return 0;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		sum(arr,size);
	}
}
