
// Sub Array : 
/* Find the shortest subarray containing both min & max
 * Arr : [1,2,3,1,3,4,6,4,6,3]
 *
 * Ans : Minsub = [3,6]
 * 	 Maxsub = [0,8]
 */
