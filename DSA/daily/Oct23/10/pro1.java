
/* 1) Given an character array return the count of pair(i,j).
 */

class Demo {

	public static void main(String[] args){
	
		char ch[] = new char[]{'b','a','a','a','c','g','g','g'};

		int pair = 0,count = 0;

		for(int i=0;i<ch.length;i++){
		
			if(ch[i] == 'a')
				count++;
			else if(ch[i] == 'g')
				pair = pair + count;
		}
		System.out.println("Count = "+pair);
	}
}
