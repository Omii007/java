
/* 2) Equilibrium index of an array.
 * You are given an array A of integers of size N.
 * Your task is to find the equilibrium index of the given array.
 * The equilibrium index of an array is an index such that the sum of elements at lower indexes is equal to the sum of elements at higher indexes.
 * If there are no elements that are at lower indexes or at higher indexes, then the corresponding sum of elements is considered as 0.
 * NOTE : array index start from 0
 * If there is no equlibrium index than return -1
 * If there are more than one equlibrium index then return the minimum index.
 *
 * Problem Constraints
 * 1<= N <= 10^5
 * -105 <= A[i] <= 105
 *
 *  Ex1: A=[-7,1,5,2,-4,3,0]
 *  O/P : 3
 *  Ex2 : A=[1,2,3]
 *  O/P : -1
 */

// TC = O(N^2)

class Demo {

	public static void main(String[] args){
	
		int arr[] = new int[]{-7,1,5,2,-4,3,0};

		int flag = 0;
		for(int i=0;i<arr.length;i++){
		
			int leftsum = 0;
			int rightsum = 0;

			for(int j=0;j<i;j++){
			
				leftsum = leftsum + arr[j];
			}
			for(int j=i+1;j<arr.length;j++){
			
				rightsum = rightsum + arr[j];
			}
			if(leftsum == rightsum){
			
				flag = 1;
				System.out.println(i);
				break;
			}
		}
		if(flag == 0)
			System.out.println("-1");
	}
}
