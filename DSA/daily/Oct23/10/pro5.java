
/* SubArray : 
 * 1) Given an integer array of size N.
 * Return the length of the smallest subarray with contains both the maximum of the array and the minimum of the array
 * Arr[] : [1,2,3,1,3,4,6,4,6,3]
 */

import java.io.*;

class Demo {

	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;

		for(int i=0;i<arr.length;i++){
		
			if(arr[i] > max)
				max = arr[i];
		}
		System.out.println(max);
		for(int i=0;i<arr.length;i++){
		
			if(arr[i] < min)
				min = arr[i];
		}
		System.out.println(min);

		int length = 0;
		int minlength = Integer.MAX_VALUE;

		for(int i = 0;i<arr.length;i++){
			
			if(arr[i] == min){			
				for(int j=i+1;j<arr.length;j++){
			
					if(arr[j] == max){
				
						length = j- i + 1;
						if(minlength > length)
							minlength = length;
					}
					
				}
				
			}else if(arr[i] == max){
			
				for(int j=i+1;j<arr.length;j++){
				
					if(arr[j] == min){
						length = j - i +1;
						if(minlength > length)
							minlength = length;
					}
				}
			}
			
		
		}
		System.out.println(minlength);

	}
}
