
// Array

/* 1) Given an array of size N.
 * print all elements.
 * Arr [] : [5,6,2,3,1,9]
 */

class Demo {

	public static void main(String[] args){
	
		int N = 6;
		int arr[] = new int[]{5,6,2,3,1,9};

		for(int i=0;i<arr.length;i++){
		
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}
