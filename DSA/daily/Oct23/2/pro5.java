
/* Q 4) Reverse the array
 * I/P : arr[] = [1,2,3,4,5,6,7,8,9,10]
 * N = 10
 * TC = O(N)
 */

class Demo {

	public static void main(String[] args){
	
		int arr[] = new int[] {1,2,3,4,5,6,7,8,9,10};

		int i = 0;
		int end = arr.length - 1;

		while(i < end){
		
			int temp = arr[i];
			arr[i] = arr[end];
			arr[end] = temp;
			i++;
			end--;
		}
		for(int j=0;j<arr.length;j++){
		
			System.out.print(arr[j] + " ");
		}
		System.out.println();
	}
}
