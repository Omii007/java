
/* 2) Given an integer array of size N.
 * Count the no. of elements having at least 1 element greater than itself.
 * arr[] = [2,5,1,4,8,0,8,1,3,8]
 * N= 10
 * O/P : 7
 */


class Demo {

	public static void main(String[] args){
	
		int arr[] = new int[] {2,5,1,4,8,0,8,1,3,8};

		int count = 0;
		int max = Integer.MIN_VALUE;

		for(int i=0;i<arr.length;i++){
		
			if(max < arr[i]){
			
				max = arr[i];
			}
		}

		for(int i=0;i<arr.length;i++){
		
			if(max == arr[i])
				count++;
		}
		System.out.println(arr.length - count);
	}
}
