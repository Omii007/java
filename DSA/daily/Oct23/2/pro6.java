
/* Q 4) sum of the array elements
 * I/P : arr[] = [1,2,3,4,5,6,7,8,9,10]
 * N = 10
 * TC = O(N)
 */

class Demo {

	public static void main(String[] args){
	
		int arr[] = new int[] {1,2,3,4,5,6,7,8,9,10,11};

		int i = 0;
		int end = arr.length - 1;

		int sum = 0;
		while(i < end){
		
			sum = arr[i] + arr[end];
			System.out.print(sum + " ");
			i++;
			end--;
		}
		System.out.println();
		for(int j=0;j<arr.length;j++){
		
			System.out.print(arr[j] + " ");
		}
		System.out.println();
	}
}
