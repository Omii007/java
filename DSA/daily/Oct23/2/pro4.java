
/* 3) Given an array of size N.
 * Return the count of pairs(i,j) with Arr[i] + Arr[j] = k.
 * Arr[] : [3,5,2,1,-3,7,8,15,6,13]
 * N = 10
 * K = 10
 * O/P : 6
 * Note i != j
 */
// TC = O(N^2)

class Demo {

	public static void main(String[] args){
	
		int arr[] = new int[]{3,5,2,1,-3,7,8,15,6,13};

		int count =0,sum = 0,K = 10;
		for(int i=0;i<arr.length;i++){
		
			for(int j=0;j<arr.length;j++){
			
				if(i != j){
				
					sum = arr[i] + arr[j];
				}
				if(sum == K)
					count++;
			}
		}
		System.out.println(count);
	}
}
