
/* 2) Given an integer array of size N.
 * Build an array rightmax of size N.
 * rightmax of i contains the maximum for the index i to the index N-1
 *
 * Arr : [-3,6,2,4,5,2,8,-9,3,1]
 * N = 10
 * O/P : [8,8,8,8,8,8,8,3,3,1]
 */

import java.io.*;

class Demo {

	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		int rightarr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		rightarr[size-1] = arr[size-1];
		for(int i=arr.length-2;i>=0;i--){
		
			if(arr[i] > rightarr[i+1])
				rightarr[i] = arr[i];

			else
				rightarr[i] = rightarr[i+1];
		}
		for(int i=0;i<arr.length;i++)
			System.out.print(rightarr[i]+" ");

		System.out.println();
	}	
		
}
