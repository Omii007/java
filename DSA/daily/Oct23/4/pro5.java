
/* 3) Given an character array(lower case)
 * Return the count of pair (i,j) such that
 * a) i < j
 * b) arr[i] = 'a'
 * arr[j] = 'g'
 * Aee : [a,b,e,g,a,g]
 * O/P : 3
 */

import java.io.*;

class Demo {

	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		char arr[] = new char[size];

		System.out.println("Enter character elements:");
		for(int i=0;i<arr.length;i++){
			
			arr[i] = (char)br.read();
		}

		int count = 0;
		for(int i=0;i<arr.length;i++){
		
			for(int j=i+1;j<arr.length;j++){
			
				if(arr[i] == 'a' && arr[j] == 'g')
					count++;
			}
		}
		System.out.println("Count = "+count);
	}
}
