
/* 1) Given an integer array of size N.
 * - Build an array leftmax of size N.
 *   leftmax of i contains the maximum for the index 0 to the index i.
 *   I/P : N = 10
 *   Arr : [-3,6,2,4,5,2,8,-9,3,1]
 *   O/P : [-3,6,6,6,6,6,8,8,8,8]
 */

import java.io.*;

class Demo {

	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		int max = Integer.MIN_VALUE;

		for(int i=0;i<arr.length;i++){
		
			if(arr[i] > max)
				max= arr[i];

			System.out.print(max+" ");
		}
		System.out.println();
	}
}
