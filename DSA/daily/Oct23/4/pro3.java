
/* 1) Given an integer array of size N.
 * - Build an array leftmax of size N.
 *   leftmax of i contains the maximum for the index 0 to the index i.
 *   I/P : N = 10
 *   Arr : [-3,6,2,4,5,2,8,-9,3,1]
 *   O/P : [-3,6,6,6,6,6,8,8,8,8]
 */

import java.io.*;

class Demo {

	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		int leftarr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		leftarr[0] = arr[0];

		for(int i=1;i<arr.length;i++){
		
			if(arr[i] > leftarr[i-1])
				leftarr[i]= arr[i];
			
			else
				leftarr[i] = leftarr[i-1];
		}
		for(int i=0;i<arr.length;i++){
		
			 System.out.print(leftarr[i]+" ");
		}
		System.out.println();
	}
}
