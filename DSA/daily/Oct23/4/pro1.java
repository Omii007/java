
// To find the maximum number in array

import java.io.*;

class Demo {

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());
		
		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		int max = Integer.MIN_VALUE;
		
		for(int i=0;i<arr.length;i++){
		
			if(arr[i] > max)
				max = arr[i];
		}
		System.out.println("Max = "+max);	
	}
}
