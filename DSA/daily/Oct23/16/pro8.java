
/* 3] Given an array of size N.
 * int arr[] = new int[]{1,2,3};
 * Find the total sum of all subarraysum.
 */


import java.io.*;

class Demo {

	static int sumsubarray(int arr[],int size){
		
		int totalsum = 0;	
		for(int i=0;i<arr.length;i++){
		
			int sum = 0;
			for(int j=i;j<arr.length;j++){
				
				sum = sum + arr[j];
				totalsum = totalsum + sum;
			}

		}
		System.out.println("sum of subarray "+totalsum);
		return 0;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");

		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		sumsubarray(arr,size);
	}
}
