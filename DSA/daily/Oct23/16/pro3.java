

/* 2] Given an array of size N.
 * print all the elements in a given subarray from start to end.
 * int arr[] = new int[]{-2,1,-3,4,-1,2,1,-5,4}
 * start = 3
 * end = 7
 */

import java.io.*;

class Demo {

	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");

		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		int start = 3;
		int end = 7;
		for(int i=start;i<=end;i++){
		
			System.out.print(arr[i] +" ");
		}
		System.out.println();
	}
}
