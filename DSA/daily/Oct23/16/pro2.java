

/* Q 2] Find out number of subarray in the given array
 *
 * 	Arr[] = [4,2,10,3,12,-2,15]
 *
 * A . 49
 * B . 21
 * C . 28
 * D . 14
 *
 * Ans : 28
 *
 */

import java.io.*;

class subarray {

	static int subcnt(int arr[],int size){
	
		int count = 0;

		for(int i=0;i<arr.length;i++){
		
			for(int j=i;j<arr.length;j++){
			
				count++;
			}
		}
		return count;
	}
	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");

		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		int cnt = subcnt(arr,size);

		System.out.println("Count of subarray is = "+cnt);
	}
}
