
/* 4] Given array of size N.
 * print all the subarrays.
 * int arr[] = new int[] {2,4,1,3}
 */

import java.io.*;

class Demo {

	static int subarray(int arr[], int size){
	
		for(int i=0;i<arr.length;i++){
		
			for(int j=i;j<arr.length;j++){
			
				for(int k=i;k<=j;k++){
				
					System.out.print(arr[k] + " ");
				}
				System.out.println();
			}
		}
		return 0;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		subarray(arr,size);
	}
}
