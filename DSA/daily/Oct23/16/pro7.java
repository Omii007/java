
/* 3] Given an array of size N.
 * int arr[] = new int[]{2,4,1,3};
 * A. print the sum of every single subarray with time complexity o(N) & without extra space complexity (Carry Forward).
 */


import java.io.*;

class Demo {

	static int sumsubarray(int arr[],int size){
	
		for(int i=0;i<arr.length;i++){
		
			int sum = 0;
			for(int j=i;j<arr.length;j++){
				
				sum = sum + arr[j];
				System.out.println("sum of subarray "+sum);
			}
		}
		return 0;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");

		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		sumsubarray(arr,size);
	}
}
