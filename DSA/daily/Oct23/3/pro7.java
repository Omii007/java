
/* Array Rotation :
 *
 * - Given an integer array A of size N & an integer B.
 *   You have to return the same array after rotating i + B times towards the right
 *
 *   Problem Constarints :
 *   1 <= N <= 10^5
 *   1 <= A[i] <= 10^9
 *   1 <= B <= 10^9
 *
 *   I/P : A [1,2,3,4]
 *   B = 2
 *   O/P : [3,4,1,2]
 */

import java.io.*;

class Demo {

	static int rotation(int arr[],int size,int x){
	
		int k = 1;

		while(k <= x){
		
			int last = arr[0];
			for(int i=0;i<arr.length-1;i++){
			
				arr[i] = arr[i+1];
			}
			arr[arr.length-1] = last;
			k++;
		}
		return 0;
	}
	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter hoe many times array will rotates:");
		int rotate = Integer.parseInt(br.readLine());

		rotation(arr,arr.length,rotate);

		for(int i=0;i<arr.length;i++)
			System.out.print(arr[i] + " ");

		System.out.println();

	}
}
