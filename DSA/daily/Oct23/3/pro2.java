
/* 2) Given an array of size N & Q no of queries.
 * Query contains two parameters(s,e)
 * s => start index, e => end index for all queries.
 * printthe sum of all elements from index s to index e.
 *
 * Arr: [-3,6,2,4,5,2,8,-9,3,1]
 * N = 10
 * Q = 3
 */

// TC = O(Q * N)
// SP = O(1) 

import java.io.*;

class Demo {

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter numbers of Query:");
		int Query = Integer.parseInt(br.readLine());

		for(int i=0;i<Query;i++){
		
			int sum = 0;
			System.out.println("Enter start of index:");
			int start = Integer.parseInt(br.readLine());
			
			System.out.println("Enter end of index:");
			int end = Integer.parseInt(br.readLine());
			
			for(int j=start;j<=end;j++){
			
				sum = sum + arr[j];
			}
			System.out.println("Sum = "+sum);
		}

	}
}
