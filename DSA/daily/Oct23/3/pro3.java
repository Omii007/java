
/* 2) Given the score of the last 10 overs of a cricket match 288,312,330,349,360,383,394,406,436,439 How many were scored in the last 5 overs?
 *
 * A.  56
 * B.  79
 * C.  45
 * D.  101
 *
 * Explenation :  439 - 360 = 79
 * Ans = B.  79
 *
 *
 * 3) How many runs were scored in the 49th over(second last over)
 *
 * A. 3
 * B. 42
 * C. 30
 * D. 36
 *
 * Explenation :  436 - 406 = 30
 * Ans : C. 30
 *
 *
 * 4) How many runs were scored from 42th to 45th over
 * A. 72
 * B. 70
 * C. 48
 * D. 82
 *
 * Explenation : PS[s.e] = arr[j] - PS[i-1]
 * 		 360-288 = 72
 * Ans. 72
 */
