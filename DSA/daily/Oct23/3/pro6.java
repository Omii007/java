
/* Array Rotation :
 *
 * - Given an integer array A of size N & an integer B.
 *   You have to return the same array after rotating i + B times towards the right
 *
 *   Problem Constarints :
 *   1 <= N <= 10^5
 *   1 <= A[i] <= 10^9
 *   1 <= B <= 10^9
 *
 *   I/P : A [1,2,3,4]
 *   B = 2
 *   O/P : [3,4,1,2]
 */

import java.io.*;

class Demo {

	static int rotation(int arr[],int size,int x){
	
		int rr[] = new int[size];
		int k = 0;

		for(int i=x;i<rr.length;i++){
		
			rr[k] = arr[i];
			k++;
		}
		for(int i=0;i<x;i++){
		
			rr[k] = arr[i];
			k++;
		}
		for(int i=0;i<rr.length;i++)
			
			arr[i] = rr[i];
		return 0;
	}
	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter hoe many times array will rotates:");
		int rotate = Integer.parseInt(br.readLine());

		rotation(arr,arr.length,rotate);

		for(int i=0;i<arr.length;i++)
			System.out.print(arr[i] + " ");

		System.out.println();

	}
}
