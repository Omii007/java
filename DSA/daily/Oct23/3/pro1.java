
// Prefix Sum

/* Arr[] : [2,5,3,11,7,9,4]
 * sum of given range
 */

import java.io.*;

class Demo {

	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		
		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter starting index:");
		int start = Integer.parseInt(br.readLine());

		System.out.println("Enter last Index:");
		int end = Integer.parseInt(br.readLine());

		int sum = 0;
		for(int i=start;i<=end;i++){
		
			sum = sum + arr[i];
		}
		System.out.println("Sum = "+sum);
	}
}
