

import java.util.*;

class Demo {

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter array size:");
		int size = sc.nextInt();

		int arr[] = new int[size];
		int PS[] = new int[arr.length];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = sc.nextInt();
		}
		System.out.println("Enter number of Queries:");
		int Queries = sc.nextInt();
		int sum = 0;
		PS[0] = arr[0];

		PS[0] = arr[0];
		for(int i=1;i<arr.length;i++){
		
			PS[i] = arr[i] + PS[i-1]; 
		}

		for(int i=1;i<=Queries;i++){
		
			System.out.println("Enter starting index:");
			int start = sc.nextInt();
			
			System.out.println("Enter ending index:");
			int end = sc.nextInt();

			if(start == 0)
				sum = PS[end];
			else
				sum = PS[end] - PS[start-1];
		
			System.out.println(sum);
			
		}

	}
}
