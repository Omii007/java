
/* 5) In place prefix sum
 * Problem description :
 * - Given an array A of N integers.
 *   Construct prefix sum of the array in the given array itself.
 *   Return an array of integers denoting the prefix sum of given array.
 *   Problem Constraints :
 *   1 <= N <=10^5
 *   1 <= arr[i] <= 10^3
 *
 *   I/.P : A =[1,2,3,4,5]   A=[4,3,2]
 *   O/P : A = [1,3,6,10,15]   a=[4,7,9]
 */

 import java.io.*;

 class Demo {
 
 	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		for(int i=1;i<arr.length;i++){
		
			arr[i] = arr[i]+arr[i-1];
		}
		for(int i=0;i<arr.length;i++){
		
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
 }
