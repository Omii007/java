
/* 4) Given a N * N matrix.
 * print the boundary in clockwise function.
 * I/P : 1  2  3  4  5
 * 	 6  7  8  9  10
 * 	 11 12 13 14 15
 * 	 16 17 18 19 20
 * 	 21 22 23 24 25
 * 
 * O/P : 1 2 3 4 5 10 15 20 25 24 23 22 21 16 11 6
 */

class Demo {

	public static void main(String[] args){
	
		int arr[][] = new int[][]{{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}};

		int i = 0;
		int j = 0;

		for(int x=0;x<arr.length-1;x++){
		
			System.out.print(arr[i][j]+" ");
			j++;
		}
		for(int x=0;x<arr.length-1;x++){
		
			System.out.print(arr[i][j]+" ");
			i++;
		}
		for(int x=0;x<arr.length-1;x++){
		
			System.out.print(arr[i][j]+" ");
			j--;
		}
		for(int x=0;x<arr.length-1;x++){
		
			System.out.print(arr[i][j]+" ");
			i--;
		}
		System.out.println();
	}
}
