
/* 2) Given a square matrix of size N * N
 * convert the matrix to its Transpose Matrix.
 *
 * Transpose => row,col => col,row
 */

class Demo {

	public static void main(String[] args){
	
		int N = 5;
		int arr[][] = new int[][]{{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}};

		for(int i=0;i<N;i++){
		
			for(int j=i+1;j<N;j++){
			
				int temp = arr[i][j];
				arr[i][j] = arr[j][i];
				arr[j][i] = temp;
			}
		}
		for(int i=0;i<N;i++){
		
			for(int j=0;j<N;j++){
				System.out.print(arr[i][j]+" ");
			}
			System.out.println();
		}
	
	}
}
