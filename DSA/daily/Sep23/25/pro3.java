

// Order Of Time Complexity :

/* 1) O(1)  =>  Constant Time Complexity
 * 2) O(log2 N)  => Logarithmic TC
 * 3) O(sqrt N)  => Root TC
 * 4) O(N)  => Linear TC
 * 5) O(N log2 N)  => Linear Logarithmic TC
 * 6) O(N^2)  => Quadratic TC
 * 7) O(N^3)  => Cubaic TC
 * 8) O(2^N)  => Exponential TC
 * 9) O(N!)  => Factorial TC
 */
