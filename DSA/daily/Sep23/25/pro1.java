
// Time Complexity


import java.io.*;

class Demo {

	static int Diff(int num1,int num2){
	
		int count = 0;
		for(int i=num1;i<=num2;i++){
		
			count++;
		}
		return count;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number1:");
		int num1 = Integer.parseInt(br.readLine());
		
		System.out.println("Enter a number2:");
		int num2 = Integer.parseInt(br.readLine());

		int ret = Diff(num1,num2);
		System.out.println(ret);
	}
}
