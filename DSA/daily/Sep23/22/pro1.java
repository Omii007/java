
// Square Root

class problem {

	static int pp(int x){
	
		int num = 0;

		for(int i=1;i<=x;i++){   // iteration 100 times
		
			if(i*i == x){
				num = i;
			}
		}
		return num;
	}
	public static void main(String[] args){
	
		int x = pp(100);
		System.out.println(x);
	}
}
