

class squareroot {

	static int sqrt(int num){
	
		int end = num,start = 1,ans = 0,itr = 0;

		while(start <= end){
		
			itr++;
			int mid = (start+end)/2;

			int sqrt = mid * mid;

			if(sqrt == num){
			
				return mid;
			}
			if(sqrt > num){
			
				end = mid-1;
			}
			if(sqrt < num){
			
				start = mid+1;
				ans = mid;
			}
		}
		System.out.println(itr);
		return ans;
	}
	public static void main(String[] args){
	
		int num = 85;
		int ret = sqrt(num);
		System.out.println(ret);
	}
}
