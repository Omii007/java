

// QuickSort

import java.io.*;

class QuickSort {

	int partion(int arr[],int start,int end){
	
		int pivot = arr[end];
		int i = start - 1;

		for(int j=start;j<end;j++){
			
			if(arr[j] < pivot){		
					
				i++;
				int temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
		i++;
		int temp = arr[i];
		arr[i] = arr[end];
		arr[end] = temp;
		return i;
		
	}

	void sort(int arr[],int start,int end){
	
		if(start < end){
		
			int pivot = partion(arr,start,end);
			sort(arr,start,pivot-1);
			sort(arr,pivot+1,end);
		}
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		QuickSort obj = new QuickSort();

		obj.sort(arr,0,arr.length-1);

		for(int i=0;i<arr.length;i++){
		
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}
