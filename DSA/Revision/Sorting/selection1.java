

// Selection Sort

import java.io.*;

class SelectionSort {

	void sort(int arr[]){
	
		for(int i=0;i<arr.length-1;i++){
		
			int minIndex = i;

			for(int j = i+1;j<arr.length;j++){
			
				if(arr[j] < arr[minIndex]){
				
					minIndex = j;
				}
			}
			int temp = arr[minIndex];
			arr[minIndex] = arr[i];
			arr[i] = temp;
			
			for(int j=0;j<arr.length;j++){
				System.out.print(arr[i]+" ");
			}
			System.out.println();
		}
		System.out.println("---------------");
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array element:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		SelectionSort obj = new SelectionSort();

		obj.sort(arr);

		for(int i=0;i<arr.length;i++)

			System.out.print(arr[i] + " ");

		System.out.println();
	}
}
