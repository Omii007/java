

// Merge Sort

import java.io.*;

class MergeSort {

	void merge(int arr[],int start,int mid,int end){
	
		int n1 = mid-start+1;
		int n2 = end - mid;

		int arr1[] = new int[n1];
		int arr2[] = new int[n2];

		for(int i=0;i<n1;i++){
		
			arr1[i] = arr[start+i];
		}
		for(int j=0;j<n2;j++){
		
			arr2[j] = arr[mid+j+1];
		}

		int i=0, j=0, k=start;

		while(i < arr1.length && j < arr2.length){
		
			if(arr1[i] < arr2[j]){
			
				arr[k] = arr1[i];
				i++;
			}else{
			
				arr[k] = arr2[j];
				j++;
			}
			k++;
		}

		while(i < arr1.length){
		
			arr[k] = arr1[i];
			i++;
			k++;
		}
		while(j < arr2.length){
		
			arr[k] = arr2[j];
			j++;
			k++;
		}
	}
	void sort(int arr[],int start,int end){
	
		if(start < end){
		
			int mid = start + (end - start)/2;

			sort(arr,start,mid);
			sort(arr,mid+1,end);
			merge(arr,start,mid,end);
		}
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		MergeSort obj = new MergeSort();

		obj.sort(arr,0,arr.length-1);

		for(int i=0;i<arr.length;i++){
		
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
