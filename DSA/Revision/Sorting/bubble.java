

import java.io.*;

class BubbleSort {

	void sort(int arr[]){
	
		for(int i=0;i<arr.length;i++){
		
			boolean flag = false;
			for(int j=0;j<arr.length-i-1;j++){
			
				if(arr[j] > arr[j+1]){
				
					int temp = arr[j+1];
					arr[j+1] = arr[j];
					arr[j] = temp;
					flag = true;
				}
			}
			for(int j=0;j<arr.length;j++){
			
				System.out.print(arr[j]+" ");
			}
			System.out.println();
			if(flag == false)
				break;
		}
	}
	public static void main(String[] args)throws IOException {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		BubbleSort obj = new BubbleSort();
		obj.sort(arr);

		for(int i=0;i<arr.length;i++)

			System.out.print(arr[i]+" ");

		System.out.println();
	}

}
