
// N number addition.

// TC = O(1)  Using Gauss Thm
// SP C = O(1)

import java.io.*;

class SumN {

	static int sum(int num){
		
		int itr = 0; 
		int sum1 = num*(num+1)/2;
		
		System.out.println("Iteration : "+ ++itr);
		return sum1;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());

		int ret = sum(num);
		System.out.println("Sum of "+num+" number is "+ret);
	}
}
