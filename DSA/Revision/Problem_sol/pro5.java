

// Square root Using Binary Search

class sqrureroot {

	static int root(int num){
	
		int start = 1,end = num,ans = 0,iteration = 0;
		while(start <= end){
			iteration++;
			int mid = (start+end)/2;

			int sqrt = mid * mid;

			if(sqrt == num){
				System.out.println("Iteration : "+iteration);
				return mid;
			}

			if(sqrt > end){
				end = mid - 1;
				ans = mid;
			}else{
				start = mid + 1;
				ans = mid;
			}
			
		}
		System.out.println("Iteration : "+iteration);
		return ans;
	}
	public static void main(String[] args){
	
		int x = root(100);
		System.out.println(x);
	}
}
