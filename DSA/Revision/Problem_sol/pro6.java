
// N number addition.

// TC = O(N)
// SP C = O(1)

import java.io.*;

class SumN {

	static int sum(int num){
	
		int sum1 = 0,itr = 0;
		for(int i=1;i<=num;i++){
			
			itr++;
			sum1 = sum1 + i;
		}
		System.out.println("Iteration : "+itr);
		return sum1;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());

		int ret = sum(num);
		System.out.println("Sum of "+num+" number is "+ret);
	}
}
