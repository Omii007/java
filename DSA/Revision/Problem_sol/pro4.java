

// Square root Using Binary Search

class sqrureroot {

	static int root(int num,int start,int end,int iteration){
	
		int ans = 0;
		if(start <= end){
			iteration++;
			int mid = start + (end-start)/2;

			int sqrt = mid * mid;

			if(sqrt == num){
				System.out.println("Iteration : "+iteration);
				return mid;
			}

			if(sqrt > end){
				end = mid - 1;
			}else{
				start = mid + 1;
				ans = mid;
			}
			return root(num,start,end,iteration++);
		}
		System.out.println("Iteration : "+iteration);
		return ans;
	}
	public static void main(String[] args){
	
		int start = 1;
		int end = 100;
		int x = root(100,start,end,0);
		System.out.println(x);
	}
}
