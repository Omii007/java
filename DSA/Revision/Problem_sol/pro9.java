
// Difference between N numbers.

// TC = O(1)
// SP C = O(1)

import java.io.*;

class DiffN {

	static int diff(int num1,int num2){

		int itr = 0;
		
		int count = 0;
		if(num1 == 1)
			count = (num2 - num1)+1;
		else
			count = (num2 - num1);

		System.out.println("Iteration : "+ ++itr);
		return count;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number1:");
		int num1 = Integer.parseInt(br.readLine());
		
		System.out.println("Enter a number2:");
		int num2 = Integer.parseInt(br.readLine());

		int ret = diff(num1,num2);
		System.out.println("Difference between number "+num1+" and number "+num2+" is "+ret);
	}
}
