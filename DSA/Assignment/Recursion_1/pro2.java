
/* 2) WAP to display the first 10 natural number in reverse order .
 */

import java.io.*;

class Demo {

	static void OneToTen(int num){
	
		if(num == 0)
			return ;

		System.out.println(num);
		OneToTen(--num);
		
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());
		
		Demo obj = new Demo();

		obj.OneToTen(num);
	}
}
