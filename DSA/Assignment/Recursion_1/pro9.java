
/* 9) WAP to print string in reverse order.
 */


import java.io.*;

class Demo {

	static String rev(String str){
	
		if(str.isEmpty())
			return str;

		return rev(str.substring(1))+str.charAt(0);
		

	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a string:");
		String str = br.readLine();

		Demo obj = new Demo();
		String str1 = obj.rev(str);

		System.out.println(str1);
	}
}
