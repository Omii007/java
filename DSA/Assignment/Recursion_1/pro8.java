
/* 8) WAP to count the occurance of a specific digit in a given number.
 */

import java.io.*;

class Demo {

	
	static int OneToTen(int num,int x){
		
		int count = 0;
		if(num == 0)
			return 0;

		int rem = num % 10;

		if(rem == x)
			count++;

		return count + OneToTen(num/10,x);
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());
		
		System.out.println("Enter a occurance number:");
		int x = Integer.parseInt(br.readLine());
		
		Demo obj = new Demo();

		int ret = obj.OneToTen(num,x);

		System.out.println("count of a occurance is = "+ret);
	}
}
