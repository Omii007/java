
/* 1) WAP to print the number between 1 to 10.
 */

import java.io.*;

class Demo {

	static void OneToTen(int num){
	
		if(num == 11)
			return ;

		System.out.println(num);
		OneToTen(++num);
		
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());
		
		Demo obj = new Demo();

		obj.OneToTen(num);
	}
}
