
/* 5) WAP to check whether the number is prime or not.
 */

import java.io.*;

class Demo {

	
	static int OneToTen(int num,int i){
		int count = 0;
		
		if(count > 2)
			return 1;
		if(num % i == 0)
			 count++;
		if(i * i > num)
			return count;	
		
		return count + OneToTen(num,i+1);
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());
		
		Demo obj = new Demo();

		int ret = obj.OneToTen(num,2);

		if(ret == 1)

			System.out.println("Not prime number");
		else
			System.out.println(" prime number");
	}
}
