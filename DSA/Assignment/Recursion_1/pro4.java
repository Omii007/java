
/* 4) WAP to print the length of digits in a number .
 */

import java.io.*;

class Demo {

	
	static int OneToTen(int num){
		int count = 0;
		if(num == 0)
			return 0;	
		count++;
		return count + OneToTen(num/10);
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());
		
		Demo obj = new Demo();

		int ret = obj.OneToTen(num);

		System.out.println(ret);
	}
}
