
/* 10) WAP to check the whether the given number is palindrome or not.
 */

import java.io.*;

class Demo {

	static int palli(int num,int mult){

		if(num == 0)
			return mult;

		mult = (mult * 10) + (num%10);

		return palli(num/10,mult);
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());

		Demo obj = new Demo();

		int ret = obj.palli(num,0);

		if(ret == num)
			System.out.println("Number is pallindrome");
		else
			System.out.println("Number is not pallindrome");
	}
}
