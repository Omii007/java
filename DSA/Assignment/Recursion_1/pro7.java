
/* 7) WAP to find the factorial of a number.
 */

import java.io.*;

class Demo {

	
	static long OneToTen(long num){
		
		if(num == 0 ||num == 1)
			return 1;

		return num * OneToTen(--num);
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		//int num = Integer.parseInt(br.readLine());
		long num = Long.parseLong(br.readLine());
		
		Demo obj = new Demo();

		long ret = obj.OneToTen(num);

		System.out.println("Factorial of a number is = "+ret);
	}
}
