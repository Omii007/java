
/* 6) WAP to calculate the sum of digits of a given positive integer.
 */

import java.io.*;

class Demo {

	
	static int OneToTen(int num){
		int sum = 0;
		
		if(num == 0)
			return 0;

		sum = sum + (num %10);
		return sum + OneToTen(num/10);
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());
		
		Demo obj = new Demo();

		int ret = obj.OneToTen(num);

		System.out.println("Sum of integer digits are = "+ret);
	}
}
