
/* 6) WAP to check whether a given number is strong number or not.
 */

import java.io.*;

class Demo {

	static int factorial(int num){
	
		if(num == 0 || num == 1)
			return 1;
		else
			return num * factorial(num-1);
	}
	static int palin(int num,int sum){
	
		if(num == 0){
			return sum;
		}else{
			int rem = num % 10;
			int fact = factorial(rem);
			return palin(num/10,sum+fact);
		}

	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());

		Demo obj = new Demo();

		int x = 0;

		int ret = obj.palin(num,x);

		if(ret == num)
			System.out.println("Strong number");
		else
			System.out.println("Not Strong number");
	}	
}
