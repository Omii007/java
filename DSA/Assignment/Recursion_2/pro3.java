
/* 3) WAP to print the maximum digit in a given number.
 */

import java.io.*;

class Demo {

	static int maxdigit(int num,int digit){
	
		if(num == 0)
			return digit;

		int rem = num % 10;
		if(digit < rem)
			digit = rem;

		return maxdigit(num/10,digit);
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());

		Demo obj = new Demo();

		int x = Integer.MIN_VALUE;

		int ret = obj.maxdigit(num,x);

		System.out.println("Maximum digit a number "+num+" is "+ret);
	}
}
