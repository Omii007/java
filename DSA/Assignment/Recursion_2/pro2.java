
/* 2) WAP to print the product of a digit of a given number.
 */

import java.io.*;

class Demo {

	static int product(int num,int mult){
	
		if(num == 0)
			return mult;

		mult = mult * (num % 10);

		return product(num/10,mult);
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());

		Demo obj = new Demo();

		int ret = obj.product(num,1);

		System.out.println("product of digit a number "+num+" is "+ret);
	}
}
