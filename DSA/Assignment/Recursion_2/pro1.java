
/* 1) WAP to print the factorial of a given number.
 */

import java.io.*;

class Demo {

	static int fact(int num){
	
		if(num == 0)
			return 1;

		return num * fact(--num);
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());

		Demo obj = new Demo();

		int ret = obj.fact(num);

		System.out.println("Factorial of a number "+num+" is "+ret);
	}
}
