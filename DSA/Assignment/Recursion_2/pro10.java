
/* 10) WAP to determine whether a given number is Armstrong number or not. (An Armstrong number is a number that is equal to the sum of its own digits each raised to the power of the number of digits).
 */

import java.io.*;

class Demo {
	
	static int perf(int num,int count){
	
		if(num == 0)
			return 0;
		else
			return (int)Math.pow(num%10,count) + perf(num/10,count);

	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());

		Demo obj = new Demo();

		int x = 0;
		int num1 = num;
		int count = 0;
		while(num1 != 0){
		
			count++;
			num1 = num1/10;
		}

		int ret = obj.perf(num,count);

		if(ret == num)
			System.out.println(num +" is a Armstrong number");
		else
			System.out.println(num + " is not Armstrong number");
	}	
}
