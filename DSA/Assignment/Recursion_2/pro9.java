
/* 9) WAP to determine whether a given number is happy number or not. (A happy number is a number which eventually reaches 1 when replaced by the sum of the square of each digit).
 */

import java.io.*;

class Demo {

	static int perf(int num,int sum){
	
		if(num == 1)
			return 1;
		if(num == 0)
			return sum;
		
		sum += ((num%10)*(num%10));

		return perf(num/10,sum);

	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());

		Demo obj = new Demo();

		int x = 0;

		int ret = obj.perf(num,x);

		if(ret == 1)
			System.out.println(num +" is a happy number");
		else
			System.out.println(num + " is not happy number");
	}	
}
