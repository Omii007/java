
/* 8) WAP to check whether a given positive integer is a perfect number or not.
 */

import java.io.*;

class Demo {

	
	static int perf(int num,int i,int sum){
	
		if(i == num)
			return sum;

		if(num % i == 0)
			sum += i;

		return perf(num,i+1,sum);

	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());

		Demo obj = new Demo();

		int x = 0;

		int ret = obj.perf(num,1,x);

		if(ret == num)
			System.out.println(num +" is a perfect number");
		else
			System.out.println(num + " is not perfect number");
	}	
}
