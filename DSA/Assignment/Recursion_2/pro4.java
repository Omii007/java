
/* 4) WAP to print the sum of odd number upto a given number.
 */

import java.io.*;

class Demo {

	static int oddsum(int num,int sum){
	
		if(num == 0)
			return sum;

		if(num % 2 == 1)
			sum += num;

		return oddsum(--num,sum);
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());

		Demo obj = new Demo();

		int x = 0;

		int ret = obj.oddsum(num,x);

		System.out.println("Sum of odd number is "+ret);
	}
}
