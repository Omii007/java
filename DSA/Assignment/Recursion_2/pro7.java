
/* 7) WAP to check whether a given number is magic number or not (A Magic number is a number in which the eventual sum of the digits is equal to 1).
 */

import java.io.*;

class Demo {

	static int factorial(int num){
	
		if(num == 0)
			return 0;
		else
			return num % 10 +  factorial(num/10);
	}
	static int palin(int num){
	
		if(num <= 9){
			return num;
		}else{
			int sum = factorial(num);
			return palin(sum);
		}


	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());

		Demo obj = new Demo();

		int x = 0;

		int ret = obj.palin(num);

		if(ret == 1)
			System.out.println(num +" is a magic number");
		else
			System.out.println(num + " is not magic number");
	}	
}
