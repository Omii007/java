
/* 5) WAP to print the given number is palindrome or not.
 */

import java.io.*;

class Demo {

	static int palin(int num,int mult){
	
		if(num == 0)
			return mult;

		mult = mult * 10+(num % 10);

		return palin(num/10,mult);
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());

		Demo obj = new Demo();

		int x = 0;

		int ret = obj.palin(num,x);

		if(ret == num)
			System.out.println(" Number is palindrome");
		else
			System.out.println(" Number is not palindrome");
	}	
}
