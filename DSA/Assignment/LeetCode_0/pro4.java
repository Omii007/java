
/* 3] Search Insert Position (LeetCode-35)
 * Given a sorted array of distinct integers and a target value, return the index if the target is found. if not, return the index where it would be if it where inserted in order.
 * You must write an algorithm with O(log n) runtime complexity.
 *
 * Ex1 : 
 * I/P : num = [1,3,5,6] target = 5
 * O/P : 2
 *
 * Ex2 : 
 * I/P : num = [1,3,5,6] target = 2
 * O/P : 1
 *
 * Ex3 : num = [1,3,5,6] target = 7
 * O/P : 4
 */

//T.C = log(n)

import java.io.*;

class Demo {
	static int insert(int arr[],int size,int key){
		
		int start = 0,end = size-1;
		
		if(arr[end] < key){
			
			return end+1;
		}
		if(arr[start] < key){

			return start;
		}
		
		while(start < end){
		
			int mid = start + (end - start)/2;

			if(arr[mid] == key)
				return mid;

			if(arr[mid] > key)
				end = mid-1;

			if(arr[mid] < key)
				start = mid+1;
		}
		return 0;
	}
	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");

		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		System.out.println("Enter insert key");
		int key = Integer.parseInt(br.readLine());

		int x = insert(arr,size,key);
		System.out.println(x);
	}
}
