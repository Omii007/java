
/* 2] Two Sum (LeetCode : 1)
 * Give an array of integer number and an integer target. return indices of the two numbers such that they add up to target.
 * You can may assure that each input would have exactly one solution, add you may not use the same element twice.
 * You can return the answer in any order.
 *
 * Ex1:
 * I/P : num = [2,7,11,15] , target = 9
 * O/P : O/P : [0,1]
 *
 * Ex2 :
 * I/P : num = [3,4,2] target = 6
 * O/P : [1,2]
 *
 * Ex3:
 * I/P : num = [3,3]
 * O/P : [0,1]
 */

import java.io.*;

class Demo{

	static int add(int arr[],int size,int key){
	
		for(int i=0;i<size-1;i++){
			int x = i+1;
			if(arr[i] + arr[x] == key){
			
				System.out.println(i +" "+x);
			}
		}
		return 0;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");

		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter key number:");
		int key = Integer.parseInt(br.readLine());

		int arr1 = add(arr,size,key);
	}
}
