
/* 1] Reverse Integer (Leetcode - 7)
 * Given a signed 32-bit integer x, return x with its digits reversed. if reverse x causes the values to go outside the signed 32-bit integer range[-231,231,-1] then return 0.
 * Assume the environment does not allow you to store 64-bit integers(signed or unsigned)
 *
 * EX :
 * I/P : x = 123
 * O/P : 321
 *
 * Ex2:
 * I/P x = -123
 * O/P : -321
 *
 * Ex3 : 
 * I/P : 120
 * O/P : 21
 *
 * constraints :
 * -231 <= x <= 231-1
 */

import java.io.*;

class Demo {

	static int rev(int num){
	
		int mult = 0;

		while(num != 0){
		
			int rem = num % 10;
			mult = mult * 10 + rem;
			num = num / 10;
		}
		return mult;
	}
	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());

		int x = rev(num);

		System.out.println("Reverse number is = "+x);
	}
}
