
// Array Assignment(Basic Array, Prefix Sum, Carry Forward)
/* 1] Max Min of an Array
 * Problem Description :
 * - Given an array of size N.
 *   You need to find sum of the maximum and minimum elements in the given array.
 *
 *   Problem Constraints
 *   1 <= N <= 105
 *   -109 <= A[i] <= 109
 *
 *   I/P : A = [-2,1,-4,5,3]
 *   O/P : 1
 *
 *   I/P : A = [1,3,4,1]
 *   O/P : 5
 */

import java.io.*;

class Demo{

	static int maxmin(int arr[],int size){
	
		int store1 = arr[0], store2 = arr[0];

		for(int i=0;i<size;i++){
		
			if(arr[i] < store1)
				store1 = arr[i];
		}
		for(int i=0;i<size;i++){
		
			if(arr[i] > store2)
				store2 = arr[i];
		}
		int x = store1 + store2;
		
		return x;
	}
	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<size;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		int x = maxmin(arr,size);

		System.out.println("Sum of MAX and MIN is = "+ x);
	}
}

