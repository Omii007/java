
/* 2] Linear Search - Multiple Occurances
 * Problem Description
 * Given an array A and integer B, find the number of occurances
 *
 * Problem Constraints
 * 1 <= B,A[i] <= 109
 * 1 <= length(A) <= 105
 *
 * I/P : A = [1,2,2] B = 2
 * O/P : 2
 *
 * I/P : A = [1,2,1]
 * O/P : 0
 */

import java.io.*;

class Demo {

	static int occu(int arr[],int size,int key){
	
		int count = 0;

		for(int i=0;i<size;i++){
		
			if(arr[i] == key)
				count++;
		}
		return count;
	}
	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<size;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		System.out.println("Enter occurance key:");
		int key = Integer.parseInt(br.readLine());

		int x = occu(arr,size,key);

		System.out.println("Occurance count is = "+x);
	}
}
