
/* 3] Range Sum Ouery
 * Problem Description
 * - You are given an integer array A of length N.
 *  You are also given a 2D integer array B with dimensions M * 2, where each row denotes a [L,R] query.
 *  For each query, you have to find the sum of all elements from L to R indices in A(0-indexed).
 *  More formally, find A[L] + A[L+1] + A[L+2] + A[L+3]+.....+A[R-1] + A[R] for each query.
 *   
 * Problem Constraint 
 *
 * 1<= N,M <= 103
 * 1<= A[i] <= 105
 * 0<= L <= R<N
 *
 * Ex1:
 * A = [1,2,3,4,5]
 * B = [[0,3],[1,2]]
 * O/P : [10,5]
 *
 * Ex2:
 * A = [2,2,2]
 * B = [[0,0],[1,2]]
 * O/P : [2,4]
 */

import java.io.*;

class Demo {

	static int sum(int arr[],int start,int end,int arr2[],int query){
	
		int sum = 0;
		for(int i=0;i<query;i++){
			for(int j=start;j<=end;j++){
		
				sum = sum+ arr[i];
			}
			arr2[i] = sum;
		}
		return 0;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter number of queries:");
		int query = Integer.parseInt(br.readLine());
		
		int arr2[] = new int[query];

		for(int i=1;i<=query;i++){

			System.out.println("Enter start for sum:");
			int start = Integer.parseInt(br.readLine());
		
			System.out.println("Enter end for sum:");
			int end = Integer.parseInt(br.readLine());
		
			sum(arr,start,end,arr2,query);
		}
		for(int i=0;i<arr2.length;i++)
			System.out.println(arr2[i]);

	}
}


