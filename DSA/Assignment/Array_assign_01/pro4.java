
/* 4] Time to equality
 * Problem Description
 * - Given an integer array a of size N.
 *   In one second, you can increase the value of one element by 1.
 *   Find the minimum time in second to make all elements of the array equal.
 *
 *   Problem Constraints
 *   1 <= N <= 1000000
 *   1 <= A[i] <= 1000
 *
 *   Ex1 : 
 *   A = [2,4,1,3,2]
 *   O/P : 8
 */


