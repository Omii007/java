
/* 2)  Given an array of non-negative integers respectively a number, implements a function to simulate the carry forward operation that occurs when adding 1 to the number represented by the array. The array represents the digits of the numbers, where the 0th index is the least significant digit. Your task is to handle the carry forward operation should return the resulting array.
 *
 * for eg : Give an array [1,9,9] representing the no 199, the function should return [2,0,0], representing the result of adding 1 to 199 with the carry forward properly handled.
 *
 * Consider edges cases such as when the number has trailing Zero or when the carry forward results in an additional digit. Optimize your solution for efficiency & discuss the time complexity & space complexity.
 *
 * I/P : [1,9,9]
 * O/P : [2,0,0]
 */

import java.io.*;

class Demo{

	static int number(int arr[],int size){
	
		int num = 0;

		for(int i=0;i<arr.length;i++){
		
			num = num*10+arr[i];
		}

		num = num + 1;
		int num1 = num;

		int count = 0;

		while(num != 0){
		
			count++;
			num = num/10;
		}
	
		int arr1[] = new int[count];

		for(int i=arr1.length-1;i>=0;i--){
		
			int rem = num1 % 10;
			arr1[i] = rem;
			num1 = num1/10;
		}
		for(int i=0;i<arr1.length;i++){
			
			System.out.print(arr1[i]+" ");
		}
		System.out.println();
		return 0;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		number(arr,size);
	}
}
