
/* 4) Given an array of positive integers nums & a positive integer target. return the minimal length of a subarray whose sum is greater than or equal to target. If there is no subarray return 0 .
 *
 * I/P : target = 7
 * num = [2,3,1,2,4,3]
 * O/P : 2
 */

import java.io.*;

class Demo {

	static int length(int arr[],int size,int target){
	
		int store1 = 0;
		for(int i=0;i<arr.length;i++){
		
			int sum = 0;
			
			for(int j=i+1;j<arr.length;j++){
			
				sum = sum + arr[j];
				if(target <= sum){
					store1 = j-i;
					break;
				}
			}
			
		}
		
		return store1;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter target sum:");
		int target = Integer.parseInt(br.readLine());
		int ret = length(arr,size,target);

		System.out.println("Minimal length is "+ret);
	}
}
