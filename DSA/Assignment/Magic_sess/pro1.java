
/* 1) Given an array containing integers. The problem is to find the sum of elements of the contiguous subarray having the smallest (minimum) sum.
 * I/P : arr[] = [3,-4,2,-3,-1,7,-5]
 * O/P : -6
 * subarray is {-4,2,-3,-1} = -6
 *
 * I/P : arr[] : [2,6,8,1,4]
 * O/P : 1
 */

import java.io.*;

class Demo {

	static int minsum(int arr[],int size){
	
		int min = Integer.MAX_VALUE;

		/*for(int i=0;i<arr.length;i++){
		
			if(arr[i] < min)
				min = arr[i];
		}*/

		int store1 = -1,store2 = -1;

		for(int i=0;i<arr.length;i++){
		
			int sum = 0;
			for(int j=i;j<arr.length;j++){
			
				sum = sum+arr[j];
				if(sum < min){
					
					min = sum;
					store1 = i;
					store2 = j;
				}
			}
		}
		System.out.println("Minimum sum of elements is "+min+" range between "+store1+" to "+store2);
		return 0;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		minsum(arr,size);
	}
}
