
/* 5) Given an array of size N-1 such that it only contains distinct in the range of 1 to N. find the missing elements.
 * I/P : N = 5
 * A[] = [1,2,3,5]
 * O/P : 4
 */


import java.io.*;

class Demo {

	static int range(int arr[],int size,int num){
	
		int x = num*(num + 1)/2;

		int sum = 0;
		for(int i=0;i<arr.length;i++){
		
			sum = sum + arr[i];
		}
		if(sum == x)
			return 0;
		else
			return (x - sum);
		
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());
		
		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array eleemnts:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		int ret = range(arr,size,num);
		
		System.out.println("Missing elements is "+ret);
	}
}
