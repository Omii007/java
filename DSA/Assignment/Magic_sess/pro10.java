
/* 10) You are given an array of size N containing integers.
 * Your task is to find the number of subarray that can be formed the given array.
 * A subarray is defined contiguous sequence of elements in the array.
 * I/P : arr : [1,2,3]
 * O/P : 6
 *
 */

import java.io.*;

class Demo {

	static int subarray(int arr[],int size){
	
		int x = size*(size+1)/2;
		return x;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		int ret = subarray(arr,size);

		System.out.println("count of subarray is "+ret);
	}
}
