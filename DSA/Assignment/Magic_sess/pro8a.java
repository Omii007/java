
/* 8) Given an unsorted array arr[] of size N. Rotate the array to the left (counter - clockwise direction) by D steps, where D is a positive Integer.
 *
 * Ex : I/P : N = 5  D = 2
 * arr[] : [1,2,3,4,5]
 * O/P : [3,4,5,1,2]
 */

import java.io.*;

class Demo {

	static int rotation(int arr[],int size,int x){
	
		int k = 1;

		while(k <= x){
		
			int last = arr[0];

			for(int i=0;i<arr.length-1;i++){
			
				arr[i] = arr[i+1];
			}
			arr[arr.length-1] = last;
			k++;
		}
		return 0;
	}

	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		System.out.println("Enter rotation number:");
		int rotate = Integer.parseInt(br.readLine());
		
		rotation(arr,size,rotate);

		for(int i=0;i<arr.length;i++){
		
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}
