
/* 6) Given an array of n positive no. The task is to find the first equilibrium point in an array. Equilibrium point in an array is an index(or position) such that the sum of all elements before that index is the same as the sum of elements after it.
 *
 * Note : Return equilibrium point in 1-based indexing, Return -1 if no such point exists.
 * I/P : N = 5
 * A : [1,3,5,2,2]
 * O/P : 3
 */

import java.io.*;

class Demo {

	static int eque(int arr[],int size){
	
		int flag = 0;
		for(int i=0;i<arr.length;i++){
		
			int leftsum = 0;
			int rightsum = 0;

			for(int j=0;j<i;j++){
			
				leftsum = leftsum + arr[j];
			}
			for(int j=i+1;j<arr.length;j++){
			
				rightsum = rightsum + arr[j];
			}
			if(leftsum == rightsum){
				flag = 1;
				return i;
			}

		}
		return -1;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		int ret = eque(arr,size);

		System.out.println("Equilibrium index is "+ret);
	}
}
