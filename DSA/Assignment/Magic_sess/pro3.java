
/* 3) Given a String S & an array of string words, determine S is a prefix string of words.
 * A string S is a prefix string of words if can be made by concatenating the first K strings in words.
 * Return true if S is a prefix of words, or false otherwise.
 *
 * I/P : S = "iloveleetcode"
 * 	 words = ["i","love","leetcode","apples"]
 * O/P : true
 * Explanation : S can be made by concatenating "i","love" & "leetcode" together.
 */


class Demo {

	public static void main(String[] args){
	
		String str = "iloveleetcode";

		//String str1[] = new String[str.length()];

		String str1[] = str.split(" ");

		/*for(int i=0;i<str1.length;i++){
		
			str1[i] = str.split(" ");
		}*/

		int flag = 0;
		for(int i=0;i<str1.length;i++){
		
			if(str1[i].equals(str))
				flag = 0;
		}
		if(flag == 0)
			System.out.println("true");
		else
			System.out.println("false");
	}
}
