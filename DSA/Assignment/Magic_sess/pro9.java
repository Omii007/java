
/* 9) Given a sorted array Arr(containing n elements with possible some duplicate. The task is to find the first and last occurance of an element x in the given array. 
*
 * Note : if the number x is not found in the array then return both the indices as -1.
 *I/P : N = 9  , X = 5
 Arr[] = [1,3,5,5,5,5,67,123,125]
 O/P : 2,5

 I/P : N = 9  X = 7
 Arr[] = [1,3,5,5,5,5,7,123,125]
 O/P : 6,6
 */

import java.io.*;

class Demo {

	static int found(int arr[],int size,int num){
	
		int store1 = -1;
		for(int i=0;i<arr.length;i++){
		
			if(num == arr[i]){
				store1 = i;
				break;
			}
		}
		int store2 = -1;
		for(int i=0;i<arr.length;i++){
		
			if(num == arr[i])
				store2 = i;
		}
		System.out.println("First and Last occurance of elements "+num+" is "+store1+" and "+store2);
	       return 0;	

	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		System.out.println("Enter array size:");
		int num = Integer.parseInt(br.readLine());

		found(arr,size,num);
	}
}
