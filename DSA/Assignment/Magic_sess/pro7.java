
/* 7) Given an array Arr[i] that contains N integers (may be positive ,negative or zero). Find the product of the maximum product subarray.
 * I/P : N = 5
 * Arr[] = [6,-3,-10,0,2]
 * O/P : 180
 */

import java.io.*;

class Demo {

	static int product(int arr[],int size){
	
		int max = Integer.MIN_VALUE;

		int store1 = -1,store2 = -1;

		for(int i=0;i<arr.length;i++){
		
			int mult = 1;
			for(int j=i;j<arr.length;j++){
			
				mult = mult * arr[j];

				if(mult > max){
					max = mult;
					store1 = i;
					store2 = j;
				}

			}
		}
		System.out.println("Maximum product of subarray is "+max+" between range "+store1+" to "+store2);
		return 0;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		product(arr,size);
	}
}
