
/* 10) Print all the permutations of string.
 * I/P : s = "AB"
 * O/P : "AB" , "BA"
 */

import java.io.*;

class Demo {

	static void Permutation(String str,String current){
	
		if(str.length() == 0){
			System.out.println(current);
		}else{
		
			for(int i=0;i<str.length();i++){
			
				char ch = str.charAt(i);
				String rem = str.substring(0,i) + str.substring(i+1);
				Permutation(rem,current+ch);
			}
		}

	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a string");
		String str = br.readLine();

		System.out.println("Permutations of "+str);
		Permutation(str,"");
	}
}
