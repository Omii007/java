
/* 3) WAP to convert all characters in the array to uppercase.
 */

import java.io.*;

class Demo {

	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a string");
		String str = br.readLine();

		char str1[] = str.toUpperCase().toCharArray();

		for(int i=0;i<str1.length;i++){
			
			System.out.print(str1[i] + " ");
			
		}
		System.out.println();
	}
}
