
/* 2) WAP to replace 'a' in a string with 'l'
 */

import java.io.*;

class Demo {

	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a string:");
		String str = br.readLine();

		char ch = 'l';
		char str1[] = str.toCharArray();

		for(int i=0;i<str.length();i++){
		
			if(str1[i] == 'a')
				str1[i] = ch;
		}
		System.out.println(str1);
	}
}
