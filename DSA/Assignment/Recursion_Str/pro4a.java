
/* 4) WAP to convert all characters in the array tolowercase.(Recursion)
 */

import java.io.*;

class Demo {

	static void lowercaseArray(char arr[],int index){
	
		if(index < arr.length){
		
			arr[index] = Character.toLowerCase(arr[index]);
			lowercaseArray(arr,index+1);
		}
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a string");
		String str = br.readLine();

		char arr[] = str.toCharArray();
		lowercaseArray(arr,0);

		System.out.println(arr);
	}
}
