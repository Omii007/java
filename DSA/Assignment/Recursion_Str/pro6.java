
/* 6) Compare two strings, if the same return true else false.
 */

import java.io.*;

class Demo {

	static boolean compare(String str1,String str2){
	
		if(str1.length() != str2.length())
			return false;

		for(int i=0;i<str1.length();i++){
		
			if(str1.charAt(i) != str2.charAt(i))
				return false;
		}
		return true;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter string str1");
		String str1 = br.readLine();
		
		System.out.println("Enter string str2");
		String str2 = br.readLine();

		boolean ret = compare(str1,str2);

		if(ret){
		
			System.out.println("Both string are equal");
		}else{
			
			System.out.println("Both string are not equal");	
		}
	}
}
