
/* 1) WAP to count the vowel in string. (Using Recursion)
 */

import java.io.*;

class Demo {

	static int cnt(char str){

		str = Character.toLowerCase(str);

		if(str == 'a' || str == 'e' || str == 'i' || str == 'o' || str == 'u')
			return 1;

		return 0;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a String");
		String str = br.readLine();

		str = str.toLowerCase();
		int count = 0;

		for(int i=0;i<str.length();i++){
		
			int ret = (Demo.cnt(str.charAt(i)));
					
			 if(ret == 1)
			 	count++;
			}

		System.out.println(count);
	}
}
