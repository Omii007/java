
/* 2) WAP to replace 'a' in a string with 'l' (Using Recursion)
 */

import java.io.*;

class Demo {

	static char rep(char arr[],int x,int i){
	
		if(i == x)
			return 0;

		if(arr[i] == 'a')
			arr[i] = 'l';

		return rep(arr,x,i+1);
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a string:");
		String str = br.readLine();

		char ch = 'l';
		char str1[] = str.toCharArray();

		rep(str1,str.length(),0);

		System.out.println(str1);
	}
}
