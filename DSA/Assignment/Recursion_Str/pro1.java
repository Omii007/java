
/* 1) WAP to count the vowel in string.
 */

import java.io.*;

class Demo {

	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a String");
		String str = br.readLine();

		str = str.toLowerCase();
		int count = 0;

		System.out.println(str.length());

		for(int i=0;i<str.length();i++){
		
			if(str.charAt(i) == 'a' || str.charAt(i) == 'e' || str.charAt(i) == 'i' || str.charAt(i) == 'o' || str.charAt(i) == 'u'){
			
				count++;
			}
		}
		System.out.println(count);
	}
}
