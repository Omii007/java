
/* 9) Given two String str1 and str2 return true if str2 is an anagram of str1, and false otherwise.
 * I/P : str1 = "listen" str2 = "silent"
 * O/P : true
 */

import java.io.*;

class Demo {

	static boolean Anagram(String str1,String str2){

		if(str1.isEmpty() && str2.isEmpty())
			return true;
			
		if(str1.length() != str2.length())
			return false;

		int index = str2.indexOf(str1.charAt(0));

		if(index != -1){
		
			String temp1 = str1.substring(1);
			String temp2 = str2.substring(0,index) + str2.substring(index + 1);

			return Anagram(temp1,temp2);
		}else{
		
			return false;
		}
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter string1");
		String str1 = br.readLine();
		
		System.out.println("Enter string2");
		String str2 = br.readLine();

		boolean ret = Anagram(str1,str2);

		if(ret)
			System.out.println("The strings are anagram");
		else
			System.out.println("The strings are not anagram");
	}
}
