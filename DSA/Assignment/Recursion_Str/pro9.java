
/* 9) Given two String str1 and str2 return true if str2 is an anagram of str1, and false otherwise.
 * I/P : str1 = "listen" str2 = "silent"
 * O/P : true
 */

import java.io.*;

class Demo {

	static boolean Anagram(String str1,String str2){
	
		if(str1.length() != str2.length())
			return false;

		//assumed all to be ASCII
		int count[] = new int[256];

		for(int i=0;i<str1.length();i++){
		
			count[str1.charAt(i)]++;
			count[str2.charAt(i)]--;
		}

		for(int val : count){
		
			if(val != 0)
				return false;

		}
		return true;

	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter string1");
		String str1 = br.readLine();
		
		System.out.println("Enter string2");
		String str2 = br.readLine();

		boolean ret = Anagram(str1,str2);

		if(ret)
			System.out.println("The strings are anagram");
		else
			System.out.println("The strings are not anagram");
	}
}
