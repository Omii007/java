
/* 10) Print all the permutations of string.
 * I/P : s = "AB"
 * O/P : "AB" , "BA"
 */

import java.io.*;

class Demo {

	static void Permutation(String str,String current){
	
		if(str.isEmpty()){
			System.out.println(current);
		}else{
		
			char ch = str.charAt(0);
			String rem = str.substring(1);
			Permutation(rem,current+ch);
			
			if(str.length() > 1){
			
				rem = str.substring(1) + str.charAt(0);
				Permutation(rem,current);
			}
		}

	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a string");
		String str = br.readLine();

		System.out.println("Permutations of "+str);
		Permutation(str,"");
	}
}
