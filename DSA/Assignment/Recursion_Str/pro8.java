
/* 8) WAP to divide a string into n equal parts. Where string length is multiple of n.
 * I/P : "aaaabbbbcccc"
 * N = 3
 * O/P : aaaa
 * 	 bbbb
 * 	 cccc
 */

import java.io.*;

class Demo {

	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter string");
		String str = br.readLine();

		System.out.println("Enter a number to string divide");
		int x = Integer.parseInt(br.readLine());

		int size = str.length()/x;

		String strArr[] = new String[size];

		for(int i=0;i<size;i++){
		
			strArr[i] = str.substring(x*i,x*(i+1));
		}

		for(int i=0;i<size;i++)
			System.out.println(strArr[i]);
	}
}
