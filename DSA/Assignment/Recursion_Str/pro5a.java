
/* 5) WAP to check whether a given string is a palindrome string or not.
 */


import java.io.*;

class Demo {

	static boolean palindrome(String str,int start,int end){

		if(start >= end)
			return true;

		if(str.charAt(start) != str.charAt(end))
			return false;
		
		return palindrome(str,start+1,end-1);
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a string");
		String str = br.readLine();

		int start = 0;
		int end = str.length()-1;
		boolean ret = palindrome(str,start,end);
		
		if(ret)
			System.out.println(str + " is palindrome");
		else
			System.out.println(str + " is not palindrome");
	}
}
