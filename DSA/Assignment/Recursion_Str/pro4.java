
/* 4) WAP to convert all character to lowercase.
 */

import java.io.*;

class Demo {

	static void lowercasearr(char arr[]){
	
		for(int i=0;i<arr.length;i++){
		
			arr[i] = Character.toLowerCase(arr[i]);
		}
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter string");
		String str = br.readLine();

		char arr[] = str.toCharArray();

		lowercasearr(arr);

		System.out.println(arr);
	}
}
