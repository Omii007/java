
/* 8) WAP to divide a string into n equal parts. Where string length is multiple of n.
 * I/P : "aaaabbbbcccc"
 * N = 3
 * O/P : aaaa
 * 	 bbbb
 * 	 cccc
 */

import java.io.*;

class Demo {

	static String[] Equal(String str,int x,String strArr[],int size){
	
		int temp = (strArr.length)-size;

		if(temp == strArr.length)
			return strArr;

		strArr[temp] = str.substring(x*temp,x*(temp+1));

		return Equal(str,x,strArr,size-1);
	}

	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter string");
		String str = br.readLine();

		System.out.println("Enter a number to string divide");
		int x = Integer.parseInt(br.readLine());

		int size = str.length()/x;

		String strArr[] = new String[size];

		String arr[] = Equal(str,x,strArr,size);

		for(int i=0;i<arr.length;i++)
			System.out.println(arr[i]);
	}
}
