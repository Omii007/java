
/* 3) WAP to convert all characters in the array to uppercase.
 */

import java.io.*;

class Demo {

	static void uppercaseArray(char arr[],int index){
	
		if(index < arr.length){
		
			arr[index] = Character.toUpperCase(arr[index]);
			uppercaseArray(arr,index+1);
		}
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a string");
		String str = br.readLine();

		char arr[] = str.toCharArray();
		uppercaseArray(arr,0);

		System.out.println(arr);
	}
}
