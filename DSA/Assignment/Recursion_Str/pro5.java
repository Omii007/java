
/* 5) WAP to check whether a given string is a palindrome string or not.
 */


import java.io.*;

class Demo {

	static boolean palindrome(String str){
	
		int start = 0;
		int end = str.length()-1;

		while(start < end){
		
			if(str.charAt(start) != str.charAt(end)){

				return false;
			}
			start++;
			end--;
		}
		return true;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a string");
		String str = br.readLine();

		boolean ret = palindrome(str);
		
		if(ret)
			System.out.println(str + " is palindrome");
		else
			System.out.println(str + " is not palindrome");
	}
}
