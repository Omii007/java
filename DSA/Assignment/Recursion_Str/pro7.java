
/* 7) WAP to print the result of removing duplicate from a given string.
 * I/P : str = HappyNewYear
 * O/P : HapyNewYr
 */

import java.io.*;

class Demo {

	static String RemoveDuplicate(String str){
	
		if(str.isEmpty())
			return str;

		char firstChar = str.charAt(0);

		String remstr = RemoveDuplicate(str.substring(1).replaceAll(String.valueOf(firstChar),""));

		return firstChar + remstr;
	}
	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a string");
		String str = br.readLine();

		String str1 = RemoveDuplicate(str);

		System.out.println("After string is = "+str1);
	}
}
