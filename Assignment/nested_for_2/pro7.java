/* 7. Write a program to print following  pattern.
 * 	1  2  9
 * 	4  25 6
 * 	49 8  81*/

import java.util.Scanner;

class patt {

	public static void main(String[] args){
	
		int rows,col,num;

		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();

		System.out.println("Enter col:");
		col = sc.nextInt();

		num = 1;

		for(int i=1;i<=rows*col;i++){
		
			if(num % 2 == 1){
			
				if(i % rows == 0){
				
					System.out.print(num*num+"\t");
					
					System.out.println(" ");
				}else{
					System.out.print(num*num+"\t");
					
				}
				num++;
			}else{
				if(i % rows == 0){
				
					System.out.print(num+"\t");
					
					System.out.println(" ");
				}else{
					System.out.print(num+"\t");
				
				}
				num++;
			
				
			}
		}
	}
}
