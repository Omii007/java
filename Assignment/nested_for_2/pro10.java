/* 10. Write a program to print following pattern.
 * 	F  5  D  3  B  1
 * 	F  5  D  3  B  1
 * 	F  5  D  3  B  1
 * 	F  5  D  3  B  1
 * 	F  5  D  3  B  1
 * 	F  5  D  3  B  1*/


import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int rows,col,num,ch;

		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();

		System.out.println("Enter col:");
		col = sc.nextInt();

		num = rows;
		ch = 64+rows;
		int j=1;

		for(int i=1;i<=rows*col;i++){
		
			if(j % 2 == 1){
			
				if(i % rows == 0){
				
					System.out.print((char)ch+"\t");
					System.out.println(" ");
					num = rows;
					ch = 64+rows;
				}else{
				
					System.out.print((char)ch+"\t");
					num--;
					ch--;
				}
				
			}else{
				if(i % rows == 0){
				
					System.out.print(num+"\t");
					System.out.println(" ");
					num = rows;
					ch = 64+rows;
				}else{
				
					System.out.print(num+"\t");
					num--;
					ch--;
				}
				
			}
			j++;
			
		
		}
	}
}
