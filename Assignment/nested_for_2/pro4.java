/* 4. Write a program to print following pattern.
 * 	1A  2B  3C
 * 	1A  2B  3C
 * 	1A  2B  3C*/

import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int rows,col,num,ch;

		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();

		System.out.println("Enter col:");
		col = sc.nextInt();

		ch = 62+rows;
		num = 1;

		for(int i=1;i<=rows*col;i++){
		
			if(i % rows == 0){
			
				System.out.print(num+""+(char) ch+"\t");
				System.out.println(" ");
				num = 1;
				ch = 62+rows;
			}else{
			
				System.out.print(num+""+(char) ch+"\t");
				ch++;
				num++;
			}
		}
	}
}
