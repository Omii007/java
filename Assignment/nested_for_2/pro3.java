/* 3. Write a program to print following pattern.
 * 	14  15  16  17
 * 	15  16  17  18
 * 	16  17  18  19
 * 	17  18  19  20*/

import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int rows,col,num;

		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();

		System.out.println("Enter col:");
		col = sc.nextInt();

		num = 10 + rows;

		for(int i=1;i<=rows*col;i++){
		
			if(i % rows == 0){
			
				System.out.print(num +"\t");
				System.out.println(" ");
				num-=2;

			}else{
			
				System.out.print(num+"\t");
				num++;
			}
		}
	}
}
