/* 1. Write a program to print followig pattern.
 * 	c2w1  c2w2  c2w3
 * 	c2w1  c2w2  c2w3
 * 	c2w1  c2w2  c2w3*/

import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int rows,col;
		String ch = "c2w";

		System.out.println("Enter rows:");
		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();

		System.out.println("Enter col:");
		col = sc.nextInt();
		
		int num = 1;

		for(int i=1;i<=rows*col;i++){

			if(i % rows == 0){
			
				System.out.print(ch +""+num+"\t");
				System.out.println(" ");
				num = 1;
			}else{
				
				System.out.print(ch +""+num+"\t");
				num++;
			}
		}
	}
}
