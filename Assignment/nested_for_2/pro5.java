/* 5. Write a program to print following pattern.
 * 	26  Z  25  Y
 * 	24  X  23  W
 * 	22  V  21  U
 * 	20  T  19  S*/

import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int rows,col,num,ch;

		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();

		System.out.println("Enter col:");
		col = sc.nextInt();

		num = 22 + rows;
		ch = 86 + rows;
		int j=1;
		for(int i=1;i<=rows*col;i++){
		
			if(j % 2 == 1){
				if(i % rows == 0){
					System.out.print(num+"\t");
					System.out.println(" ");
			
				}else{
				
					System.out.print(num+"\t");
					
				}
				num--;
				
			}else{
				if(i % rows == 0){
					System.out.print((char) ch+"\t");
					System.out.println(" ");
				
				}else{
					System.out.print((char) ch+"\t");
					
				}		
				ch--;
			}
			j++;
		}
	}
}
