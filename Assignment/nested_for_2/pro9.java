/* 9. Write a program to print following pattern.
 * 	1C3  4B2  9A1
 * 	16C3 25B2 36A1
 * 	49C3 64B2 81A1*/


import java.util.Scanner;

class pattern {

	public static void main(String[] args){
	
		int rows,col,num,num1,ch;

		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();

		System.out.println("Enter col:");
		col = sc.nextInt();

		num = 1;
		num1 = 3;
		ch = 64+rows;

		for(int i=1;i<=rows*col;i++){
		
			if(i % rows == 0){
			
				System.out.print(num*num+""+(char)ch+""+num1+"\t");
				System.out.println(" ");
				ch = 64+rows;
				num1 = 3;
				num++;
			}else{
			
				System.out.print(num*num+""+(char)ch+""+num1+"\t");
				num++;
				ch--;
				num1--;
			}
		}
	}
}
