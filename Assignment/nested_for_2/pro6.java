/* 6. Write a program to print following pattern.
 * 	1  4  9
 * 	16 25 36
 * 	49 64 81*/


import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int rows,col,num = 1;
		
		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();

		System.out.println("Enter col:");
		col = sc.nextInt();

		for(int i=1;i<=rows*col;i++){
		
			if(i % rows == 0){
			
				System.out.print(num*num+"\t");
				System.out.println(" ");
				num++;
			}else{
			
				System.out.print(num*num+"\t");
				num++;
			}
		}
	}
}
