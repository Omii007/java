/* 8. Write a program to print following pattern.
 * 	A  b  C  d
 *	E  f  G  h
 *	I  j  K  l
 *	M  n  O  p*/

import java.util.Scanner;

class patn {

	public static void main(String[] args){
	
		int rows,col,ch;

		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();

		System.out.println("Enter col:");
		col = sc.nextInt();

		ch = 61 + rows;
		int j=1;

		for(int i=1;i<=rows*col;i++){
		
			if(j % 2 == 1){
			
				if(i % rows == 0){
				
					System.out.print((char) ch+"\t");
					System.out.println(" ");
				}else{
				
					System.out.print((char) ch+"\t");
				}
				
			}else{
				int ch1= ch+32;	
				if(i % rows == 0){
				
					System.out.print((char) ch1 +"\t");
					System.out.println(" ");
				}else{
				
					System.out.print((char) ch1 +"\t");
				}
				
			
			
			}
			j++;
			ch++;
		}
		
	}
}
