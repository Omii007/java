/* 2. Write a program to print following pattern.
 * 	4  4  4  4
 * 	5  5  5  5
 * 	6  6  6  6
 * 	7  7  7  7*/

import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int rows,col,num;

		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();

		System.out.println("Enter col:");
		col = sc.nextInt();

		num = rows;

		for(int i=1;i<=rows*col;i++){
		
			if(i % rows == 0){		
				System.out.print(num+"\t");
				System.out.println(" ");
				num++;
			}else{
				System.out.print(num+"\t");
			}
		}
	}
}
