/* Que 2. WAP to reverse each elements in an array.
 * I/P : 10 25 252 36 364
 * O/P : 01 52 252 63 463*/

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size  = Integer.parseInt(br.readLine());
		
		int arr[] = new int [size];

		System.out.println("Enter array elements:");

		for(int i=0;i<size;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		int temp;
		for(int i=0;i<size;i++){
		
			temp = arr[i];
			int sum=0;

			while(temp != 0){
			
				int rem = temp % 10;
				sum = sum*10+rem;
				temp = temp/10;
			}
			System.out.println(arr[i]+" in reverse is "+sum);
		}
	}
}
