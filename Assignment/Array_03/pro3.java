/* Que 3. WAP to find a composite number from an array & return its index.
 * I/P : 1 2 3 5 6 7
 * O/P : Composite number 6 found at index 4*/

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size  = Integer.parseInt(br.readLine());
		
		int arr[] = new int [size];

		System.out.println("Enter array elements:");

		for(int i=0;i<size;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		int temp;
		for(int i=0;i<size;i++){
		
			temp = arr[i];
			int count = 0;
			int Index = 0;

			for(int j=2;j<temp;j++){
			
				if(temp % j == 0){
				
					count++;
				}
			}
			if(count >= 1){
			
				Index = i;
				System.out.println("Composite "+arr[i]+" found at index "+Index);
			}	
		}
		
	}
}
