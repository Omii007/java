/* Que 5. WAP to find a perfect number from an array & return its index.
 * I/P : 10 25 252 496 564
 * O/P : Perfect number 496 found at index 3.*/

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size  = Integer.parseInt(br.readLine());
		
		int arr[] = new int [size];

		System.out.println("Enter array elements:");

		for(int i=0;i<size;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		int temp;
		for(int i=0;i<size;i++){
		
			temp = arr[i];
			int sum=0;
			int Index = 0;

			for(int j=1;j<temp;j++){
			
				if(temp % j == 0){
				
					sum = sum + j;
				}
			
			}if(arr[i] == sum){
				Index = i;
				System.out.println("Perfect number "+arr[i]+" found at index "+Index);
			}
			
		}
	}
}
