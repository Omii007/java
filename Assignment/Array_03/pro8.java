/* Que 8. WAP to find a Armstrong number from an array & return its index.
 * I/P : 10 25 252 36 153 55 89
 * O/P : Armstrong number 153 found at index 4.*/

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size  = Integer.parseInt(br.readLine());
		
		int arr[] = new int [size];

		System.out.println("Enter array elements:");

		for(int i=0;i<size;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		int temp;
		for(int i=0;i<size;i++){
		
			temp = arr[i];
			int sum=0,count=0;
			
			while(temp != 0){
			
				count++;
				temp = temp/10;
			}
			temp = arr[i];
			while(temp != 0){
				int mult = 1;
				int rem = temp % 10;
				for(int j=1;j<=count;j++){
					mult = mult*rem;
				}
				sum = sum+mult;
				temp = temp/10;
			
			}
			if(arr[i] == sum){
				
				System.out.println("Strong number "+arr[i]+" found at index "+i);
			}
			
		}
	}
}
