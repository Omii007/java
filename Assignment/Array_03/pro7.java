/* Que 7. WAP to find a Strong number from an array & return its index.
 * I/P : 10 25 252 36 564 145
 * O/P : Strong number 145 found at index 5.*/

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size  = Integer.parseInt(br.readLine());
		
		int arr[] = new int [size];

		System.out.println("Enter array elements:");

		for(int i=0;i<size;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		int temp;
		for(int i=0;i<size;i++){
		
			temp = arr[i];
			int sum=0;
			int Index = 0;

			while(temp != 0){
				int mult = 1;
				int rem = temp % 10;
				for(int j=2;j<=rem;j++){
					mult = mult*j;
				}
				sum = sum+mult;
				temp = temp/10;
			
			}
			if(arr[i] == sum){
				Index = i;
				System.out.println("Strong number "+arr[i]+" found at index "+Index);
			}
			
		}
	}
}
