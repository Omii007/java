/* Que 4. WAP to find a prime number from an array & return its index.
 * I/P : 10 25 36 566 34 53 50 100
 * O/P : Prime number 53 found at index 5*/

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter array size:");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int [size];

		System.out.println("Enter array elements:");

		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		for(int i=0;i<arr.length;i++){
		
			int temp = arr[i];
			int count = 0;
			int Index = 0;

			for(int j=2;j<temp;j++){
			
				if(temp % j == 0){
				
					count++;
				}
			}
			if(count == 0){
			
				Index = i;
				System.out.println("Prime number "+arr[i]+" found at index "+Index);
			}
		}
	}
}
