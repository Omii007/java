/* Que 1.WAP to print count of digits in elements of array.
 * I/P : array = [02 255 2 1554]
 * O/P : 2 3 1 4*/

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size  = Integer.parseInt(br.readLine());
		
		int arr[] = new int [size];

		System.out.println("Enter array elements:");

		for(int i=0;i<size;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		int temp;
		for(int i=0;i<size;i++){
		
			temp = arr[i];
			int count = 0;

			while(temp != 0){
			
				int rem = temp % 10;
				count++;
				temp = temp/10;
			}
			System.out.println("Digit "+arr[i]+" "+count);
		}
	}
}
