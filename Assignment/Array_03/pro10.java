/* Que 10. WAP to print the second min elements in the array.
 * I/P : 255 2 1554 15 65 95 89
 * O/P : 15 */

import java.io.*;

class Demo{
	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter array size :");
		
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];

		System.out.println("Enter array elements :");
		for(int i=0;i<arr.length;i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}
                
		int temp;
		for(int i=0;i<arr.length;i++) {
			for(int j=i+1;j<arr.length;j++){
				
				if(arr[i] < arr[j]) {
					temp  = arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}
			}
		}
			System.out.println("Second min element = "+arr[size-2]);
	}
}
