import java.io.*;
class Program7{

	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Number Of Rows:");
		
		int row = Integer.parseInt(br.readLine());
		int num = row*(row+1)/2; //Gauss theorem.

		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				if(row%2==0){
					if(i%2!=0){
						System.out.print(num+" ");
						num--;
					}else{
						
						System.out.print(((char)(num+64))+" ");
						num--;
					}

				}else{
						if(i%2!=0){	
						System.out.print(((char)(num+64))+" ");
						num--;
						}else{
							System.out.print(num+" ");
							num--;
						}
				}

			}
			System.out.println();
		}

	}
}
