import java.io.*;
class Program6{

	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter Character1:");	
		char ch1 = (char)br.read();
	
		br.skip(1);
		System.out.println("Enter Character2:");
		char ch2 = (char)br.read();
		

		if(ch1==ch2)
			System.out.println("Difference Is Zero");
		else if(ch1>ch2)
			System.out.println("Difference between "+ch1+" & "+ch2+" is "+(ch1-ch2));
		else	
			System.out.println("Difference between "+ch1+" & "+ch2+" is "+(ch2-ch1));
		
			
		}

	}

