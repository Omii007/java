import java.io.*;
class Program10{

	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter start:");
		int start = Integer.parseInt(br.readLine());
		System.out.println("Enter end:");
		int end = Integer.parseInt(br.readLine());
	

		for(int i=start;i<=end;i++){
			
			int count=0;
			
			for(int j=2;j<=i/2;j++){

				if(i%j==0)
					count++;
			}
			if(count==0)
				System.out.print(i+" ");

		}
		System.out.println();
	}

}

