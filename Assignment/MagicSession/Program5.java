import java.io.*;
class Program5{

	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter Number Of Rows:");
		
		int row = Integer.parseInt(br.readLine());
		int t1=0,t2=1,t3=0;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				t1=t3;
				t3=t2;
				System.out.print(t1+"\t");
				t2=t1+t3;	
			}
			System.out.println();

		}
			
		}

	}

