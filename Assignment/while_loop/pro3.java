/* 3. Write a program to count the digit of given number.
 * I/P : 942111423
 * O/P : count=9*/


import java.util.Scanner;

class cnt {

	public static void main(String[] args){
	
		long num,count=0;

		System.out.println("Enter a number:");

		Scanner sc = new Scanner(System.in);
		num = sc.nextLong();

		while(num > 0){
		
			count++;
			num = num/10;
		}
		System.out.println(count);

	}
}
