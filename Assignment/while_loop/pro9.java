/* 9. Write a program in reverse order.
 * I/P : 942111423
 * O/P : 324111249*/

import java.util.Scanner;

class reverse {

	public static void main(String[] args){
	
		long num,rem,rev = 0;

		System.out.println("Enter number::");

		Scanner sc = new Scanner(System.in);
		num = sc.nextLong();

		while(num != 0){
		
			rem = num % 10;
			rev = rev * 10 + rem;
			num = num / 10;
		}
		System.out.println(rev);
	}
}
