/* 6. Write a program to print the sum of all even number & multiplication of odd number between 1 to 10.
 * O/P : sum of even number between 1 to 10 = 30
 * 	 multiplication of odd number between 1 to 10 = 945*/


import java.util.Scanner;

class bet {

	public static void main(String[] args){
	
		int num,i=1,sum=0,mult=1;

		System.out.println("Enter number::");

		Scanner sc = new Scanner(System.in);
		num = sc.nextInt();

		while(i <= num){
		
			if(i % 2 == 0){

				sum = sum + i;
				i++;
			}else{
			
				mult = mult * i;
				i++;
			}
		}
		System.out.println("sum = "+sum);
		System.out.println("mult ="+mult);
	}
}
