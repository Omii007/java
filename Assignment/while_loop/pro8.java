/* 8. Write a program to print the countdown of days to submit the assignment.
 * I/P : days = 7
 * O/P : 7 days remaining
 * 	 6 days remaining
 * 	 .
 * 	 .
 * 	 0 days Assignment is overdue*/


import java.util.Scanner;

class count {

	public static void main(String[] args){
	
		int days;

		System.out.println("Enter days::");

		Scanner sc = new Scanner(System.in);

		days = sc.nextInt();

		while(days >= 0){
		
			if(days > 0){
			
				System.out.println(days +" days remaining");
				days--;
			}else{
			
				System.out.println(days +" days Assignment is overdue");
				days--;
			}
		}
		
	}
}
