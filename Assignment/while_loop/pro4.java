/* 4. Write a program to count the odd digits of the given number.
 * I/P : 942111423
 * O/P : 5*/


import java.util.Scanner;

class odd {

	public static void main(String[] args){
	
		long num,count=0,rem;

		System.out.println("Enter a number:");

		Scanner sc = new Scanner(System.in);

		num = sc.nextLong();

		while(num > 0){
			
			rem = num % 10;
			if(rem % 2 == 1){
			
				count++;

			}
			num = num/10;
		}
		System.out.println(count);
	}
}
