/* 5. Write a program to print the square of even digits of given number
 * I/P : 942111423
 * O/P : 4 16 4 16*/


import java.util.Scanner;

class Demo{

	public static void main(String[] args){
	
		long num,rem;

		System.out.println("Enter number::");

		Scanner sc = new Scanner(System.in);
		num = sc.nextLong();

		while(num > 0){
		
			rem = num % 10;
			if(rem % 2 == 0){
			
				System.out.println(rem * rem);
			}
			num = num / 10;
		}
	}
}
