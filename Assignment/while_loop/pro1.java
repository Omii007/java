/* 1. Write a program to print a table of 2.
 * I/P : 2
 * O/P : 2 4 6 8 10 12 14 16 18 20*/


import java.util.Scanner;

class table {

	public static void main(String[] args){
	
		int num,i=1;

		System.out.println("Enter a number");

		Scanner sc = new Scanner(System.in);
		num = sc.nextInt();

		while(i <= 10){
		
			System.out.println(num * i);
			i++;
		}
	}
}
