/* 7. Write a program which takes number from user if number is even print that no. in reverse order, if number is odd print that number in reverse order by difference two?
 * I/P : 6
 * O/P : 6 5 4 3 2 1
 * I/P : 7
 * O/P : 7 5 3 1*/

import java.util.Scanner;

class pro {

	public static void main(String[] args){
	
		int num;

		System.out.println("Enter number::");

		Scanner sc = new Scanner(System.in);
		num = sc.nextInt();

		if(num % 2 == 0){
		
			while(num > 0){
			
				System.out.println(num);
				num--;
			}
		}else{
		
			while(num > 0){
			
				System.out.println(num);
				num-=2;
			}
		}
	}
}
