/* 2. Write a program to calculate the factorial of given number.
 * I/P : 6
 * O/P : 720*/

import java.util.Scanner;

class facto {

	public static void main(String[] args){
	
		int num,fact=1;

		System.out.println("Enter a number:");

		Scanner sc = new Scanner(System.in);
		num = sc.nextInt();

		while(num > 1){
		
			fact = fact * num;
			num--;
		}
		System.out.println(fact);
	}
}
