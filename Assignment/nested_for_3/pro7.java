/* 7. Write a program to print following pattern.
 * 	F
 * 	E  F
 * 	D  E  F
 * 	C  D  E  F
 * 	B  C  D  E  F
 * 	A  B  C  D  E  F*/


import java.util.Scanner;

class patt {

	public static void main(String[] args){
	
		int rows,col,ch;

		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();

		System.out.println("Enter col:");
		col = sc.nextInt();

		for(int i=1;i<=rows;i++){
		
			ch = 65+rows-i;
			for(int j=1;j<=i;j++){
			
				System.out.print((char) ch+"\t");
				ch++;
			}
			System.out.println(" ");
		}
	}
}
