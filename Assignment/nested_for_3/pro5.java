/* 5. Write a program to print following pattern.
 *	9
 *	9  8
 *	9  8  7
 *	9  8  7  6*/


import java.util.Scanner;

class dm {

	public static void main(String[] args){
	
		int rows,col,num;

		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();

		System.out.println("Enter col:");
		col = sc.nextInt();

		for(int i=1;i<=rows;i++){
		
			num = 9;
			for(int j=1;j<=i;j++){
			
				System.out.print(num+"\t");
				num--;
			}
			System.out.println(" ");
		}
	}
}
