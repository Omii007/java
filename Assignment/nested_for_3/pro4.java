/* 4. Write a program to print following pattern.
 * 	3C  3C  3C  3C
 * 	3C  3C  3C
 * 	3C  3C
 * 	3C*/


import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int rows,col,num,ch;

		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();

		System.out.println("Enter col:");
		col = sc.nextInt();

		ch = 63 +rows;
		num = 3;

		for(int i=1;i<=rows;i++){
		
			for(int j=1;j<=i;j++){
			
				System.out.print((char) ch+""+num+"\t");

			}
			System.out.println(" ");
		}
	}
}
