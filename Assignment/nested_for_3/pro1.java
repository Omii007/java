/* 1. Write a program to print following pattern.
 * 	c2w 
 * 	c2w  c2w 
 * 	c2w  c2w  c2w
 * 	c2w  c2w  c2w  c2w*/

import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int rows,col;
		String ch = "c2w";

		System.out.println("Enter rows:");

		Scanner sc= new Scanner(System.in);
		rows = sc.nextInt();

		System.out.println("Enter col:");
		col = sc.nextInt();

		for(int i=1;i<=rows;i++){
		
			for(int j=1;j<=i;j++){
			
				System.out.print(ch+"\t");
			}
			System.out.println(" ");
		}
	}
}
