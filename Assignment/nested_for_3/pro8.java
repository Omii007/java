/* 8. Write a program to print following pattern.
 * 	J
 * 	I  H
 * 	G  F  E
 * 	D  C  B  A*/

import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int rows,col,ch;

		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();

		System.out.println("Enter col:");
		col = sc.nextInt();

		ch = 70 + rows;

		for(int i=1;i<=rows;i++){
		
			for(int j=1;j<=i;j++){
			
				System.out.print((char) ch +"\t");
				ch--;
			}
			System.out.println(" ");
		}
	}
}
