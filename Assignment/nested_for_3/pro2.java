/* 2. Write a program to print following pattern.
 * 	1
 * 	2  3
 * 	4  5  6
 * 	7  8  9  10*/


import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int rows,col,num;

		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();

		System.out.println("Enter col:");
		col = sc.nextInt();

		num = 1;

		for(int i=1;i<=rows;i++){
		
			for(int j=1;j<=i;j++){
			
				System.out.print(num +"\t");
				num++;
			}
			System.out.println(" ");
		}
	}
}
