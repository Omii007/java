/* 6. Write a program to print following pattern.
 * 	10  10  10  10
 * 	11  11  11  
 * 	12  12 
 * 	13*/


import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int rows,col,num;

		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();

		System.out.println("Enter col:");
		col = sc.nextInt();

		num = rows*(rows+1)/2;

		for(int i=1;i<=rows;i++){
		
			for(int j=col;j>=i;j--){
			
				System.out.print(num +"\t");

			}
			System.out.println(" ");
			num++;
		}
	}
}
