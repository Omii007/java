/* 10. Write a program to take range as input from the user and print Amstrong numbers.(Take a start and end number from a user.)
 * I/P : start: 1
 * 	 end: 1650
 * O/P : Amstrong number between 1 and 1650 is
 * 1 2 3  5 6 7 8 9 153 370 371 407 1634
 */

import java.io.*;

class Demo {

	public static void main (String[] args) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter start number:");
		int num1 = Integer.parseInt(br.readLine());
		
		System.out.println("Enter end number:");
		int num2 = Integer.parseInt(br.readLine());
		
		System.out.println("Armstrong number:");

		for(int i=num1;i<=num2;i++){
		
			int rev = i;
			int count = 0;
			
			while(rev > 0){
				
				count++;	
				rev = rev/10;
			}
			int str = i;
			int sum = 0;
		
			while(str > 0){
				
				int rem = str % 10;
				int mult = 1;
				for(int j=1;j<=count;j++){
				
					mult = mult * rem;
				}
				str = str/10;	
				sum = sum + mult;
			}
			if(i == sum)
				System.out.print(sum+" ");
		}
		System.out.println();
	}
}
