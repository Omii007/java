/* 9. Write a program to print a series of strong numbers from entered range.(Take a start and end number from a user).
 * I/P : start: 1
 * 	 end: 100
 * O/P : strong numbers between 1 to 150 is = 1 2 145
 */

import java.io.*;

class Demo {

	public static void main (String[] args) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter start number:");
		int num1 = Integer.parseInt(br.readLine());
		
		System.out.println("Enter end number:");
		int num2 = Integer.parseInt(br.readLine());
		
		System.out.println("Strong number:");
		for(int i=num1;i<=num2;i++){
			
			int rev = i;
			int sum = 0;
			while(rev > 0){
				
				int mult = 1;
				int rem = rev % 10;
				for(int j=1;j<=rem;j++){
				
					mult = mult*j;
				}
				sum = sum+mult;
				rev = rev/10;

			}
			if(i == sum)
				System.out.print(i+" ");
		}
		System.out.println();
	}
}
