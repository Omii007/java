/* 6. Write a program to take 5 numbers as input from the user and print the count of digits in those numbers.
 * I/P : Enter 5 numbers
 * 5
 * The digit count in 5 is 1
 * 25 -> The count digit in 25 is 2
 * 225 ->The count digit in 225 is 3
 * 6548 -> The count digit in 6548 is 4
 */

import java.io.*;

class Demo {

	public static void main (String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());
		
		int num1 = num;
		int count = 0;

		while(num > 0){
		
			count++;
			num = num/10;
		}
		System.out.println("The count digit in "+num1+" is "+count);
	}
}
