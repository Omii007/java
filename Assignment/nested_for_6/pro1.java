/* 1. Write a program to print the numbers divisible by 5 from 1 to 50 & the number is even also print the count of even numbers.
 * I/P : Enter lower limit: 1
 * 	 Enter upper limit: 50
 * O/P : 10,20,30,40,50
 * Count = 5
 */

import java.io.*;

class Demo {

	public static void main (String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter lower limit number:");
		int num1 = Integer.parseInt(br.readLine());
		
		System.out.println("Enter upper limit number:");
		int num2 = Integer.parseInt(br.readLine());
		
		int count = 0;
		for(int i=num1;i<=num2;i++){
		
			if(i % 5 == 0 && i % 2 == 0){
			
				System.out.println(i+" ");
				count++;
			}
		}
		System.out.println("Count = "+count);
	}
}
