/* 3. Write a program to take range from the user and print perfect squares between that range.
 * I/P : start: 1
 * 	 end: 100
 * O/P: perfect squares between 1 and 100
 */

import java.io.*;

class Demo {

	public static void main (String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter start number:");
		int num1 = Integer.parseInt(br.readLine());
		
		System.out.println("Enter end number:");
		int num2 = Integer.parseInt(br.readLine());

		for(int i=num1;i<=num2;i++){
		
			for(int j=1;j<=i;j++){
			
				if(j*j == i){
					System.out.println(i+" ");
				}
			}
		}
	}
}
