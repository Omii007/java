/* 2. Write a program to take range as input from the user and print composite numbers.
 * I/P : start: 1
 * 	 end: 20
 * O/P : 4 6 9 10 12 14 15 16 18 20
 */

import java.io.*;

class Demo {

	public static void main (String[] args) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter start number:");
		int num1 = Integer.parseInt(br.readLine());
		
		System.out.println("Enter end number:");
		int num2 = Integer.parseInt(br.readLine());
		
		for(int i=num1;i<=num2;i++){
			
			int count = 0;
			for(int j=1;j<=i;j++){
				
				if(i % j == 0){
			
					count++;
				}
			}
			if(count >2)
				System.out.print(i+" ");
		}
		System.out.println("");
	}
}
