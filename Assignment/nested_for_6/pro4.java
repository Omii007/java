/* 4. Write a program to take range as input from the user and print perfect cubes between that range.
 * I/P : start: 1
 * 	 end: 100
 * O/P : perfect cubes between 1 and 100
 */

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter start number:");
		int num1 = Integer.parseInt(br.readLine());
		
		System.out.println("Enter end number:");
		int num2 = Integer.parseInt(br.readLine());

		for(int i=num1;i<=num2;i++){
		
			for(int j=1;j*j*j<=i;j++){
			
				if(j*j*j == i){
				
					System.out.print(i+" ");
				}
			}
			
		}
		System.out.println(" ");

	}
}
