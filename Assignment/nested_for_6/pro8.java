/* 8. Write a program to take range as input from the user and print the Pallindrome numbers. (Take a start and end number from a user).
 * I/P : start: 100
 * 	 end: 200
 * O/P : Pallindrome number between 100 and 200 .....................
 */


import java.io.*;

class Demo {

	public static void main (String[] args) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter start number:");
		int num1 = Integer.parseInt(br.readLine());
		
		System.out.println("Enter end number:");
		int num2 = Integer.parseInt(br.readLine());
		
		System.out.println("Pallindrome number:");
		for(int i=num1;i<=num2;i++){
			
			int rev = i;
			int mult = 0;
			while(rev > 0){
			
				int rem = rev % 10;
				mult = mult *10+rem;
				rev = rev/10;
			}
			if(i == mult)
				System.out.print(i+" ");
			
		}
		System.out.println();
	}
}
