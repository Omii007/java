/* 5. Write a program to take range as input from the user and print perfect numbers.
 * Note : Perfect number is the one whose perfect divisor's addition is equal to that number. 6 is perfect number = 1+2+3 = 6).
 * I/P : start: 1
 * 	 end: 30
 * O/P : perfect numbers between 1 and 30
 * 6 28
 */

import java.io.*;

class Demo {

	public static void main (String[] args) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter start number:");
		int num1 = Integer.parseInt(br.readLine());
		
		System.out.println("Enter end number:");
		int num2 = Integer.parseInt(br.readLine());

		for(int i=num1;i<=num2;i++){
			
			int sum = 0;
			for(int j=1;j<i;j++){
			
				if(i % j == 0){
				
					sum = sum + j;
				}
			}
			if(i == sum)
				System.out.println("perfect number is "+i);
		}
	}
}
