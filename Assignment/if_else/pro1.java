/* 1. Write a java program to check if a number is even or odd.
 * I/P : 18
 * O/P : 18 is even number
 * I/P : 7
 * O/P : 7 is odd number
 */

import java.util.*;

class evenOdd{


	public static void main(String omkar[]){
	
		int x;
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Enter any integer number::");
		x=scan.nextInt();

		if(x%2==0)
			System.out.println(x+" is even number.");
		else
			System.out.println(x+" is odd number.");

	}
}
