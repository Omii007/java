/* 8. Write a program to print the countdown of days to submit the assignment
 * I/P : day = 7
 * O/P : 7 days remaining
 * 	 6 days remaining
 * 	 5 days remaining
 * 	 .
 * 	 .
 * 	 0 days assignment is overdue*/

import java.util.Scanner;

class day {

	public static void main(String[] args){
	
		int days;

		System.out.println("Enter number of day:");
		
		Scanner sc = new Scanner(System.in);
		days = sc.nextInt();

	
		if(days == 0)
			System.out.println(days +" assignment is overdue");
		else if(days == 1)
			System.out.println(days +" days remaining");
		else if(days == 2)
			System.out.println(days +" days remaining");
		else if(days == 3)
			System.out.println(days +" days remaining");
		else if(days == 4)
			System.out.println(days +" days remaining");
		else if(days == 5)
			System.out.println(days +" days remaining");
		else if(days == 6)
			System.out.println(days +" days remaining");
		else if(days == 7)
			System.out.println(days +" days remaining");
		else
			System.out.println("Invalid");
		
	}
}

