/* 4. Write a java program that checks a number from 0 to 5 and prints its spelling, if the number is greater than 5 print the number is greater than 5.
 * I/P :4
 * O/P : four
 * I/P :6
 * O/P : number is greater than 5*/


import java.util.Scanner;

class number {

	public static void main(String[] args){
	
		int num;

		System.out.println("Enter number::");

		Scanner sc = new Scanner(System.in);
		num = sc.nextInt();

		if(num == 0)
			System.out.println("Zero");
		else if(num == 1)
			System.out.println("One");
		else if(num == 2)
			System.out.println("Two");
		else if(num == 3)
			System.out.println("Three");
		else if(num == 4)
			System.out.println("Four");
		else if(num == 5)
			System.out.println("Five");
		else if(num > 5)
			System.out.println("number is greater than 5");
		else
			System.out.println("number is negative");	
	}
}
