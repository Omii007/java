/* 3. Write a java program take a number and print whether it is positive or negative.
 * I/P : 5
 * O/P : 5 is positive number
 * I/P : -9
 * O/P : -9 is negative number*/

import java.util.Scanner;

class positive {

	public static void main(String[] args){
	
		int num;

		System.out.println("Enter number::");

		Scanner sc = new Scanner(System.in);
		num = sc.nextInt();

		if(num > 0)
			System.out.println(num +" is positive number");
		else if(num < 0)
			System.out.println(num +" is negative number");
		else
			System.out.println(num +" is equal to zero");
	}
}
