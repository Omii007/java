/* 2. Write a java program take a number and print whether it is less than 10 or greater than 
 * 10
 * I/P : 5
 * O/P : 5 is less than 10
 * I/P : 16
 * O/P : 16 is greater than 16*/


import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int num;

		System.out.println("Enter number::");

		Scanner sc = new Scanner(System.in);
		num = sc.nextInt();

		if(num == 10)
			System.out.println(num + " is equal to 10");
		else if(num > 10)
			System.out.println(num + " is greater than 10");
		else
			System.out.println(num + " is less than 10");
	}
}
