/* 10. Write a unique real-time example of if elseif else ladder*/


import java.util.Scanner;

class real {

	public static void main(String[] args){
	
		int money;

		System.out.println("Enter rupees:");

		Scanner sc = new Scanner(System.in);
		money = sc.nextInt();

		if(money > 10000)
			System.out.println("Goa");
		else if(money > 8000)
			System.out.println("Kokan");
		else if(money > 6000)
			System.out.println("Hydrabad");
		else if(money > 4000)
			System.out.println("Mumbai");
		else if(money > 2000)
			System.out.println("Lonavla");
		else if(money > 1000)
			System.out.println("Sihgad");
		else
			System.out.println("Ghari");
	}
}
