/* 5. Write a java program in which according to month no print the no.of days in that month.
 * I/P : month=7
 * O/P : july has 31 days
 * I/P : month=13
 * O/P : Invalid month*/


import java.util.Scanner;

class days {

	public static void main(String[] args){
	
		int month;

		System.out.println("Enter month number::");

		Scanner sc = new Scanner(System.in);
		month = sc.nextInt();

		if(month == 1)
			System.out.println("Jan has 31 days");

		else if(month == 2)
			System.out.println("Feb has 28 days");

		else if(month == 3)
			System.out.println("March has 31 days");

		else if(month == 4)
			System.out.println("April has 30 day");

		else if(month == 5)
			System.out.println("May has 31 days");

		else if(month == 6)
			System.out.println("June has 30 days");

		else if(month == 7)
			System.out.println("July has 31 days");

		else if(month == 8)
			System.out.println("Aug has 31 days");

		else if(month == 9)
			System.out.println("Sep has 30 days");

		else if(month == 10)
			System.out.println("Oct has 31 days");

		else if(month == 11)
			System.out.println("Nov has 30 days");

		else if(month == 12)
			System.out.println("Dec has 31 days");

		else
			System.out.println("Invalid month");

	}
}
