/* 7. Calculate profit or loss.
 * Write a program the takes the cost price and selling price (take it hardcoded) and calculate its profit or loss
 * I/P :SP= 1200
 * I/P :CP= 1000
 * O/P : profit of 200*/
 

import java.util.Scanner;

class price {

	public static void main(String[] args){
	
		int SP,CP;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter Selling price:");
		SP = sc.nextInt();
		
		System.out.println("Enter Cost price:");
		CP = sc.nextInt();

		int total = SP - CP;

		if(total > 0)
			System.out.println("Profit of "+total);
		else if(total < 0)
			System.out.println("Loss of "+total);
		else
			System.out.println("Neither Profit Nor Loss");

	}
}
