/* 6. Write a java program to find a maximum between three numbers.
 * I/P : num1=1,num2=2,num3=3;
 * O/P : 3 is the maximum between 1,2 and 3
 * I/P : num1=42,num2=32,num3=42
 * O/P : 42 is greater than 32*/


import java.util.Scanner;

class max {

	public static void main(String[] args){
	
		int num1,num2,num3;

		System.out.println("Enter a number::");

		Scanner sc = new Scanner(System.in);
		num1 = sc.nextInt();
		num2 = sc.nextInt();
		num3 = sc.nextInt();

		if(num1 > num2 && num1 > num3)
			System.out.println(num1 +" is graeter than");

		else if(num2 > num1 && num2 > num3)
			System.out.println(num2 +" is greater than");

		else if(num3 > num1 && num3 > num2)
			System.out.println(num3 +" is greater than");

		else if(num1 > num3 && num2 > num3)
			System.out.println(num1+" is greater than");

		else if(num2 > num1 && num3 > num1)
			System.out.println(num2+" is greater than");

		else 
			System.out.println(num3+" is greater than");
	}
}
