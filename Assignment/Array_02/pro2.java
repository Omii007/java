/* Que 2. WAP to find the number of even and odd integers in a given array of integers.
 *I/P : 1 2 5 4 6 7 8
 O/P : Number of even number : 4
       Number of odd number : 3*/

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
		
		System.out.println("Enter array size:");

		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		
		System.out.println("Enter array elements:");
		
		//BufferedReader br1 = new BufferedReader (new InputStreamReader(System.in));
		
		for(int i=0;i<size;i++)	
	       		arr[i] = Integer.parseInt(br.readLine());
		int cnt1=0,cnt2=0;

		for(int i=0;i<size;i++){
		
			if(arr[i] % 2 == 0){
				cnt1++;

			}else{
				cnt2++;

			}
		}
		System.out.println("Number of Even Elements = "+cnt1);
		System.out.println("Number of Odd Elements = "+cnt2);


	}
}
