/* Que 3. WAP to find the sum of even and odd integers in a given array of integers.
 *I/P : 11  12  13  14  15
 O/P : Even number sum : 26
       Odd number sum : 39*/

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
		
		System.out.println("Enter array size:");

		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		
		System.out.println("Enter array elements:");
		
		//BufferedReader br1 = new BufferedReader (new InputStreamReader(System.in));
		
		for(int i=0;i<size;i++)	
	       		arr[i] = Integer.parseInt(br.readLine());
		
		int sum1=0,sum2=0;

		for(int i=0;i<size;i++){
		
			if(arr[i] % 2 == 0){
				
				sum1 = sum1+arr[i];

			}else{
				sum2 = sum2+arr[i];

			}
		}
		System.out.println("Even number sum = "+sum1);
		System.out.println("Odd number sum = "+sum2);


	}
}
