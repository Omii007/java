/* Que 1. WAP to create an array of 'n' integers elements.
 * Write 'n' value should be taken from the user.
 * Insert the values from the user and find the sum of all elements in the array.
 * I/P : n = 6
 * 	 array : 2 3 6 9 5 1
 * O/P : 26*/

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		System.out.println("Enter array size:");

		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		int size = Integer.parseInt(br.readLine());
		
		int arr[] = new int[size]; 
		System.out.println("Enter array elements:");
		
		//BufferedReader br1 = new BufferedReader (new InputStreamReader(System.in));
		for(int i=0;i<size;i++)
			arr[i] = Integer.parseInt(br.readLine());
		
		int sum = 0;
		for(int i=0;i<size;i++){
			
			sum = sum + arr[i];
		}
		System.out.print("Sum of array element = "+sum);
		
		System.out.println(" ");
	}
}
