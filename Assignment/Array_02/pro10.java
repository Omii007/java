/* Que 10. WAP to print the elements whose addition of digits is even.
 * EX : 26 = 2 + 6 = 8(8 is even so print 26)
 * I/P : array = 1 2 3 5 15 16 14 28 17 29 123
 * O/P : 2 15 28 17 123
 */

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException {
	
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter array size:");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");

		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		int rem,sum=0,temp;

		for(int i=0;i<arr.length;i++){
			
			temp = arr[i];
			while(temp != 0){
				rem = temp % 10;
				sum = sum+rem;
				temp = temp/10;
			}
			if(sum % 2 == 0){
			
				System.out.print(arr[i]+" ");
			}
			sum = 0;
		}
		System.out.println(" ");
	}
}
