/* Que 5. WAP to take size of array from the user and also take integer elements from user.
 * find the maximum element from the array.
 * I/P : 5
 * Array elements : 1 2 5 0 4
 * O/P : min ele = 5;
 */

import java.io.*;

class Demo{

	public static void main(String[] args) throws IOException{
	
		System.out.println("Enter array size:");

		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		
		//BufferedReader br1 = new BufferedReader (new InputStreamReader(System.in));
		
		for(int i=0;i<size;i++)
			arr[i] = Integer.parseInt(br.readLine());

		int max = 0;
		for(int i=0;i<size;i++){
		
			if(max < arr[i])
				max = arr[i];
		}
		System.out.println("Max element = "+max);
	}
}
