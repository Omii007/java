/* Que 9. Write a java program to merge two given arrays.
 * I/P : array1 = [10,20,30,40,50]
 * 	 array2 = [9,18,2,36,45]
 * O/P : merged array = [10,20,30,40,50,9,18,27,36,45]
 * Hint : you can take 3rd array*/

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter array1 size:");

		int size1 = Integer.parseInt(br.readLine());
		
		System.out.println("Enter array2 size:");

		int size2 = Integer.parseInt(br.readLine());

		int arr1[] = new int[size1];
		int arr2[] = new int[size2];
		
		System.out.println("Enter array1 elements:");

		for(int i=0;i<size1;i++){  // arr1.length use kru shkto
		
			arr1[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter array2 elements:");

		for(int i=0;i<size2;i++){   // arr2.length use kru shkto
		
			arr2[i] = Integer.parseInt(br.readLine());
		}

		int arr3[] = new int [size1+size2];

		for(int i=0;i<size1;i++){ // arr1.length
		
			arr3[i] = arr1[i];
		}
		for(int i=0;i<size2;i++){  // arr2.length
		
			arr3[size1+i] = arr2[i];
		}

		System.out.println("Merge array:");

		for(int i=0;i<size1+size2;i++) {  // arr3.length
		
			System.out.print(arr3[i] +" ");
		}
		System.out.println(" ");


	}
}
