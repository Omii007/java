/* Que 7. WAP to find the common elements between two arrays.
 * I/P : 
 * Enter first array : 1 2 3 5
 * Enter second array : 2 1 9 8
 * O/P : 1  2*/

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		System.out.println("Enter array size:");

		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		int size = Integer.parseInt(br.readLine());

		int arr1[] = new int[size];
		int arr2[] = new int [size];

		System.out.println("Enter array1 elements:");
		//BufferedReader br1 = new BufferedReader (new InputStreamReader(System.in));
		
		for(int i=0;i<size;i++)
			arr1[i] = Integer.parseInt(br.readLine());
		
		System.out.println("Enter array2 elements:");
		//BufferedReader br2 = new BufferedReader (new InputStreamReader(System.in));
		
		for(int i=0;i<size;i++)
			arr2[i] = Integer.parseInt(br.readLine());
	
		for(int i=0;i<size;i++){
		
			for(int j=0;j<size;j++){
			
				if(arr1[i] == arr2[j]){
				
					System.out.println("Common elements = "+arr2[j]);
				}
			
			}
		}
	}
}
