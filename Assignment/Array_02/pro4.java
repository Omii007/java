/* Que 4. WAP to search a specific element from an array and return its index.
 * I/P : 1 2 4 5 6
 * Enter element to search : 4
 * O/P : element found at index : 2*/

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		System.out.println("Enter array size:");

		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");
		
		//BufferedReader br1 = new BufferedReader (new InputStreamReader(System.in));
		
		for(int i=0;i<size;i++)
			arr[i] = Integer.parseInt(br.readLine());
		
		System.out.println("Enter search elements:");
		//BufferedReader br2 = new BufferedReader (new InputStreamReader(System.in));
		int search = Integer.parseInt(br.readLine());
		
		int Index = -1;
		for(int i=0;i<size;i++){

			if(arr[i] == search)
				Index = i;	
				
		}
		if(Index == -1)
			System.out.println("Element not found");
		else		
			System.out.println("Element found at index = "+Index);

	}
}
