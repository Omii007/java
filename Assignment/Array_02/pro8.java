/* Que 8. WAP to find the uncommon elements between two arrays.
 * I/P : 
 * Enter first array : 1 2 3 5
 * Enter second array : 2 1 9 8
 * O/P : 1  2*/

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		System.out.println("Enter array size:");

		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		int size = Integer.parseInt(br.readLine());

		int arr1[] = new int[size];
		int arr2[] = new int [size];

		System.out.println("Enter array1 elements:");
				
		for(int i=0;i<size;i++)
			arr1[i] = Integer.parseInt(br.readLine());
		
		System.out.println("Enter array2 elements:");
				
		for(int i=0;i<size;i++)
			arr2[i] = Integer.parseInt(br.readLine());
		int flag = 0;

		for(int i=0;i<size;i++){
		
			for(int j=0;j<size;j++){
			
				if(arr1[i] == arr2[j]){
				
					flag = 1;
				}
			}
			if(flag == 0){
				System.out.print(arr1[i]+" ");
			}
			flag = 0;
		}
		for(int i=0;i<size;i++){
		
			for(int j=0;j<size;j++){
			
				if(arr2[i] == arr1[j]){
					
					flag = 1;
				}
			}
			if(flag == 0){
				System.out.print(arr2[i]+" ");
			}
			flag = 0;
		}
		System.out.println(" ");

	}
}
