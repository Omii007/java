/* 6. Write a program to print following pattern.
 *
 * 	1
 * 	2  3
 * 	3  4  5
 * 	4  5  6  7*/

import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int rows,col,num;

		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();
		
		System.out.println("Enter rows:");
		col = sc.nextInt();

		for(int i=1;i<=rows;i++){
		
			num = i;
			for(int j=1;j<=i;j++){
			
				System.out.print(num+"\t");
				num++;
			}
			System.out.println(" ");
		}

	}
}
