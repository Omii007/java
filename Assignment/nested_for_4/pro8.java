/* 8. Write a program to print following pattern.
 * 	10
 * 	I  H
 * 	7  6  5
 * 	D  C  B  A*/


import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int rows,col,num;
		
		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();
		
		System.out.println("Enter rows:");
		col = sc.nextInt();
		
		num = rows*(rows+1)/2;

		int ch = 70 + rows;

		for(int i=1;i<=rows;i++){
		
			for(int j=1;j<=i;j++){
			
				if(i % 2 == 0){
				
					System.out.print((char)+ch+"\t");
				}else{
					
					System.out.print(num+"\t");
				}
				num--;
				ch--;
			}
			System.out.println(" ");
		}
	}
}
