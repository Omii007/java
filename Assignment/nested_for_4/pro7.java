/* 7. Write a program to print following pattern.
 * 	F
 * 	E  1
 * 	D  2  E
 * 	C  3  D  4
 * 	B  5  C  6  D
 * 	A  7  B  8  C  9*/


import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int rows,col,ch,num;

		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();
		
		System.out.println("Enter rows:");
		col = sc.nextInt();

		num = 1;
		for(int i=1;i<=rows;i++){
		
			ch = 65+rows-i;
			for(int j=1;j<=i;j++){
			
				if(j % 2 == 1){
				
					System.out.print((char)+ch+"\t");
					ch++;
				}else{
				
					System.out.print(num +"\t");
					num++;
				}
			}
			System.out.println(" ");
		}

	}
}
