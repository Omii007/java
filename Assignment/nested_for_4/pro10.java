/* 10. Write a program to print the following pattern.
 * 	
 * 	1
 * 	8  9
 * 	2  16  125
 * 	64  25  216  49*/

import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int rows,col,num;

		System.out.println("Enter rows:");
		
		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();
	
		System.out.println("Enter col:");
		col = sc.nextInt();

		for(int i=1;i<=rows;i++){
		
			num = i;
			for(int j=1;j<=i;j++){
			
				if(i % 2 == 0){
				
					if(j % 2 == 1){
					
						System.out.print(num*num*num +"\t");

					}else{
						System.out.print(num*num +"\t");
						
					}
					num++;
				}else{
					if(j % 2 == 1){
					
						System.out.print(num*num +"\t");

					}else{
						System.out.print(num*num*num +"\t");
						
					}
					num++;
				}
			}
			System.out.println(" ");
		}
	}
}
