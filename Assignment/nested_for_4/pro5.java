/* 5. Write a program to print following print.
 * 	
 * 	A  B  C  D
 * 	B  C  D
 * 	C  D 
 * 	D*/

import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int rows,col,ch;

		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();
		
		System.out.println("Enter rows:");
		col = sc.nextInt();

		for(int i=1;i<=rows;i++){
		
			ch = 60+rows+i;
			for(int j=rows;j>=i;j--){
			
				System.out.print((char)+ch+"\t");
				ch++;
			}
			System.out.println(" ");
		}

	}
}
