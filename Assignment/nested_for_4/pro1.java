/* 1. Write a program to print following pattern.
 * 	c2w10  
 * 	c2w9  c2w8
 * 	c2w7  c2w6  c2w5
 * 	c2w4  c2w3  c2w2  c2w1*/


import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int rows,col,num;
		String ch = "c2w";
		
		System.out.println("Enter rows:");

		Scanner sc =  new Scanner(System.in);
		rows = sc.nextInt();

		System.out.println("Enter rows:");
		col = sc.nextInt();

		num = rows*(rows+1)/2;

		for(int i=1;i<=rows;i++){
		
			for(int j=1;j<=i;j++){
			
				System.out.print(ch +""+num +"\t");
				num--;
			}
			System.out.println(" ");
		}
	}
}
