/* 4. Write a program to print following pattern.
 * 	1  2  3  4
 * 	2  3  4
 * 	3  4 
 * 	4*/

import java.util.Scanner;

class demo {

	public static void main(String[] args){
	
		int rows,col,num;

		System.out.println("Enter rows:");

		Scanner sc = new Scanner(System.in);
		rows = sc.nextInt();
		
		System.out.println("Enter rows:");
		col = sc.nextInt();

		for(int i=1;i<=rows;i++){
		
			num = i;
			for(int j=col;j>=i;j--){
			
				System.out.print(num +"\t");
				num++;
			}
			System.out.println(" ");
		}
	}
}
