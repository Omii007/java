/* Que 9. WAP to take a number as input & print the addition of factorial of each digit from that number.
 * I/P : 1234
 * O/P : 33 */

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter number:");
		
		int num = Integer.parseInt(br.readLine());
		int sum = 0;

		while(num > 0){
		
			int rem = num % 10;
			int mult = 1;

			for(int i=1;i<=rem;i++){
			
				mult = mult * i;
			}
			sum = sum + mult;
			num = num / 10;
		}
		System.out.println("Sum of factorial of each digit is "+sum);
	}
}
