/* Que 10. WAP to print a series of prime number from entered range.(Take a start and end number from the user)
 * I/P : Start : 10
 * 	 End : 100
 * O/P : 11  13  17  19....89  97*/


import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter Start:");
		int start = Integer.parseInt(br.readLine());

		System.out.println("Enter End:");
		int end = Integer.parseInt(br.readLine());

		for(int i=start;i<=end;i++){
			
			int count = 0;

			for(int j=2;j<=i/2;j++){
			
				if(i % j == 0){
				
					count++;
				}
			}
			if(count == 0)
				System.out.print(i+" ");
		}
		System.out.println(" ");
	}
}
