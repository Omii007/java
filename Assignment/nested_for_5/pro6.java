/* Que6. WAP and take two characters if these characters are equal then print they are unequal then print their difference.(Note : consider positional difference not ASCII)
 * I/ P : a p
 * O/P : The difference between a & p is 15*/


import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		
		System.out.println("Enter character1:");
		char ch1 = (char)br.read();
		
		br.skip(1);

		System.out.println("Enter character2:");
		char ch2 = (char)br.read();

		if(ch1 == ch2){
		
			System.out.println(ch1+" and "+ch2+" Both are equal");
		}else{
			
			System.out.println("The difference between "+ch1+" and "+ch2+" is "+(ch2-ch1));
		
		}
	}
}
