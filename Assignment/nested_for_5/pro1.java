/* Que1. Write a program to print following pattern
 * 	D4  C3  B2  A1
 * 	A1  B2  C3  D4
 * 	D4  C3  B2  A1
 * 	A1  B2  C3  D4 */
import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter rows:");
		int rows = Integer.parseInt(br.readLine());
		
		System.out.println("Enter col:");
		int col = Integer.parseInt(br.readLine());

		for(int i=1;i<=rows;i++){
		
			int ch1 = 64+rows;
			int ch2 = 61+rows;
			int x = 1;
			int y = rows;

			for(int j=1;j<=col;j++){
			
				if(i % 2 == 0){
				
					System.out.print((char)ch2+""+x+"\t");
					ch2++;
					x++;

				}else{
					System.out.print((char)ch1+""+y+"\t");
					ch1--;
					y--;
				
				
				}
				
			}
			System.out.println(" ");
		}


	}
}
