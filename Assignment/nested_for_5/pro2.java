/* Que 2. Write a program to print following pattern.
 * 	#  =  =  =  =
 * 	=  #  =  =  =
 * 	=  =  #  =  =
 * 	=  =  =  #  =
 * 	=  =  =  =  # */

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter Rows:");
		int rows = Integer.parseInt(br.readLine());
		
		System.out.println("Enter col:");
		int col = Integer.parseInt(br.readLine());

		for(int i=1;i<=rows;i++){
		
			for(int j=1;j<=col;j++){
			
				if(i == j){
				
					System.out.print("#\t");
				}else{
				
					System.out.print("=\t");
				}
			}
			System.out.println(" ");
		}

	}
}
