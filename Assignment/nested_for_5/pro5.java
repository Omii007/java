/* Que5. Write a program to print following pattern.
 * 	0
 * 	1  1
 * 	2  3  5
 * 	8  13  21  34*/


import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter rows:");

		int rows = Integer.parseInt(br.readLine());

		int a=0,b=1,c=0;

		for(int i=1;i<=rows;i++){
		
			for(int j=1;j<=i;j++){
			
				a = c;
				c = b;
				System.out.print(a+"\t");
				b = a + c;
			}
			System.out.println(" ");
		}
	}
}
