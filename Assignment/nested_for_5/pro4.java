/* Que4. WAP to print all even number in reverse order & odd numbers in standard way within range. Take start ans end from the user.
 * I/P : Start : 2
 * 	 End : 9
 * O/P : 8  6  4  2
 * 	 3  5  7  9*/


import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter Start:");
		int start = Integer.parseInt(br.readLine());
	
		System.out.println("Enter End:");
		int end = Integer.parseInt(br.readLine());

		for(int i=end;i>=start;i--){
		
			if(i % 2 == 0)
			       System.out.print(i+"\t");	
		}
		System.out.println(" ");

		for(int i=start;i<=end;i++){
		
			if(i % 2 == 1)
			       System.out.print(i+"\t");	
		}
		System.out.println(" ");

	}
}
