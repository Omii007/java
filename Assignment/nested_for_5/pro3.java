/* Que3. Write a program to print following pattern.
 * 	5  4  3  2  1
 * 	8  6  4  2
 * 	9  6  3
 * 	8  4 
 * 	5*/

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter rows:");

		int rows = Integer.parseInt(br.readLine());

		for(int i=1;i<=rows;i++){
		
			int num = rows-i+1;
			int num1 = num*i;
			for(int j=rows;j>=i;j--){
			
				System.out.print(num1+"\t");
				num1-=i;
			}
			System.out.println(" ");
		}
	}
}
