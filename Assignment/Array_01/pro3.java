/* Que 3. WAP to take size of array from user and also take integer elements from user print product of odd index only.
 * I/P : size - 6
 * array = 1 2 3 4 5 6 
 * O/P : 2*2*10 = 40
 */

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter array size:");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");

		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		int mult = 1;

		for(int i=0;i<arr.length;i++){
		
			if(i % 2 == 1){
			
				mult = mult * arr[i];
			}
		}
		System.out.println("Total product = "+mult);
	}
}
