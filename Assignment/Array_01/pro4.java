/* Que 4. WAP take 7 characters as input. print only vowels from the array.
 * I/P : array = a b c o d p e 
 * O/P : a o e
 */

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter array size:");

		int size = Integer.parseInt(br.readLine());

		char arr[] = new char[size];

		System.out.println("Enter array elements:");

		for(int i=0;i<arr.length;i++){
		
			arr[i] = (char)br.read();
		}

		for(int i=0;i<arr.length;i++){
		
			if(arr[i] == 'a' || arr[i] == 'e' || arr[i] == 'i' || arr[i] == 'o' || arr[i] == 'u' || arr[i] == 'A' || arr[i] == 'E' || arr[i] == 'I' || arr[i] == 'O' || arr[i] == 'U')
			
			{
				System.out.println(arr[i] +" ");
			}
		}
		
	}
}
