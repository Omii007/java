/* Que 1. WAP to take size of array from user and also take integer elements from user print sum of odd elements only.
 * I/P : size - 5
 * array = 1 2 3 4 5 
 * O/P : 1+3+5 = 9
 */

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter array size:");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");

		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		int sum = 0;

		for(int i=0;i<arr.length;i++){
		
			if(arr[i] % 2 == 1){
			
				sum = sum + arr[i];
			}
		}
		System.out.println("Total sum = "+sum);
	}
}
