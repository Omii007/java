/* Que 2. WAP to take size of array from user and also take integer elements from user print product of even elements only.
 * I/P : size - 9
 * array = 1 2 3 2 5 10 55 77 99 
 * O/P : 2*2*10 = 40
 */

import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter array size:");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements:");

		for(int i=0;i<arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		int mult = 1;

		for(int i=0;i<arr.length;i++){
		
			if(arr[i] % 2 == 0){
			
				mult = mult * arr[i];
			}
		}
		System.out.println("Total product = "+mult);
	}
}
