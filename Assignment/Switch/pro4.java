/* 4 . Take a choice from the user show this to user
 * What's your interest?
 * 1. Movies
 * 2. Series
 *
 * if User enters 1 :
 * Movie you wish to watch today
 * 1) Founder 2) Snowden 3) Jobs 4) Her 5) Social Network 6) Wall-E 7) AI
 * Enter your choice : 2
 * O/P : 2. Snowden
 *
 * If user enters 2 :
 * Best series to watch :
 * 1) Silicon Valley 2) Devs 3) The IT Crowed 4) Mr.Robot
 * Print user choice:
 */

import java.io.*;

class Enjoy {

	public static void main (String[] args) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter your choice:");

		System.out.println("1.Movies");
		System.out.println("2.Series");

		int choice = Integer.parseInt(br.readLine());

		switch(choice){
		
			case 1 :{
			
				System.out.println("Movie you wish to watch today:");
				System.out.println("1.Founder");
				System.out.println("2.Snowden");
				System.out.println("3.Jobs");
				System.out.println("4.Her");
				System.out.println("5.Social Network");
				System.out.println("6.Wall-E");
				System.out.println("7.AI");
				int wish = Integer.parseInt(br.readLine());

				switch(wish){
				
					case 1 :
						System.out.println("Today you watch FOUNDER movie");
						break;
					case 2 :
						System.out.println("Today you watch SNOWDEN movie");
						break;
					case 3 :
						System.out.println("Today you watch JOBS movie");
						break;
					case 4 :
						System.out.println("Today you watch HER movie");
						break;
					case 5 :
						System.out.println("Today you watch SOCIAL NETWORK movie");
						break;
					case 6 :
						System.out.println("Today you watch WALL-E movie");
						break;
					case 7 :
						System.out.println("Today you watch AI movie");
						break;
					default :
						System.out.println("Invalid choice");
						break;
				}
			}
			break;
			case 2 :{
				
				System.out.println("Best series to watch:");
				System.out.println("1.Silicon Valley");
				System.out.println("2.Devs");
				System.out.println("3.The It Crowd");
				System.out.println("4.Mr.Robot");
				int wish = Integer.parseInt(br.readLine());

				switch(wish){
				
					case 1 :
						System.out.println("Today you watch SILICON VALLEY series");
						break;
					case 2 :
						System.out.println("Today you watch DEVS series");
						break;
					case 3 :
						System.out.println("Today you watch THE IT CROWD series");
						break;
					case 4 :
						System.out.println("Today you watch MR.ROBOT series");
						break;
					default :
						System.out.println("Invalid choice");
						break;
				}
			}
			break;
			default :
				System.out.println("Invalid choice");
				break;

		}
	}
}
