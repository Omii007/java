/* 3 . Write a program in which the user should enter two numbers if both the numbers are positive multiply them and provide to switch case to verify whether the number is even or odd , if the user enters any negative numbers program should terminate by saying "Sorry negative number not allowed".
 */

import java.io.*;

class Demo {

	public static void main (String[] args) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number 1:");
		int num1 = Integer.parseInt(br.readLine());
		
		System.out.println("Enter number 2:");
		int num2 = Integer.parseInt(br.readLine());

		if(num1 < 0 || num2 < 0){
		
			System.out.println("Sorry negative number not allowed");
		}else{
		
			switch ((num1*num2)%2){
			
				case 0 :
					System.out.println("Multiplication is even");
					break;
				case 1 :
					System.out.println("Multiplication is odd");
					break;

			}
		}
	}
}
