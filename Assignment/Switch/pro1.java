/* 1 . Write a program in which students should enter marks of 5 different subjects. if all subject having above passing marks add them and provide to switch case to print grades(first class second class). if students get fail in any subject program should print "You failed in exam".
 */

import java.io.*;

class Student {

	public static void main (String[] args) throws IOException {
	
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		
		System.out.println("Enter Marathi subject Marks:");
		int Marathi = Integer.parseInt(br.readLine());
		
		System.out.println("Enter English subject Marks:");
		int English = Integer.parseInt(br.readLine());
		
		System.out.println("Enter Mathematic subject Marks:");
		int Mathematics = Integer.parseInt(br.readLine());
		
		System.out.println("Enter Hindi subject Marks:");
		int Hindi = Integer.parseInt(br.readLine());
		
		System.out.println("Enter Science subject Marks:");
		int Science = Integer.parseInt(br.readLine());
		
		if(Marathi < 35 || English < 35 || Mathematics < 35 || Hindi < 35 || Science < 35){
			System.out.println("You failed in exam");
		}else{
		
			double avg = (Marathi + English + Mathematics + Hindi + Science)/5;
			System.out.println("Avg = "+avg);
		
			char ch;

			if(avg > 75){
			
				ch = 'A';
			}else if (avg > 60){
		
				ch = 'B';
			}else if(avg > 50){
		
				ch = 'C';
			}else if(avg > 40){
		
				ch = 'D';
			}else if (avg > 35){
		
				ch = 'E';
			}else {
		
				ch = 'F';
			}
			switch (ch){
		
				case 'A' :
					System.out.println("Pass with distinction");
					break;
				case 'B' :
					System.out.println("Pass with first class");
					break;
				case 'C' :
					System.out.println("Pass with second class");
					break;
				case 'D' :
					System.out.println("Pass with third class");
					break;
				case 'E' :
					System.out.println("Pass");
					break;
				case 'F' :
					System.out.println("Fail");
					break;
			}
		}
	}
}
